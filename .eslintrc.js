module.exports = {
  extends: 'erb',
  settings: {
    'import/resolver': {
      webpack: {
        config: require.resolve('./configs/webpack.config.eslint.js')
      }
    }
  },
  rules: {
    'flowtype/no-weak-types': [
      2,
      {
        any: false,
        Object: true,
        Function: true
      }
    ]
  },
  parserOptions: {
    ecmaFeatures: {
      legacyDecorators: true
    }
  }
};
