/**
 * Build config for electron renderer process
 */

import path from 'path';
import webpack from 'webpack';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import merge from 'webpack-merge';
import TerserPlugin from 'terser-webpack-plugin';
import baseConfig, {
  // libraryTarget,
  rootBase,
  appBase,
  contentBase
} from './webpack.config.base';
import CheckNodeEnv from '../internals/scripts/CheckNodeEnv';
import ProcessWeb from '../internals/scripts/ProcessWeb';
import FilterChunkWebpackPlugin from '../internals/scripts/FilterChunkWebpackPlugin';

CheckNodeEnv('production');

const target = process.env.TARGET || 'electron-renderer';
const entry = {
  renderer: path.join(appBase, 'index')
};

const node = {};

const modulePath = path.join(rootBase, 'modules');
const moduleDistPath = path.join(rootBase, 'data', 'modules');
const modules = process.env.MODULE ? process.env.MODULE.split(',') : [];
const sourceMap = process.env.SOURCE_MAP
  ? process.env.SOURCE_MAP === 'true'
  : !modules.length; // default disable for module
const minimizer = [];
if (!process.env.E2E_BUILD) {
  const terserOptions = {
    parallel: true,
    sourceMap,
    cache: true
  };
  // this one run before emit, so should not minify other but module assets
  if (modules.length)
    terserOptions.chunkFilter = chunk => modules.includes(chunk.name);

  minimizer.push(new TerserPlugin(terserOptions));
  minimizer.push(
    new OptimizeCSSAssetsPlugin({
      cssProcessorOptions: sourceMap
        ? {
            map: {
              inline: false,
              annotation: true
            }
          }
        : {}
    })
  );
}

const plugins = [
  /**
   * Create global constants which can be configured at compile time.
   *
   * Useful for allowing different behaviour between development builds and
   * release builds
   *
   * NODE_ENV should be production so that modules do not perform certain
   * development checks
   */

  new webpack.EnvironmentPlugin({
    NODE_ENV: 'production',
    TARGET: target,
    WEB: process.env.WEB || 'false',
    DISABLED_AI: process.env.DISABLED_AI || 'false',
    DEBUG_PROD: process.env.DEBUG_PROD || 'false',
    MENU_DEBUG: process.env.MENU_DEBUG || 'false',
    SERVER_PORT: process.env.SERVER_PORT || 3000
  }),

  new MiniCssExtractPlugin({
    filename: 'style.css',
    chunkFilename: `[name].${target}.prod.css`
  }),

  new BundleAnalyzerPlugin({
    analyzerMode: process.env.OPEN_ANALYZER === 'true' ? 'server' : 'disabled',
    openAnalyzer: process.env.OPEN_ANALYZER === 'true'
  }),

  new FilterChunkWebpackPlugin({
    modules,
    modulePath
  })
];

// if (target !== 'web') {
//   if (!modules.length) entry.background = path.join(appBase, 'background');
// } else {
if (!modules.length) ProcessWeb(path.join(appBase, 'index.html'), contentBase);
// no fs in web
node.fs = 'empty';

// on web should not bundle this module
// if (process.env.DISABLED_AI === 'true') {
//   plugins.push(new webpack.IgnorePlugin(/.*/, /faceapi/));
// }
// }

// 1024 KB, if not web than can be maximum 5 * max entry size
const maxEntrypointSize = target === 'web' ? 1024000 : 5120000;

export default merge.smart(baseConfig, {
  devtool: sourceMap ? 'source-map' : false,

  // // css no need to parse if not the module
  // externals(context, request, callback) {
  //   const ignore = context
  //     .substr(rootBase.length)
  //     .startsWith(modules.length ? '/app' : '/modules');
  //   if (ignore && request.endsWith('.css')) {
  //     return callback(null, `${libraryTarget} ${request}`);
  //   }
  //   callback();
  // },

  mode: 'production',

  target,

  entry,

  node,

  output: {
    path: modules.length ? moduleDistPath : contentBase,
    publicPath: target === 'web' ? '/' : './dist/', // for chunk loading, on web default static is dist
    filename: `[name].${target}.prod.js`,
    chunkFilename: `[name].${target}.prod.js`
  },

  performance: {
    maxEntrypointSize,
    maxAssetSize: maxEntrypointSize
  },

  module: {
    rules: [
      {
        test: /\.wasm$/i,
        type: 'javascript/auto',
        use: [
          {
            loader: 'file-loader'
          }
        ]
      },
      // Extract all .global.css to style.css as is
      {
        test: /\.global\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: './'
            }
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap
            }
          }
        ]
      },
      // Pipe other styles through css modules and append to style.css
      {
        test: /^((?!\.global).)*\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]__[local]__[hash:base64:5]'
              },
              sourceMap
            }
          }
        ]
      },
      // Add SASS support  - compile all .global.scss files and pipe it to style.css
      {
        test: /\.global\.(scss|sass)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap,
              importLoaders: 1
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap
            }
          }
        ]
      },
      // Add SASS support  - compile all other .scss files and pipe it to style.css
      {
        test: /^((?!\.global).)*\.(scss|sass)$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]__[local]__[hash:base64:5]'
              },
              importLoaders: 1,
              sourceMap
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap
            }
          }
        ]
      },
      // WOFF Font
      {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/font-woff'
          }
        }
      },
      // WOFF2 Font
      {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/font-woff'
          }
        }
      },
      // TTF Font
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/octet-stream'
          }
        }
      },
      // EOT Font
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        use: 'file-loader'
      },
      // SVG Font
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'image/svg+xml'
          }
        }
      },
      // Common Image Formats
      {
        test: /\.(?:ico|gif|png|jpg|jpeg|webp)$/,
        use: 'url-loader'
      }
    ]
  },

  optimization: {
    minimizer
  },

  plugins
});
