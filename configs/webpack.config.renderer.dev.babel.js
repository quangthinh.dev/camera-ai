/* eslint global-require: off, import/no-dynamic-require: off */

/**
 * Build config for development electron renderer process that uses
 * Hot-Module-Replacement
 *
 * https://webpack.js.org/concepts/hot-module-replacement/
 */

import path from 'path';
import fs from 'fs';
import webpack from 'webpack';
import chalk from 'chalk';
import merge from 'webpack-merge';
import { v4 } from 'internal-ip';
import { execSync } from 'child_process';
import baseConfig, {
  rootBase,
  appBase,
  contentBase
} from './webpack.config.base';
import CheckNodeEnv from '../internals/scripts/CheckNodeEnv';
import ProcessWeb from '../internals/scripts/ProcessWeb';
import OpenApp from '../internals/scripts/OpenApp';

import logger from '../app/server/logger';

// When an ESLint server is running, we can't set the NODE_ENV so we'll check if it's
// at the dev webpack config is not accidentally run in a production environment
if (process.env.NODE_ENV === 'production') {
  CheckNodeEnv('development');
}

// default ip for testing
const target = process.env.TARGET || 'electron-renderer';
// on electron can not resolve 0.0.0.0
const host = process.env.HOST || (target === 'web' ? '0.0.0.0' : 'localhost');
const port = process.env.PORT || 1212;
const baseURL = `http://${host}:${port}`;
const SERVER_PORT = process.env.SERVER_PORT || 3000;

const publicPath = '/dist/'; // where to load file
const dll = path.join(rootBase, 'dll');
const manifest = path.resolve(dll, 'renderer.json');
const requiredByDLLConfig = module.parent.filename.includes(
  'webpack.config.renderer.dev.dll'
);

const entry = {
  renderer: [
    ...(process.env.PLAIN_HMR ? [] : ['react-hot-loader/patch']),
    `webpack-dev-server/client?${baseURL}/`,
    'webpack/hot/only-dev-server',
    require.resolve('../app/index')
  ]
};

const node = {
  __dirname: false,
  __filename: false
};

const devServerOptions = {};
const open = process.env.OPEN === 'true';

// if (target !== 'web') {
//   if (process.env.DISABLED_AI !== 'true')
//     entry.background = require.resolve('../app/background');

//   // extend for web only
//   // Object.assign(devServerOptions, {
//   //   contentBase
//   // });
// } else {
ProcessWeb(path.join(appBase, 'index.html'), dll);
// no fs in web
node.fs = 'empty';

// extend for web only, priority from dll so it is development
Object.assign(devServerOptions, {
  contentBase: [dll, contentBase]
});

// if target is web, and open, set open page
if (open) {
  Object.assign(devServerOptions, {
    open: true,
    openPage: baseURL
  });
}
// }

/**
 * Warn if the DLL is not built
 */
if (!requiredByDLLConfig && !(fs.existsSync(dll) && fs.existsSync(manifest))) {
  logger.log(
    chalk.black.bgYellow.bold(
      'The DLL files are missing. Sit back while we build them for you with "yarn build-dll"'
    )
  );
  execSync('yarn build-dll');
}

export default merge.smart(baseConfig, {
  devtool: 'inline-source-map',

  mode: 'development',

  target,

  entry,

  output: {
    publicPath: `${target === 'web' ? '' : baseURL}${publicPath}`, // from web server
    filename: '[name].dev.js'
  },

  module: {
    rules: [
      {
        test: /\.wasm$/i,
        type: 'javascript/auto',
        use: [
          {
            loader: 'file-loader'
          }
        ]
      },
      {
        test: /\.global\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /^((?!\.global).)*\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]__[local]__[hash:base64:5]'
              },
              sourceMap: true,
              importLoaders: 1
            }
          }
        ]
      },
      // SASS support - compile all .global.scss files and pipe it to style.css
      {
        test: /\.global\.(scss|sass)$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      // SASS support - compile all other .scss files and pipe it to style.css
      {
        test: /^((?!\.global).)*\.(scss|sass)$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]__[local]__[hash:base64:5]'
              },
              sourceMap: true,
              importLoaders: 1
            }
          },
          {
            loader: 'sass-loader'
          }
        ]
      },
      // WOFF Font
      {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/font-woff'
          }
        }
      },
      // WOFF2 Font
      {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/font-woff'
          }
        }
      },
      // TTF Font
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/octet-stream'
          }
        }
      },
      // EOT Font
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        use: 'file-loader'
      },
      // SVG Font
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'image/svg+xml'
          }
        }
      },
      // Common Image Formats
      {
        test: /\.(?:ico|gif|png|jpg|jpeg|webp)$/,
        use: 'url-loader'
      }
    ]
  },
  resolve: {
    alias: {
      'react-dom': '@hot-loader/react-dom'
    }
  },
  plugins: [
    requiredByDLLConfig
      ? null
      : new webpack.DllReferencePlugin({
          context: dll,
          manifest: require(manifest),
          sourceType: 'var'
        }),

    new webpack.HotModuleReplacementPlugin({
      multiStep: true
    }),

    new webpack.NoEmitOnErrorsPlugin(),

    /**
     * Create global constants which can be configured at compile time.
     *
     * Useful for allowing different behaviour between development builds and
     * release builds
     *
     * NODE_ENV should be production so that modules do not perform certain
     * development checks
     *
     * By default, use 'development' as NODE_ENV. This can be overriden with
     * 'staging', for example, by changing the ENV variables in the npm scripts
     */
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
      WEB: process.env.WEB || 'false',
      DISABLED_AI: process.env.DISABLED_AI || 'false',
      TARGET: target,
      SERVER_PORT
    }),

    new webpack.LoaderOptionsPlugin({
      debug: true
    })
  ],

  node,

  devServer: {
    host,
    disableHostCheck: true,
    port,
    publicPath,
    compress: true,
    noInfo: true,
    stats: 'errors-only',
    inline: true,
    lazy: false,
    hot: true,
    headers: { 'Access-Control-Allow-Origin': '*' },
    injectClient: false, // already inject
    watchOptions: {
      aggregateTimeout: 300,
      ignored: /node_modules/,
      poll: 500
    },
    historyApiFallback: {
      verbose: true,
      disableDotRule: false
    },
    ...devServerOptions,
    before() {
      if (process.env.START_HOT !== 'false') {
        // web or electron ?
        if (target === 'web') {
          logger.log('Starting Web ...');

          // open web
          if (!open) {
            const remoteHost = v4.sync() || 'localhost';
            let msg = `Open web page at ${baseURL}`;
            if (remoteHost !== host) {
              msg += ` or http://${remoteHost}:${port}`;
            }
            logger.log(msg);
          }
        } else {
          logger.log('Starting Main Process...');

          if (open)
            OpenApp(process.env.INSPECT ? 'start-main-dev' : 'start-main');
          else logger.log(`Run SERVER_PORT=${SERVER_PORT} yarn open-main`);
        }
      }
    }
  }
});
