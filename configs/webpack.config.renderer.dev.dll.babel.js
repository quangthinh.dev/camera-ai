/* eslint global-require: off, import/no-dynamic-require: off */

/**
 * Builds the DLL for development electron renderer process
 */

import webpack from 'webpack';
import path from 'path';
import fs from 'fs';
import merge from 'webpack-merge';
import baseConfig, { appBase, rootBase } from './webpack.config.base';
import { dependencies } from '../package.json';
import CheckNodeEnv from '../internals/scripts/CheckNodeEnv';

const target = process.env.TARGET || 'electron-renderer';

CheckNodeEnv('development');

const dist = path.join(rootBase, 'dll');

// make sure dll dir existed
if (!fs.existsSync(dist)) fs.mkdirSync(dist);

export default merge.smart(baseConfig, {
  context: rootBase,

  devtool: 'eval',

  mode: 'development',

  target,

  externals: ['fsevents', 'crypto-browserify'],

  /**
   * Use `module` from `webpack.config.renderer.dev.js`
   */
  module: require('./webpack.config.renderer.dev.babel').default.module,

  entry: {
    renderer: Object.keys(dependencies || {})
  },

  output: {
    library: 'renderer',
    path: dist,
    filename: '[name].dev.dll.js',
    libraryTarget: 'var'
  },

  plugins: [
    new webpack.DllPlugin({
      path: path.join(dist, '[name].json'),
      name: '[name]'
    }),

    /**
     * Create global constants which can be configured at compile time.
     *
     * Useful for allowing different behaviour between development builds and
     * release builds
     *
     * NODE_ENV should be production so that modules do not perform certain
     * development checks
     */
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
      TARGET: target,
      DISABLED_AI: process.env.DISABLED_AI || 'false',
      SERVER_PORT: process.env.SERVER_PORT || 3000
    }),

    new webpack.LoaderOptionsPlugin({
      debug: true,
      options: {
        context: appBase,
        output: {
          path: dist
        }
      }
    })
  ]
});
