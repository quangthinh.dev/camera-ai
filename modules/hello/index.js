import React from 'react';
import SettingsIcon from '@material-ui/icons/Settings';
import HelloPage from './containers/HelloPage';

export { messages } from './locales';

export const route = {
  key: 'location.hello',
  path: '/module/hello',
  component: HelloPage,
  icon: <SettingsIcon />
};

export default {
  name: 'Hello'
};
