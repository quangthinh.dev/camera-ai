import reducer from '../../app/reducers/settings';
import * as actions from '../../app/actions/settings';

describe('settings action', () => {
  it('should setItem', () => {
    const items = 2;
    const action = actions.setItem('items', items);
    expect(action).toBeInstanceOf(Object);
    const newState = reducer({}, action);
    console.log(newState);
    expect(newState).toEqual({ items });
  });
});
