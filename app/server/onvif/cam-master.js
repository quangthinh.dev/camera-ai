/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
/* eslint-disable max-classes-per-file */
/* eslint-disable class-methods-use-this */
// import { fork } from 'child_process';
import { PassThrough } from 'stream';
import { fork } from 'child_process';
import { Worker } from 'worker_threads';
import Cam from './cam';
import { MAIN_STREAM } from '../constants/stream.json';
import logger from '../logger';

const createCamFork = filename => {
  // load from external file
  const worker = fork(filename, {
    serialization: 'advanced'
  });

  return worker;
};

const createCamWorker = filename => {
  // load from external file
  const worker = new Worker(filename);
  worker.send = worker.postMessage.bind(worker);
  return worker;
};

class InnerCamWorker {
  constructor(connectedDevice, isIPCam, workerType) {
    // by default thread can share ArrayBuffer
    this.connectedDevice = connectedDevice;
    this.isIPCam = isIPCam;
    this.workerType = workerType;
  }

  init() {
    logger.log(
      'Init cam',
      this.connectedDevice.url,
      'use worker type',
      this.workerType
    );

    // only 1 cam worker
    const camWorkerPath =
      global.camWorkerPath || require.resolve('./cam-worker.js');

    if (this.workerType === 'thread') {
      this.camWorker = createCamWorker(camWorkerPath);
    } else {
      this.camWorker = createCamFork(camWorkerPath);
    }

    this.connectedDevice.on('profileChanged', this.updateProfile);
  }

  updateProfile = data => {
    this.camWorker.send({
      cmd: 'profile',
      data
    });
  };

  async connect() {
    const connectedDevice = {
      url: this.connectedDevice.url,
      user: this.connectedDevice.user,
      pass: this.connectedDevice.pass,
      profile: this.connectedDevice.profile
    };
    this.camWorker.send({
      cmd: 'init',
      data: {
        connectedDevice,
        isIPCam: this.isIPCam
      }
    });
  }

  async close() {
    this.connectedDevice.off('profileChanged', this.updateProfile);
    // process exit = kill
    this.camWorker.send({ cmd: 'close' });
    this.camWorker = null;
  }

  get IsConnected() {
    // always connected
    return true;
  }
}

const createInnerCam = workerType => (connectedDevice, isIPCam) => {
  return new InnerCamWorker(connectedDevice, isIPCam, workerType);
};

export default class CamMaster extends Cam {
  constructor(connectedDevice, isIPCam, workerType) {
    super({
      connectedDevice,
      isIPCam,
      keepAliveTime: 0,
      createInnerCam: createInnerCam(workerType)
    });
    // then init this cam
    this.innerCam.init();
    this.innerCam.camWorker.on('message', ({ cmd, data }) => {
      if (cmd === 'pipe') {
        const { StreamType, type, buffer } = data;
        const videoStream = this.getVideoStream(StreamType);
        videoStream[type].write(buffer);
      }
    });

    this.mainStreams = {
      video: new PassThrough(),
      audio: new PassThrough()
    };
    this.subStreams = {
      video: new PassThrough(),
      audio: new PassThrough()
    };
  }

  // override get video stream
  getVideoStream(StreamType) {
    return StreamType === MAIN_STREAM ? this.mainStreams : this.subStreams;
  }
}
