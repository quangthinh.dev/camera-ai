/* eslint-disable no-plusplus */
/* eslint-disable no-empty */
/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-cond-assign */
/* eslint-disable camelcase */
/* ------------------------------------------------------------------
 * node-onvif - service-ptz.js
 *
 * Copyright (c) 2016 - 2017, Futomi Hatano, All rights reserved.
 * Released under the MIT license
 * Date: 2017-08-30
 * ---------------------------------------------------------------- */

const mOnvifSoap = require('./soap.js');
const OnvifServiceBase = require('./service-base');

/* ------------------------------------------------------------------
 * Constructor: OnvifServicePtz(params)
 * - params:
 *    - xaddr   : URL of the entry point for the media service
 *                (Required)
 *    - user  : User name (Optional)
 *    - pass  : Password (Optional)
 *    - time_diff: ms
 * ---------------------------------------------------------------- */
class OnvifServicePtz extends OnvifServiceBase {
  constructor(params) {
    super(params);

    this.name_space_attr_list = [
      'xmlns:ter="http://www.onvif.org/ver10/error"',
      'xmlns:xs="http://www.w3.org/2001/XMLSchema"',
      'xmlns:tt="http://www.onvif.org/ver10/schema"',
      'xmlns:tptz="http://www.onvif.org/ver20/ptz/wsdl"'
    ];

    this.name_space_method = 'tptz';
  }

  /* ------------------------------------------------------------------
   * Method: getNodes([callback])
   * ---------------------------------------------------------------- */
  getNodes() {
    return this._createMethodRequestSoap('GetNodes').then(result => {
      const d = result.data.PTZNode;
      if (!Array.isArray(d)) {
        result.data.PTZNode = [d];
      }

      return result;
    });
  }

  /* ------------------------------------------------------------------
   * Method: getNode(params[, callback])
   * - params:
   *   - NodeToken | String | required | a token of the targeted PTZ node
   * ---------------------------------------------------------------- */
  getNode(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.NodeToken, 'string'))) {
      return Promise.reject(
        new Error(`The "NodeToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetNode',
      `<tptz:NodeToken>${params.NodeToken}</tptz:NodeToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: getConfigurations([callback])
   * ---------------------------------------------------------------- */
  getConfigurations() {
    return this._createMethodRequestSoap('GetConfigurations').then(result => {
      const d = result.data.PTZConfiguration;
      if (!Array.isArray(d)) {
        result.data.PTZConfiguration = [d];
      }

      return result;
    });
  }

  /* ------------------------------------------------------------------
   * Method: getConfiguration(params[, callback])
   * - params:
   *   - ConfigurationToken | String | required | a token of the targeted PTZ node
   *
   * No device I own does not work well for now.
   * ---------------------------------------------------------------- */
  getConfiguration(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetConfiguration',
      `<tptz:PTZConfigurationToken>${params.ConfigurationToken}</tptz:PTZConfigurationToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: getConfigurationOptions(params[, callback])
   * - params:
   *   - ConfigurationToken | String | required | a token of the targeted PTZ node
   * ---------------------------------------------------------------- */
  getConfigurationOptions(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetConfigurationOptions',
      `<tptz:ConfigurationToken>${params.ConfigurationToken}</tptz:ConfigurationToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: getStatus(params[, callback])
   * - params:
   *   - ProfileToken | String | required |
   * ---------------------------------------------------------------- */
  getStatus(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetStatus',
      `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: continuousMove(params[, callback])
   * - params:
   *   - ProfileToken | String  | required |
   *   - Velocity     | Object  | required | pan, tilt and zoom
   *     - x          | Float   | required |
   *     - y          | Float   | required |
   *     - x          | Float   | required |
   *   - Timeout      | Integer | optional | timeout (seconds)
   *
   * {
   *   'ProfileToken': 'Profile1',
   *   'Velocity': {'x': 0.5, 'y': 1.0, 'z': 1.0},
   *   'Timeout': 5
   * }
   * ---------------------------------------------------------------- */
  continuousMove(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.Velocity, 'object'))) {
      return Promise.reject(
        new Error(`The "Velocity" property was invalid: ${err_msg}`)
      );
    }

    const klist = ['x', 'y', 'z'];
    for (let i = 0; i < klist.length; i++) {
      const k = klist[i];
      const v = params.Velocity[k];
      if ((err_msg = mOnvifSoap.isInvalidValue(v, 'float'))) {
        return Promise.reject(
          new Error(`The "${k}" property was invalid: ${err_msg}`)
        );
      }
    }

    if ('Timeout' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.Timeout, 'integer'))) {
        return Promise.reject(
          new Error(`The "Timeout" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    body += `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`;
    body += '<tptz:Velocity>';
    body += `<tt:PanTilt x="${params.Velocity.x}" y="${params.Velocity.y}"></tt:PanTilt>`;
    if (params.Velocity.z) {
      body += `<tt:Zoom x="${params.Velocity.z}"></tt:Zoom>`;
    }
    body += '</tptz:Velocity>';
    if (params.Timeout) {
      body += `<tptz:Timeout>PT${params.Timeout}S</tptz:Timeout>`;
    }

    return this._createMethodRequestSoap('ContinuousMove', body);
  }

  /* ------------------------------------------------------------------
   * Method: absoluteMove(params[, callback])
   * - params:
   *   - ProfileToken | String  | required |
   *   - Position     | Object  | required | pan, tilt and zoom
   *     - x          | Float   | required |
   *     - y          | Float   | required |
   *     - x          | Float   | required |
   *   - Speed        | Object  | optional | pan, tilt and zoom
   *     - x          | Float   | required |
   *     - y          | Float   | required |
   *     - x          | Float   | required |
   *
   * {
   *   'ProfileToken': cam['ProfileToken'],
   *   'Position'    : {'x': 0.5, 'y': -1, 'z': 0.5},
   *   'Speed'       : {'x': 1, 'y': 1, 'z': 1}
   * }
   * ---------------------------------------------------------------- */
  absoluteMove(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.Position, 'object'))) {
      return Promise.reject(
        new Error(`The "Position" property was invalid: ${err_msg}`)
      );
    }

    const klist = ['x', 'y', 'z'];

    for (let i = 0; i < klist.length; i++) {
      const k = klist[i];
      const v = params.Position[k];
      if ((err_msg = mOnvifSoap.isInvalidValue(v, 'float'))) {
        return Promise.reject(
          new Error(`The "${k}" property was invalid: ${err_msg}`)
        );
      }
    }

    if ('Speed' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.Speed, 'object'))) {
        return Promise.reject(
          new Error(`The "Speed" property was invalid: ${err_msg}`)
        );
      }
      for (let i = 0; i < klist.length; i++) {
        const k = klist[i];
        const v = params.Speed[k];
        if ((err_msg = mOnvifSoap.isInvalidValue(v, 'float'))) {
          return Promise.reject(
            new Error(`The "${k}" property was invalid: ${err_msg}`)
          );
        }
      }
    }

    let body = '';

    body += `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`;

    body += '<tptz:Position>';
    body += `<tt:PanTilt x="${params.Position.x}" y="${params.Position.y}" />`;
    body += `<tt:Zoom x="${params.Position.z}"/>`;
    body += '</tptz:Position>';

    if (params.Speed) {
      body += '<tptz:Speed>';
      body += `<tt:PanTilt x="${params.Speed.x}" y="${params.Speed.y}" />`;
      body += `<tt:Zoom x="${params.Speed.z}"/>`;
      body += '</tptz:Speed>';
    }
    return this._createMethodRequestSoap('AbsoluteMove', body);
  }

  /* ------------------------------------------------------------------
   * Method: relativeMove(params[, callback])
   * - params:
   *   - ProfileToken | String  | required |
   *   - Translation  | Object  | required | pan, tilt and zoom
   *     - x          | Float   | required |
   *     - y          | Float   | required |
   *     - x          | Float   | required |
   *   - Speed        | Object  | optional | pan, tilt and zoom
   *     - x          | Float   | required |
   *     - y          | Float   | required |
   *     - x          | Float   | required |
   *
   * {
   *   'ProfileToken': 'Profile1',
   *   'Translation' : {'x': 0.5, 'y': 1.0, 'z': 1.0},
   *   'Speed'       : {'x': 1, 'y': 1, 'z': 1}
   * }
   * ---------------------------------------------------------------- */
  relativeMove(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.Translation, 'object'))) {
      return Promise.reject(
        new Error(`The "Translation" property was invalid: ${err_msg}`)
      );
    }

    const klist = ['x', 'y', 'z'];
    for (let i = 0; i < klist.length; i++) {
      const k = klist[i];
      const v = params.Translation[k];
      if ((err_msg = mOnvifSoap.isInvalidValue(v, 'float'))) {
        return Promise.reject(
          new Error(`The "${k}" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    body += `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`;
    body += '<tptz:Translation>';
    body += `<tt:PanTilt x="${params.Translation.x}" y="${params.Translation.y}"/>`;
    body += `<tt:Zoom x="${params.Translation.z}"/>`;
    body += '</tptz:Translation>';

    return this._createMethodRequestSoap('RelativeMove', body);
  }

  /* ------------------------------------------------------------------
   * Method: stop(params[, callback])
   * - params:
   *   - ProfileToken | String  | required | a token of the targeted PTZ node
   *   - PanTilt      | Boolean | optional | true or false
   *   - Zoom         | Boolean | optional | true or false
   *
   * {
   *   'ProfileToken': 'Profile1',
   *   'PanTilt': true,
   *   'Zoom': true
   * }
   * ---------------------------------------------------------------- */
  stop(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if ('PanTilt' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.PanTilt, 'boolean'))) {
        return Promise.reject(
          new Error(`The "PanTilt" property was invalid: ${err_msg}`)
        );
      }
    }

    if ('Zoom' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.Zoom, 'boolean'))) {
        return Promise.reject(
          new Error(`The "Zoom" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    body += `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`;
    if ('PanTilt' in params) {
      body += `<tptz:PanTilt>${params.PanTilt}</tptz:PanTilt>`;
    }
    if ('Zoom' in params) {
      body += `<tptz:Zoom>${params.Zoom}</tptz:Zoom>`;
    }

    return this._createMethodRequestSoap('Stop', body);
  }

  /* ------------------------------------------------------------------
   * Method: gotoHomePosition(params[, callback])
   * - params:
   *   - ProfileToken | String | required |
   *   - Speed        | Float  | optional |
   *
   * {
   *   'ProfileToken': 'Profile1',
   *   'Speed': 0.5
   * }
   * ---------------------------------------------------------------- */
  gotoHomePosition(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if ('Speed' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.Speed, 'float'))) {
        return Promise.reject(
          new Error(`The "Speed" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    body += `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`;
    if ('Speed' in params) {
      body += `<tptz:Speed>${params.Speed}</tptz:Speed>`;
    }

    return this._createMethodRequestSoap('GotoHomePosition', body);
  }

  /* ------------------------------------------------------------------
   * Method: setHomePosition(params[, callback])
   * - params:
   *   - ProfileToken | String | required |
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  setHomePosition(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'SetHomePosition',
      `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: setPreset(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the targeted PTZ node
   *   - PresetToken  | String | optional |
   *   - PresetName   | String | optional |
   *
   * {
   *   'ProfileToken': 'Profile1',
   *   'PresetName'  : 'Preset1'
   * }
   * ---------------------------------------------------------------- */
  setPreset(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if ('PresetToken' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.PresetToken, 'string'))) {
        return Promise.reject(
          new Error(`The "PresetToken" property was invalid: ${err_msg}`)
        );
      }
    }

    if ('PresetName' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.PresetName, 'string'))) {
        return Promise.reject(
          new Error(`The "PresetName" property was invalid: ${err_msg}`)
        );
      }
    }

    if (!('PresetToken' in params) && !('PresetName' in params)) {
      return Promise.reject(
        new Error(
          'Either the "ProfileToken" or the "PresetName" property must be specified.'
        )
      );
    }

    let body = '';

    body += `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`;
    if ('PresetToken' in params) {
      body += `<tptz:PresetToken>${params.PresetToken}</tptz:PresetToken>`;
    }
    if ('PresetName' in params) {
      body += `<tptz:PresetName>${params.PresetName}</tptz:PresetName>`;
    }

    return this._createMethodRequestSoap('SetPreset', body);
  }

  /* ------------------------------------------------------------------
   * Method: getPresets(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the targeted PTZ node
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getPresets(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetPresets',
      `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: gotoPreset(params[, callback])
   * - params:
   *   - ProfileToken | String  | required |
   *   - PresetToken  | String  | required |
   *   - Speed        | Object  | optional | pan, tilt and zoom
   *     - x          | Float   | required |
   *     - y          | Float   | required |
   *     - x          | Float   | required |
   *
   * {
   *   'ProfileToken': 'Profile1',
   *   'PresetToken' : 'Preset1',
   *   'Speed'       : {'x': 0.5, 'y': 1.0, 'z': 0.5}
   * }
   * ---------------------------------------------------------------- */
  gotoPreset(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.PresetToken, 'string'))) {
      return Promise.reject(
        new Error(`The "PresetToken" property was invalid: ${err_msg}`)
      );
    }

    const klist = ['x', 'y', 'z'];
    if ('Speed' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.Speed, 'object'))) {
        return Promise.reject(
          new Error(`The "Speed" property was invalid: ${err_msg}`)
        );
      }
      for (let i = 0; i < klist.length; i++) {
        const k = klist[i];
        const v = params.Speed[k];
        if ((err_msg = mOnvifSoap.isInvalidValue(v, 'float'))) {
          return Promise.reject(
            new Error(`The "${k}" property was invalid: ${err_msg}`)
          );
        }
      }
    }

    let body = '';

    body += `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`;
    body += `<tptz:PresetToken>${params.PresetToken}</tptz:PresetToken>`;
    if (params.Speed) {
      body += '<tptz:Speed>';
      body += `<tt:PanTilt x="${params.Speed.x}" y="${params.Speed.y}" />`;
      body += `<tt:Zoom x="${params.Speed.z}"/>`;
      body += '</tptz:Speed>';
    }

    return this._createMethodRequestSoap('GotoPreset', body);
  }

  /* ------------------------------------------------------------------
   * Method: removePreset(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the targeted PTZ node
   *   - PresetToken  | String | required |
   *
   * {
   *   'ProfileToken': 'Profile1',
   *   'PresetToken' : 'Preset1'
   * }
   * ---------------------------------------------------------------- */
  removePreset(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.PresetToken, 'string'))) {
      return Promise.reject(
        new Error(`The "PresetToken" property was invalid: ${err_msg}`)
      );
    }

    let body = '';

    body += `<tptz:ProfileToken>${params.ProfileToken}</tptz:ProfileToken>`;
    body += `<tptz:PresetToken>${params.PresetToken}</tptz:PresetToken>`;

    return this._createMethodRequestSoap('RemovePreset', body);
  }
}

module.exports = OnvifServicePtz;
