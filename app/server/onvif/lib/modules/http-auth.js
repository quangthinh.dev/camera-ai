/* eslint-disable no-plusplus */
/* eslint-disable no-underscore-dangle */
/* eslint-disable func-names */
/* eslint-disable prefer-destructuring */
/* ------------------------------------------------------------------
 * node-onvif - http-auth.js
 *
 * Copyright (c) 2016, Futomi Hatano, All rights reserved.
 * Released under the MIT license
 * Date: 2017-08-26
 * ---------------------------------------------------------------- */

const mHttp = require('http');
const mHttps = require('https');

const { createCnonce, createHash, parseAuthHeader } = require('./utils');

/* ------------------------------------------------------------------
 * Constructor: OnvifHttpAuth()
 * ---------------------------------------------------------------- */
class OnvifHttpAuth {
  constructor() {
    this.user = '';
    this.pass = '';
    this.method = '';
    this.path = '';
    this.nonce_count = 0;
    this.options = null;
  }

  /* ------------------------------------------------------------------
   * Method: request(options, callback)
   * ---------------------------------------------------------------- */
  request(options, callback) {
    this.options = JSON.parse(JSON.stringify(options));
    const http = options && options.protocol === 'https:' ? mHttps : mHttp;
    if (options.auth && typeof options.auth === 'string') {
      const pair = options.auth.split(':');
      this.user = pair[0];
      this.pass = pair[1];
    }
    // delete options.auth;
    if (options.method && typeof options.method === 'string') {
      this.method = options.method.toUpperCase();
    } else {
      this.method = 'GET';
    }
    if (options.path && typeof options.path === 'string') {
      this.path = options.path;
    }
    const req = http.request(options, res => {
      if (res.statusCode === 401 && res.headers['www-authenticate']) {
        if (res.headers['www-authenticate'].match(/Digest realm/)) {
          this._handleHttpDigest(http, res, callback);
        } else {
          callback(res);
        }
      } else {
        callback(res);
      }
    });
    return req;
  }

  _handleHttpDigest(http, res, callback) {
    const o = parseAuthHeader(res.headers['www-authenticate']);
    if (!this.options.headers) {
      this.options.headers = {};
    }
    this.options.headers.Authorization = this._createAuthReqHeaderValue(o);
    const req = http.request(this.options, callback);
    req.end();
  }

  _createAuthReqHeaderValue(o) {
    const ha1 = createHash(
      o.algorithm,
      [this.user, o['Digest realm'], this.pass].join(':')
    );
    const ha2 = createHash(o.algorithm, [this.method, this.path].join(':'));
    const cnonce = createCnonce(8);
    this.nonce_count++;
    const nc = `0000000${this.nonce_count.toString(16)}`.slice(-8);
    const response = createHash(
      o.algorithm,
      [ha1, o.nonce, nc, cnonce, o.qop, ha2].join(':')
    );

    let hvalue = [
      `username="${this.user}"`,
      `realm="${o['Digest realm']}"`,
      `nonce="${o.nonce}"`,
      `uri="${this.path}"`,
      `algorithm=${o.algorithm}`,
      `qop=${o.qop}`,
      `nc=${nc}`,
      `cnonce="${cnonce}"`,
      `response="${response}"`
    ].join(', ');
    hvalue = `Digest ${hvalue}`;
    return hvalue;
  }
}

module.exports = new OnvifHttpAuth();
