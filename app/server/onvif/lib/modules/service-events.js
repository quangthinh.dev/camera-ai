/* eslint-disable no-cond-assign */
/* eslint-disable camelcase */
/* eslint-disable no-underscore-dangle */
/* ------------------------------------------------------------------
 * node-onvif - service-events.js
 *
 * Copyright (c) 2016 - 2017, Futomi Hatano, All rights reserved.
 * Released under the MIT license
 * Date: 2017-08-26
 * ---------------------------------------------------------------- */

const ServiceBase = require('./service-base');

/* ------------------------------------------------------------------
 * Constructor: OnvifServiceEvents(params)
 * - params:
 *    - xaddr   : URL of the entry point for the media service
 *                (Required)
 *    - user  : User name (Optional)
 *    - pass  : Password (Optional)
 *    - time_diff: ms
 * ---------------------------------------------------------------- */
class OnvifServiceEvents extends ServiceBase {
  constructor(params) {
    super(params);
    this.name_space_attr_list = [
      'xmlns:wsa="http://www.w3.org/2005/08/addressing"',
      'xmlns:tev="http://www.onvif.org/ver10/events/wsdl"'
    ];
    this.name_space_method = 'tev';
  }

  /* ------------------------------------------------------------------
   * Method: getEventProperties([callback])
   * ---------------------------------------------------------------- */
  getEventProperties() {
    return this._createMethodRequestSoap('GetEventProperties');
  }
}

module.exports = OnvifServiceEvents;
