/* eslint-disable no-plusplus */
/* eslint-disable no-empty */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
/* eslint-disable promise/always-return */
/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
/* ------------------------------------------------------------------
 * node-onvif - soap.js
 *
 * Copyright (c) 2016-2018, Futomi Hatano, All rights reserved.
 * Released under the MIT license
 * Date: 2018-08-13
 * ---------------------------------------------------------------- */

const mHttp = require('http');
const mCrypto = require('crypto');
const mHtml = require('../../../utils/html');
const {
  parseSOAP,
  parseResponseResult,
  getFaultReason,
  getTypeOfValue
} = require('./utils');

/* ------------------------------------------------------------------
 * Constructor: OnvifSoap()
 * ---------------------------------------------------------------- */
class OnvifSoap {
  constructor(timeout = 3000) {
    this.HTTP_TIMEOUT = timeout; // ms
  }

  parse(xml) {
    return parseSOAP(xml);
  }

  /* ------------------------------------------------------------------
   * Method: requestCommand(oxaddr, method_name, soap)
   * ---------------------------------------------------------------- */
  async requestCommand(oxaddr, method_name, soap) {
    const xml = await this._request(oxaddr, soap);
    const result = await parseSOAP(xml);

    const fault = getFaultReason(result);
    if (fault) {
      throw new Error(fault);
    } else {
      const parsed = parseResponseResult(method_name, result);
      if (parsed) {
        const res = {
          soap: xml,
          formatted: mHtml ? mHtml.prettyPrint(xml, { indent_size: 2 }) : '',
          converted: result,
          data: parsed
        };
        return res;
      }
      throw new Error(
        `The device seems to not support the ${method_name}() method.`
      );
    }
  }

  _request(oxaddr, soap) {
    return new Promise((resolve, reject) => {
      const post_opts = {
        protocol: oxaddr.protocol,
        // auth    : oxaddr.auth,
        hostname: oxaddr.hostname,
        port: oxaddr.port || 80,
        path: oxaddr.pathname,
        method: 'POST',
        headers: {
          // 'Content-Type': 'application/soap+xml; charset=utf-8; action="http://www.onvif.org/ver10/device/wsdl/GetScopes"',
          'Content-Type': 'application/soap+xml; charset=utf-8;',
          'Content-Length': Buffer.byteLength(soap)
        }
      };

      let req = mHttp.request(post_opts, res => {
        res.setEncoding('utf8');
        let xml = '';
        res.on('data', chunk => {
          xml += chunk;
        });
        res.on('end', () => {
          if (req) {
            req.removeAllListeners('error');
            req.removeAllListeners('timeout');
            req = null;
          }
          if (res) {
            res.removeAllListeners('data');
            res.removeAllListeners('end');
          }
          if (res.statusCode === 200) {
            resolve(xml);
          } else {
            const err = new Error(`${res.statusCode} ${res.statusMessage}`);
            const code = res.statusCode;
            const text = res.statusMessage;
            if (xml) {
              parseSOAP(xml)
                .then(parsed => {
                  let msg = '';
                  try {
                    msg = parsed.Body.Fault.Reason.Text;
                    if (typeof msg === 'object') {
                      msg = msg._;
                    }
                  } catch {}
                  if (msg) {
                    reject(new Error(`${code} ${text} - ${msg}`));
                  } else {
                    reject(err);
                  }
                })
                .catch(reject);
            } else {
              reject(err);
            }
          }
          res = null;
        });
      });

      req.setTimeout(this.HTTP_TIMEOUT);

      req.on('timeout', () => {
        req.abort();
      });

      req.on('error', error => {
        req.removeAllListeners('error');
        req.removeAllListeners('timeout');
        req = null;
        reject(new Error(`Network Error: ${error ? error.message : ''}`));
      });

      req.write(soap, 'utf8');
      req.end();
    });
  }

  /* ------------------------------------------------------------------
   * Method: createRequestSoap(params)
   * - params:
   *   - body: description in the <s:Body>
   *   - xmlns: a list of xmlns attributes used in the body
   *       e.g., xmlns:tds="http://www.onvif.org/ver10/device/wsdl"
   *   - diff: Time difference [ms]
   *   - user: user name
   *   - pass: password
   * ---------------------------------------------------------------- */
  createRequestSoap(params) {
    let soap = '';
    soap += '<?xml version="1.0" encoding="UTF-8"?>';
    soap += '<s:Envelope';
    soap += '  xmlns:s="http://www.w3.org/2003/05/soap-envelope"';
    if (params.xmlns && Array.isArray(params.xmlns)) {
      params.xmlns.forEach(ns => {
        soap += ` ${ns}`;
      });
    }
    soap += '>';
    soap += '<s:Header>';
    if (params.user) {
      soap += this._createSoapUserToken(params.diff, params.user, params.pass);
    }
    soap += '</s:Header>';
    soap += `<s:Body>${params.body}</s:Body>`;
    soap += '</s:Envelope>';

    soap = soap.replace(/>\s+</g, '><');
    return soap;
  }

  _createSoapUserToken(diff, user, pass) {
    if (!diff) {
      diff = 0;
    }
    if (!pass) {
      pass = '';
    }
    const date = new Date(Date.now() + diff).toISOString();
    const nonce_buffer = this._createNonce(16);
    const nonce_base64 = nonce_buffer.toString('base64');
    const shasum = mCrypto.createHash('sha1');
    shasum.update(
      Buffer.concat([nonce_buffer, Buffer.from(date), Buffer.from(pass)])
    );
    const digest = shasum.digest('base64');
    let soap = '';
    soap +=
      '<Security s:mustUnderstand="1" xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">';
    soap += '  <UsernameToken>';
    soap += `    <Username>${user}</Username>`;
    soap += `    <Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">${digest}</Password>`;
    soap += `    <Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">${nonce_base64}</Nonce>`;
    soap += `    <Created xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">${date}</Created>`;
    soap += '  </UsernameToken>';
    soap += '</Security>';
    return soap;
  }

  _createNonce(digit) {
    const nonce = Buffer.alloc(digit);
    for (let i = 0; i < digit; i++) {
      nonce.writeUInt8(Math.floor(Math.random() * 256), i);
    }
    return nonce;
  }

  /* ------------------------------------------------------------------
   * Method: isInvalidValue(value, type, allow_empty)
   * - type: 'undefined', 'null', 'array', 'integer', 'float', 'boolean', 'object'
   * ---------------------------------------------------------------- */
  isInvalidValue(value, type, allow_empty) {
    const vt = getTypeOfValue(value);
    if (type === 'float') {
      if (!vt.match(/^(float|integer)$/)) {
        return `The type of the value must be "${type}".`;
      }
    } else if (vt !== type) {
      return `The type of the value must be "${type}".`;
    }

    if (!allow_empty) {
      if (vt === 'array' && value.length === 0) {
        return 'The value must not be an empty array.';
      }
      if (vt === 'string' && value === '') {
        return 'The value must not be an empty string.';
      }
    }
    if (typeof value === 'string') {
      if (value.match(/[^\x20-\x7e]/)) {
        return 'The value must consist of ascii characters.';
      }
      if (value.match(/[<>]/)) {
        return 'Invalid characters were found in the value ("<", ">")';
      }
    }
    return '';
  }
}

module.exports = new OnvifSoap();
