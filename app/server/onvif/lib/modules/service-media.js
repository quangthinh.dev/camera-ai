/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
/* eslint-disable no-cond-assign */
/* ------------------------------------------------------------------
 * node-onvif - service-media.js
 *
 * Copyright (c) 2016 - 2017, Futomi Hatano, All rights reserved.
 * Released under the MIT license
 * Date: 2017-08-26
 * ---------------------------------------------------------------- */

const mOnvifSoap = require('./soap.js');
const ServiceBase = require('./service-base');

/* ------------------------------------------------------------------
 * Constructor: OnvifServiceMedia(params)
 * - params:
 *    - xaddr   : URL of the entry point for the media service
 *                (Required)
 *    - user  : User name (Optional)
 *    - pass  : Password (Optional)
 *    - time_diff: ms
 * ---------------------------------------------------------------- */
class OnvifServiceMedia extends ServiceBase {
  constructor(params) {
    super(params);

    this.name_space_attr_list = [
      'xmlns:trt="http://www.onvif.org/ver10/media/wsdl"',
      'xmlns:tt="http://www.onvif.org/ver10/schema"'
    ];

    this.name_space_method = 'trt';
  }

  /* ------------------------------------------------------------------
   * Method: getStreamUri(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the profile
   *   - Protocol     | String | required | "UDP", "HTTP", or "RTSP"
   *
   * {
   *   'ProfileToken': 'Profile1,
   *   'Protocol'    : 'UDP'
   * }
   * ---------------------------------------------------------------- */
  getStreamUri(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.Protocol, 'string'))) {
      return Promise.reject(
        new Error(`The "Protocol" property was invalid: ${err_msg}`)
      );
    }
    if (!params.Protocol.match(/^(UDP|HTTP|RTSP)$/)) {
      return Promise.reject(
        new Error(
          'The "Protocol" property was invalid: The value must be either "UDP", "HTTP", or "RTSP".'
        )
      );
    }

    let body = '';

    body += '<trt:StreamSetup>';
    body += '<tt:Stream>RTP-Unicast</tt:Stream>';
    body += '<tt:Transport>';
    body += `<tt:Protocol>${params.Protocol}</tt:Protocol>`;
    body += '</tt:Transport>';
    body += '</trt:StreamSetup>';
    body += `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;

    return this._createMethodRequestSoap('GetStreamUri', body);
  }

  /* ------------------------------------------------------------------
   * Method: getVideoEncoderConfigurations([callback])
   * ---------------------------------------------------------------- */
  getVideoEncoderConfigurations() {
    return this._createMethodRequestSoap('GetVideoEncoderConfigurations');
  }

  /* ------------------------------------------------------------------
   * Method: getVideoEncoderConfiguration(params[, callback])
   * - params:
   *   - ConfigurationToken | String | required | a token of the configuration
   *
   * {
   *   'ConfigurationToken': 'Configuration1'
   * }
   * ---------------------------------------------------------------- */
  getVideoEncoderConfiguration(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetVideoEncoderConfiguration',
      `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: getCompatibleVideoEncoderConfigurations(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the profile
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getCompatibleVideoEncoderConfigurations(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetCompatibleVideoEncoderConfigurations',
      `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: getVideoEncoderConfigurationOptions(params[, callback])
   * - params:
   *   - ProfileToken       | String | optional | a token of the profile
   *   - ConfigurationToken | String | optional | a token of the configuration
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getVideoEncoderConfigurationOptions(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ('ProfileToken' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))
      ) {
        return Promise.reject(
          new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
        );
      }
    }

    if ('ConfigurationToken' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(
          params.ConfigurationToken,
          'string'
        ))
      ) {
        return Promise.reject(
          new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    if (params.ProfileToken) {
      body += `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    }
    if (params.ConfigurationToken) {
      body += `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;
    }

    return this._createMethodRequestSoap(
      'GetVideoEncoderConfigurationOptions',
      body
    );
  }

  /* ------------------------------------------------------------------
   * Method: getGuaranteedNumberOfVideoEncoderInstances(params[, callback])
   * - params:
   *   - ConfigurationToken | String | required | a token of the configuration
   *
   * {
   *   'ConfigurationToken': 'Configuration1'
   * }
   * ---------------------------------------------------------------- */
  getGuaranteedNumberOfVideoEncoderInstances(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetGuaranteedNumberOfVideoEncoderInstances',
      `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: getProfiles([callback])
   * ---------------------------------------------------------------- */
  getProfiles() {
    return this._createMethodRequestSoap('GetProfiles');
  }

  /* ------------------------------------------------------------------
   * Method: getProfile(params[, callback])
   * - params:
   *   - ProfileToken | required | a token of the profile
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getProfile(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetProfile',
      `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: createProfile(params[, callback])
   * - params:
   *   - Name  | String | required | a name of the profile
   *   - Token | String | optional | a token of the profile
   *
   * {
   *   'Name: 'TestProfile1'
   * }
   * ---------------------------------------------------------------- */
  createProfile(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.Name, 'string'))) {
      return Promise.reject(
        new Error(`The "Name" property was invalid: ${err_msg}`)
      );
    }

    if ('Token' in params) {
      if ((err_msg = mOnvifSoap.isInvalidValue(params.Token, 'string'))) {
        return Promise.reject(
          new Error(`The "Token" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    body += `<trt:Name>${params.Name}</trt:Name>`;
    if ('Token' in params) {
      body += `<trt:Token>${params.Token}</trt:Token>`;
    }

    return this._createMethodRequestSoap('CreateProfile', body);
  }

  /* ------------------------------------------------------------------
   * Method: deleteProfile(params[, callback])
   * - params:
   *   - ProfileToken | String | required |
   *
   * {
   *   'ProfileToken: 'TestProfile1'
   * }
   * ---------------------------------------------------------------- */
  deleteProfile(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'DeleteProfile',
      `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: getVideoSources([callback])
   * ---------------------------------------------------------------- */
  getVideoSources() {
    return this._createMethodRequestSoap('GetVideoSources');
  }

  /* ------------------------------------------------------------------
   * Method: getVideoSourceConfiguration(params[, callback])
   * - params:
   *   - ConfigurationToken | String | required |
   *
   * {
   *   'ConfigurationToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getVideoSourceConfiguration(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetVideoSourceConfiguration',
      `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: getVideoSourceConfigurations([callback])
   * ---------------------------------------------------------------- */
  getVideoSourceConfigurations() {
    return this._createMethodRequestSoap('GetVideoSourceConfigurations');
  }

  /* ------------------------------------------------------------------
   * Method: addVideoSourceConfiguration(params[, callback])
   * - params:
   *   - ProfileToken       | String | required | a token of the Profile
   *   - ConfigurationToken | String | required |
   *
   * {
   *   'ProfileToken': 'Profile1'
   *   'ConfigurationToken': 'Profile1'
   * }
   *
   * No device I own does not support this command
   * ---------------------------------------------------------------- */
  addVideoSourceConfiguration(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    let body = '';

    body += `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    body += `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;

    return this._createMethodRequestSoap('AddVideoSourceConfiguration', body);
  }

  /* ------------------------------------------------------------------
   * Method: getCompatibleVideoSourceConfigurations(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the targeted PTZ node
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getCompatibleVideoSourceConfigurations(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    return this._createMethodRequestSoap(
      'GetCompatibleVideoSourceConfigurations',
      `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`
    );
  }

  /* ------------------------------------------------------------------
   * Method: getVideoSourceConfigurationOptions(params[, callback])
   * - params:
   *   - ProfileToken       | optional | a token of the Profile
   *   - ConfigurationToken | optional | a token of the configuration
   *
   * {
   *   'ProfileToken': 'Profile1'
   *   'ConfigurationToken': 'Conf1'
   * }
   * ---------------------------------------------------------------- */
  getVideoSourceConfigurationOptions(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ('ProfileToken' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))
      ) {
        return Promise.reject(
          new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
        );
      }
    }

    if ('ConfigurationToken' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(
          params.ConfigurationToken,
          'string'
        ))
      ) {
        return Promise.reject(
          new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    if ('ProfileToken' in params) {
      body += `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    }
    if ('ConfigurationToken' in params) {
      body += `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;
    }

    return this._createMethodRequestSoap(
      'GetVideoSourceConfigurationOptions',
      body
    );
  }

  /* ------------------------------------------------------------------
   * Method: getMetadataConfiguration(params[, callback])
   * - params:
   *   - ConfigurationToken | required |
   *
   * {
   *   'ConfigurationToken': 'Conf1'
   * }
   * ---------------------------------------------------------------- */
  getMetadataConfiguration(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    const body = `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;

    return this._createMethodRequestSoap('GetMetadataConfiguration', body);
  }

  /* ------------------------------------------------------------------
   * Method: getMetadataConfigurations([callback])
   * ---------------------------------------------------------------- */
  getMetadataConfigurations() {
    return this._createMethodRequestSoap('GetMetadataConfigurations');
  }

  /* ------------------------------------------------------------------
   * Method: addMetadataConfiguration(params[, callback])
   * - params:
   *   - ProfileToken       | String | required | a token of the Profile
   *   - ConfigurationToken | String | required |
   *
   * {
   *   'ProfileToken': 'Profile1'
   *   'ConfigurationToken': 'Conf1'
   * }
   *
   * No device I own does not support this command
   * ---------------------------------------------------------------- */
  addMetadataConfiguration(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    let body = '';

    body += `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    body += `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;

    return this._createMethodRequestSoap('AddMetadataConfiguration', body);
  }

  /* ------------------------------------------------------------------
   * Method: getCompatibleMetadataConfigurations(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the Profile
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getCompatibleMetadataConfigurations(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    const body = `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;

    return this._createMethodRequestSoap(
      'GetCompatibleMetadataConfigurations',
      body
    );
  }

  /* ------------------------------------------------------------------
   * Method: getMetadataConfigurationOptions(params[, callback])
   * - params:
   *   - ProfileToken       | String | optional | a token of the Profile
   *   - ConfigurationToken | String | optional |
   *
   * {
   *   'ProfileToken': 'Profile1'
   *   'ConfigurationToken': 'Conf1'
   * }
   * ---------------------------------------------------------------- */
  getMetadataConfigurationOptions(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ('ProfileToken' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))
      ) {
        return Promise.reject(
          new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
        );
      }
    }

    if ('ConfigurationToken' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(
          params.ConfigurationToken,
          'string'
        ))
      ) {
        return Promise.reject(
          new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    if ('ProfileToken' in params) {
      body += `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    }
    if ('ConfigurationToken' in params) {
      body += `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;
    }

    return this._createMethodRequestSoap(
      'GetMetadataConfigurationOptions',
      body
    );
  }

  /* ------------------------------------------------------------------
   * Method: getAudioSources([callback])
   * ---------------------------------------------------------------- */
  getAudioSources() {
    return this._createMethodRequestSoap('GetAudioSources');
  }

  /* ------------------------------------------------------------------
   * Method: getAudioSourceConfiguration(params[, callback])
   * - params:
   *   - ConfigurationToken | String | required |
   *
   * {
   *   'ConfigurationToken': 'Conf1'
   * }
   * ---------------------------------------------------------------- */
  getAudioSourceConfiguration(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    const body = `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;

    return this._createMethodRequestSoap('GetAudioSourceConfiguration', body);
  }

  /* ------------------------------------------------------------------
   * Method: getAudioSourceConfigurations([callback])
   * ---------------------------------------------------------------- */
  getAudioSourceConfigurations() {
    return this._createMethodRequestSoap('GetAudioSourceConfigurations');
  }

  /* ------------------------------------------------------------------
   * Method: addAudioSourceConfiguration(params[, callback])
   * - params:
   *   - ProfileToken       | String | required | a token of the Profile
   *   - ConfigurationToken | String | required |
   *
   * {
   *   'ProfileToken': 'Profile1',
   *   'ConfigurationToken': 'Conf1'
   * }
   *
   * No device I own does not support this command
   * ---------------------------------------------------------------- */
  addAudioSourceConfiguration(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    let body = '';

    body += `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    body += `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;

    return this._createMethodRequestSoap('AddAudioSourceConfiguration', body);
  }

  /* ------------------------------------------------------------------
   * Method: getCompatibleAudioSourceConfigurations(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the profile
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getCompatibleAudioSourceConfigurations(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    const body = `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;

    return this._createMethodRequestSoap(
      'GetCompatibleAudioSourceConfigurations',
      body
    );
  }

  /* ------------------------------------------------------------------
   * Method: getAudioSourceConfigurationOptions(params[, callback])
   * - params:
   *   - ProfileToken       | String | optional | a token of the Profile
   *   - ConfigurationToken | String | optional |
   *
   * {
   *   'ProfileToken': 'Profile1'
   *   'ConfigurationToken': 'Conf1'
   * }
   * ---------------------------------------------------------------- */
  getAudioSourceConfigurationOptions(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ('ProfileToken' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))
      ) {
        return Promise.reject(
          new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
        );
      }
    }

    if ('ConfigurationToken' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(
          params.ConfigurationToken,
          'string'
        ))
      ) {
        return Promise.reject(
          new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    if ('ProfileToken' in params) {
      body += `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    }
    if ('ConfigurationToken' in params) {
      body += `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;
    }

    return this._createMethodRequestSoap(
      'GetAudioSourceConfigurationOptions',
      body
    );
  }

  /* ------------------------------------------------------------------
   * Method: getAudioEncoderConfiguration(params[, callback])
   * - params:
   *   - ConfigurationToken | String | required |
   *
   * {
   *   'ConfigurationToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getAudioEncoderConfiguration(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    const body = `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;

    return this._createMethodRequestSoap('GetAudioEncoderConfiguration', body);
  }

  /* ------------------------------------------------------------------
   * Method: getAudioEncoderConfigurations([callback])
   * ---------------------------------------------------------------- */
  getAudioEncoderConfigurations() {
    return this._createMethodRequestSoap('GetAudioEncoderConfigurations');
  }

  /* ------------------------------------------------------------------
   * Method: addAudioEncoderConfiguration(params[, callback])
   * - params:
   *   - ProfileToken       | String | required | a token of the Profile
   *   - ConfigurationToken | String | required |
   *
   * {
   *   'ProfileToken': 'Profile1',
   *   'ConfigurationToken': 'Conf1'
   * }
   *
   * Not device I own does not support this command
   * ---------------------------------------------------------------- */
  addAudioEncoderConfiguration(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    if (
      (err_msg = mOnvifSoap.isInvalidValue(params.ConfigurationToken, 'string'))
    ) {
      return Promise.reject(
        new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
      );
    }

    let body = '';

    body += `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    body += `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;

    return this._createMethodRequestSoap('AddAudioEncoderConfiguration', body);
  }

  /* ------------------------------------------------------------------
   * Method: getCompatibleAudioEncoderConfigurations(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the profile
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getCompatibleAudioEncoderConfigurations(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    const body = `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;

    return this._createMethodRequestSoap(
      'GetCompatibleAudioEncoderConfigurations',
      body
    );
  }

  /* ------------------------------------------------------------------
   * Method: getAudioEncoderConfigurationOptions(params[, callback])
   * - params:
   *   - ProfileToken       | String | optional | a token of the Profile
   *   - ConfigurationToken | String | optional |
   *
   * {
   *   'ProfileToken': 'Profile1'
   *   'ConfigurationToken': 'Conf1'
   * }
   * ---------------------------------------------------------------- */
  getAudioEncoderConfigurationOptions(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ('ProfileToken' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))
      ) {
        return Promise.reject(
          new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
        );
      }
    }

    if ('ConfigurationToken' in params) {
      if (
        (err_msg = mOnvifSoap.isInvalidValue(
          params.ConfigurationToken,
          'string'
        ))
      ) {
        return Promise.reject(
          new Error(`The "ConfigurationToken" property was invalid: ${err_msg}`)
        );
      }
    }

    let body = '';

    if ('ProfileToken' in params) {
      body += `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    }
    if ('ConfigurationToken' in params) {
      body += `<trt:ConfigurationToken>${params.ConfigurationToken}</trt:ConfigurationToken>`;
    }

    return this._createMethodRequestSoap(
      'GetAudioEncoderConfigurationOptions',
      body
    );
  }

  /* ------------------------------------------------------------------
   * Method: startMulticastStreaming(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the Profile
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   *
   * No device I own does not support this command
   * ---------------------------------------------------------------- */
  startMulticastStreaming(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    const body = `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;

    return this._createMethodRequestSoap('StartMulticastStreaming', body);
  }

  /* ------------------------------------------------------------------
   * Method: stopMulticastStreaming(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the Profile
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   *
   * No device I own does not support this command
   * ---------------------------------------------------------------- */
  stopMulticastStreaming(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    const body = `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    return this._createMethodRequestSoap('StopMulticastStreaming', body);
  }

  /* ------------------------------------------------------------------
   * Method: getSnapshotUri(params[, callback])
   * - params:
   *   - ProfileToken | String | required | a token of the Profile
   *
   * {
   *   'ProfileToken': 'Profile1'
   * }
   * ---------------------------------------------------------------- */
  getSnapshotUri(params) {
    let err_msg = '';
    if ((err_msg = mOnvifSoap.isInvalidValue(params, 'object'))) {
      return Promise.reject(
        new Error(`The value of "params" was invalid: ${err_msg}`)
      );
    }

    if ((err_msg = mOnvifSoap.isInvalidValue(params.ProfileToken, 'string'))) {
      return Promise.reject(
        new Error(`The "ProfileToken" property was invalid: ${err_msg}`)
      );
    }

    const body = `<trt:ProfileToken>${params.ProfileToken}</trt:ProfileToken>`;
    return this._createMethodRequestSoap('GetSnapshotUri', body);
  }
}

module.exports = OnvifServiceMedia;
