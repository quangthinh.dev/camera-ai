/* eslint-disable prefer-destructuring */
/* eslint-disable import/prefer-default-export */
import http from 'http';
import IPCamClient from '../dvrip';
import ONVIFClient from '../rtsp-stream/ONVIFClient';
import mOnvifSoap from './lib/modules/soap';

const soap = mOnvifSoap.createRequestSoap({
  body: '<tds:GetDeviceInformation/>',
  xmlns: [
    'xmlns:tds="http://www.onvif.org/ver10/device/wsdl"',
    'xmlns:tt="http://www.onvif.org/ver10/schema"'
  ],
  diff: 0,
  user: '',
  pass: ''
});

const soapHeader = {
  'Content-Type': 'application/soap+xml; charset=utf-8;',
  'Content-Length': Buffer.byteLength(soap)
};

export const emitScanItem = (
  data,
  protocol = 'http:',
  path = '/onvif/device_service',
  timeout = 1000
) =>
  new Promise((resolve, reject) => {
    // console.log(data);
    let url = new URL(`${protocol}//${data.ip}`);
    url.pathname = path;
    url.port = data.port;
    url = url.toString();
    let name = data.ip;
    let req = http.request(
      {
        method: 'POST',
        timeout,
        protocol,
        hostname: data.ip,
        port: data.port,
        path,
        headers: soapHeader
      },

      res => {
        // should be zero content length
        // console.log(res.statusCode, res.headers);
        if (res.statusCode === 200) {
          // console.log(res.body)
          let xml = '';
          res
            .on('data', chunk => {
              xml += chunk;
            })
            .on('end', async () => {
              const obj = await mOnvifSoap.parse(xml);
              if (!obj.Body) {
                // console.log(url);
                reject(obj);
                return;
              }
              if (obj.Body.GetDeviceInformationResponse) {
                name = obj.Body.GetDeviceInformationResponse.Model;
              }

              return resolve({
                // ...data,
                name,
                url
              });
            });
          // unauthorized
        } else if (res.statusCode === 401) {
          if (res.headers['www-authenticate']) {
            const match = res.headers['www-authenticate'].match(
              /realm="([^"]+)"/
            );
            if (match) name = match[1];
          }

          resolve({
            // ...data,
            name,
            url
          });
        } else {
          // default handle
          const err = new Error('HTTP connection');
          err.code = res.statusCode;
          reject(err);
        }
      }
    );
    req.setTimeout(mOnvifSoap.HTTP_TIMEOUT);

    req.on('timeout', () => {
      req.abort();
    });

    req.on('error', err => {
      req.removeAllListeners('error');
      req.removeAllListeners('timeout');
      // reject
      reject(err);
      req = null;
    });

    req.write(soap, 'utf8');
    req.end();
  });

export const createInnerCamDefault = (connectedDevice, isIPCam) => {
  return isIPCam
    ? new IPCamClient(connectedDevice)
    : new ONVIFClient(connectedDevice);
};
