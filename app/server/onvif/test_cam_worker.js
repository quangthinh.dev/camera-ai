import { fork, spawn } from 'child_process';
import WebSocket from 'ws';
import { Writable } from 'stream';

class Test extends Writable {
  constructor({ port }) {
    super();
    this.port = port;
    this.start = Date.now();
  }

  init() {
    this.wss = new WebSocket.Server({ port: this.port });
    const elapsed = Date.now() - this.start;
    console.log(`Server ready on port ${this.port}`, 'take ', elapsed, 'ms');

    this.wss.on('connection', ws => {
      ws.send(this.firstBuffer);
      ws.on('error', error => {
        console.log('WebSocket error', error);
      });
    });
  }

  write(chunk) {
    if (!this.firstBuffer) {
      this.firstBuffer = chunk;
      return this.init();
    }
    // console.log(chunk);
    // if (chunk.length < 100) return;
    this.wss.clients.forEach(ws => {
      ws.send(chunk);
    });
  }
}

const wrapPipe = () => {
  const spawnOptions = [
    '-threads',
    '1',
    '-hide_banner',
    '-loglevel',
    'panic',
    '-fflags',
    'nobuffer',
    '-fflags',
    '+genpts',
    '-i',
    'pipe:',
    '-c',
    'copy',
    '-an',
    '-f',
    'flv',
    'pipe:1'
  ];
  const ffmpeg = spawn('ffmpeg', spawnOptions);

  const test = new Test({ port: 3000 });
  ffmpeg.stdout.pipe(test);

  return ffmpeg;
};

const run = async () => {
  const camWorker = fork(require.resolve('./cam-worker.js'), {
    serialization: 'advanced'
  });
  const url = 'rtsp://nguyenchanh.cameraddns.net:1554/Streaming/Channels/101/';
  camWorker.send({
    cmd: 'init',
    data: {
      connectedDevice: {
        url,
        user: 'demo',
        pass: 'admin123',
        profile: { stream: url }
      },
      isIPCam: false
    }
  });

  const ffmpeg = wrapPipe();

  camWorker.on('message', ({ cmd, data }) => {
    if (cmd === 'pipe') {
      const { buffer } = data;
      ffmpeg.stdin.write(buffer);
    }
  });
};

run();
