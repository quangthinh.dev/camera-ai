/* eslint-disable prefer-destructuring */
import Evilscan from 'evilscan';
import Onvif from './lib/node-onvif';
// also singleton
import logger from '../logger';
import { fixURL } from '../utils';
import { emitScanItem } from './utils';

// var options = {
//     target:'127.0.0.1',
//     port:'21-23',
//     status:'TROU', // Timeout, Refused, Open, Unreachable
//     banner:true
// };

// if use array then this is not this Onvif
Onvif.prototype.startScan = function startScan(options) {
  // default scan
  if (!options || !options.target) {
    // scan with a range ?
    if (!this.probePromise) {
      this.probePromise = this.startProbe().then(deviceList => {
        delete this.probePromise;
        logger.log('Scan onvif finished!');
        return deviceList.map(device => ({
          url: fixURL(device.xaddrs[0]),
          name: device.name
        }));
      });
    }

    return this.probePromise;
  }

  // reuse
  if (this.scanner) {
    // already running
    return new Promise(resolve => this.scanner.on('resolve', resolve));
  }

  // create promise
  if (!options.onSuccess) {
    return new Promise((resolve, reject) => {
      this.startScan({
        ...options,
        onSuccess: resolve,
        onFailure: reject
      });
    });
  }

  // renew scanner, maximum is 1024 on linux by default
  this.scanner = new Evilscan({ ...options, concurrency: 1000, status: 'O' });
  const result = [];

  this.scanner.on('result', data => {
    if (data.status === 'open') result.push(data);
  });

  this.scanner.on('error', err => {
    if (options.onFailure) options.onFailure(err);
  });

  this.scanner.on('done', async () => {
    const ret = [];

    await Promise.all(
      result.map(async data => {
        try {
          const item = await emitScanItem(data);
          if (options.onData) options.onData(item);
          ret.push(item);
        } catch (ex) {
          if (options.onError) options.onError(ex);
        }
      })
    );

    this.result = ret;

    // finished !
    this.stopScan(options.onSuccess);
  });

  this.scanner.run();
};

Onvif.prototype.stopScan = function stopScan(callback) {
  if (this.scanner) {
    this.scanner.removeAllListeners('result');
    this.scanner.removeAllListeners('error');
    this.scanner.removeAllListeners('done');
    this.scanner.abort();
    // trigger

    if (callback) {
      callback(this.result);
    }

    logger.log('Scan onvif finished!');
    this.scanner.emit('resolve', this.result);
    delete this.result;
    delete this.scanner;
  }
};

// singleton
export default new Onvif();
