/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable no-async-promise-executor */
/* eslint-disable no-underscore-dangle */
import logger from '../logger';

import {
  MAIN_STREAM,
  SUB_STREAM
  // probesize
} from '../constants/stream.json';
import { createInnerCamDefault } from './utils';
// this time onvif and ipcam has the same functions
export default class Cam {
  constructor({
    connectedDevice,
    isIPCam,
    keepAliveTime = 10000, // same as timeout command
    createInnerCam = createInnerCamDefault,
    shouldStopOnRetry
  }) {
    // let retry = 0;

    this.isIPCam = isIPCam;

    this.mainPipes = { video: new Set(), audio: new Set() };
    this.subPipes = { video: new Set(), audio: new Set() };

    this.innerCam = createInnerCam(connectedDevice, isIPCam);
    let firstTime = true;

    if (keepAliveTime) {
      const keepAlive = async () => {
        // retry 1 time
        if (!this.IsConnected) {
          // try {
          // incase connect loop forever
          // await connectedDevice.updateProfile();
          // const camHasChanged = await connectedDevice.recheckCam();
          // if (camHasChanged) return;
          if (!firstTime && shouldStopOnRetry) {
            const shouldStop = await shouldStopOnRetry(connectedDevice);
            if (shouldStop) {
              // stop rather than retry later
              logger.log('Cam has been removed', connectedDevice.url);
              return this.destroy();
            }
          }

          await this.connect();
          firstTime = false;
          // } catch (err) {
          //   logger.log('Cam retry error', connectedDevice.url, err);
          // }
        }

        this.keepAliveTimer = setTimeout(keepAlive, keepAliveTime);
      };
      keepAlive();
    }
  }

  getPipes(type, StreamType) {
    return (StreamType === MAIN_STREAM ? this.mainPipes : this.subPipes)[type];
  }

  pipeStream(type, StreamType, stream) {
    if (stream) this.getPipes(type, StreamType).add(stream);
  }

  unpipeStream(type, StreamType, stream) {
    if (stream) this.getPipes(type, StreamType).delete(stream);
  }

  pipeVideo(StreamType, stream) {
    return this.pipeStream('video', StreamType, stream);
  }

  unpipeVideo(StreamType, stream) {
    return this.unpipeStream('video', StreamType, stream);
  }

  pipeAudio(StreamType, stream) {
    return this.pipeStream('audio', StreamType, stream);
  }

  unpipeAudio(StreamType, stream) {
    return this.unpipeStream('audio', StreamType, stream);
  }

  unpipeAll() {
    this.unpipeStream('video', MAIN_STREAM);
    this.unpipeStream('audio', MAIN_STREAM);
    this.unpipeStream('video', SUB_STREAM);
    this.unpipeStream('audio', SUB_STREAM);
  }

  async destroy() {
    // unbind all then disconnect
    if (this.keepAliveTimer) clearTimeout(this.keepAliveTimer);
    this.unpipeAll();
    // disconnect
    await this.disconnect();
  }

  // should init only 1 time
  async init() {
    await this.connect();
  }

  // repipe all streams
  pipeMessage(type, StreamType) {
    const inputStream = this.getVideoStream(StreamType)[type];
    if (inputStream) {
      const pipes = this.getPipes(type, StreamType);
      // broad cast event for each pipes, so no blocking buffer
      // let probeBuffer;
      // accumulating probesize for ffmpeg to encode and decode
      inputStream.on('data', buffer => {
        // first time
        // if (!probeBuffer) {
        //   probeBuffer = buffer;
        //   return;
        // }

        // // not enough
        // if (probeBuffer.length < probesize) {
        //   probeBuffer = Buffer.concat([probeBuffer, buffer]);
        //   return;
        // }

        // now ok
        pipes.forEach(stream => {
          // if destroy then delete, undefined or null is ok
          if (stream.destroyed) {
            pipes.delete(stream);
          } else {
            // this help decode header, and even write a new file stream
            // if (!stream.inited) {
            //   stream.inited = true;
            //   stream.write(probeBuffer);
            // }
            stream.write(buffer);
          }
        });
      });
    }
  }

  pipeMessageAll() {
    this.pipeMessage('video', MAIN_STREAM);
    this.pipeMessage('audio', MAIN_STREAM);
    this.pipeMessage('video', SUB_STREAM);
    this.pipeMessage('audio', SUB_STREAM);
  }

  async connect() {
    // prevent call multiple times
    if (this.connecting) return;
    this.connecting = true;

    const { protocol, url } = this.innerCam.connectedDevice;

    // do not let connect throw error, so claim one stream does not affect other
    await this.innerCam.connect(protocol);
    if (this.IsConnected) {
      logger.log('connected', url);
      this.pipeMessageAll();
    }
    this.connecting = false;
  }

  get IsConnected() {
    return this.innerCam.IsConnected;
  }

  async disconnect() {
    try {
      await this.innerCam.close();
      return true;
    } catch (ex) {
      console.log(ex);
      return false;
    }
  }

  getVideoStream(StreamType) {
    return this.innerCam.getVideoStream(StreamType);
  }
}
