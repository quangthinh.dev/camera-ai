/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
/* eslint-disable max-classes-per-file */
/* eslint-disable no-lonely-if */
/* eslint-disable no-underscore-dangle */
import { EventEmitter } from 'events';
import logger from '../logger';
import onvif from './index';
import {
  getSize,
  getPTZMove,
  createPromiseTimeout
  // fixURL,
} from '../utils';
import Cam from './cam';
import CamMaster from './cam-master';
import { MAIN_STREAM, SUB_STREAM } from '../constants/stream.json';

const isModelIPCam = Model => {
  // return false;
  return (
    Model &&
    (Model.startsWith('HI') ||
      Model.startsWith('53H') ||
      Model.startsWith('50H'))
  );
};

/**
 * useWorker : ['process', 'thread', false]
 */
export default class ConnectedDevice extends EventEmitter {
  constructor({
    url,
    getFlvStream,
    useWorker = false,
    protocol = 'TCP',
    isIPCam,
    shouldStopOnRetry
  }) {
    super();
    this.url = url;
    this.protocol = protocol;
    this.useWorker = useWorker;
    this.getFlvStream = getFlvStream;
    this.isIPCam = isIPCam;
    this.shouldStopOnRetry = shouldStopOnRetry;
  }

  async createCam(isIPCam) {
    const cam = this.useWorker
      ? new CamMaster(this, isIPCam, this.useWorker)
      : new Cam({
          connectedDevice: this,
          isIPCam,
          shouldStopOnRetry: this.shouldStopOnRetry
        });
    await cam.init();
    return cam;
  }

  async init({ user, pass } = {}) {
    // update user and pass
    const loginChanged = user !== this.user || pass !== this.pass;
    // already connected and nothing changed, for onvif it will re-create
    // for ip it will re-login

    // update login info
    this.user = user;
    this.pass = pass;

    // for later re-connect
    if (this.device) {
      // this.device.user = user;
      // this.device.pass = pass;
      // await this.updateProfile();

      // nothing changed, return
      if (!loginChanged) {
        return;
      }

      // close before re-init
      await this.close();
    }

    // already init
    if (this.cam) {
      return;
    }

    if (this.url.startsWith('rtsp')) {
      this.cam = await this.createCam(false);
      // by default extract from url
    } else {
      // create device for first time and update profile

      if (!this.device) {
        this.device = new onvif.OnvifDevice({
          xaddr: this.url,
          user,
          pass
        });
        await this.updateProfile();
      }
      // console.log({ isIPCam });

      // try init to create cam again
      // let isIPCam = !!camCheck[this.url];
      // first time
      if (this.isIPCam === undefined) {
        const { Model } = this.info;
        // HIK VISSION
        this.isIPCam = isModelIPCam(Model);
        // camCheck[this.url] = isIPCam;
      }

      // always have cam
      this.cam = await this.createCam(this.isIPCam);
    }

    return this.cam;
  }

  get info() {
    return this._info || {};
  }

  async updateProfile(timeout = 10000) {
    try {
      this._info = await Promise.race([
        this.device.init(),
        createPromiseTimeout(timeout)
      ]);
      this._profile = null;
      this.emit('profileChanged', this.profile);
    } catch (error) {
      logger.log(error, this.url);
    }
  }

  get profile() {
    if (!this._profile) {
      let profile;
      if (this.device) {
        profile = { ...this.info, url: this.url };
        const profiles = this.device
          .getProfileList()
          .map(item => ({
            stream: item.stream.rtsp || item.stream.udp || item.stream.http,
            snapshot: item.snapshot,
            size: getSize(item.video.encoder.resolution)
          }))
          .sort((a, b) => b.size - a.size);

        if (profiles.length > 1) {
          // this is for maximum

          profile.mainSnapshot = profiles[0].snapshot;
          profile.snapshot = profiles[1].snapshot;
          profile.mainStream = profiles[0].stream;
          profile.stream = profiles[1].stream;

          // we define extra and main stream
          if (this.getFlvStream) {
            profile.flvStream = this.getFlvStream(this.url, SUB_STREAM);
            profile.mainFlvStream = this.getFlvStream(this.url, MAIN_STREAM);
          }
        } else {
          // default
          profile.mainStream = profile.stream = this.device.getUdpStreamUrl();
          const defaultProfile = this.device.getCurrentProfile();
          if (defaultProfile) {
            profile.mainSnapshot = profile.snapshot = defaultProfile.snapshot;
          }
          if (this.getFlvStream) {
            profile.mainFlvStream = profile.flvStream = this.getFlvStream(
              this.url,
              SUB_STREAM
            );
          }
        }
      } else {
        profile = {
          stream: this.url,
          flvStream: this.getFlvStream(this.url, SUB_STREAM)
        };
        profile.mainFlvStream = profile.flvStream;
        profile.mainStream = profile.stream;
      }

      this._profile = profile;
    }

    return this._profile;
  }

  move(action, distance, timeout) {
    if (!this.device) return;
    const move = getPTZMove(action, distance);
    if (timeout) {
      setTimeout(() => this.device.ptzStop(), timeout);
    }
    return this.device.ptzMove(move);
  }

  async close() {
    if (this.cam) {
      await this.cam.destroy();
      this.cam = null;
    }
    // delete device
    this.device = null;
  }
}
