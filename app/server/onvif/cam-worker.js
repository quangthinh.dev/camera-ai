import { parentPort, isMainThread } from 'worker_threads';
import logger from '../logger';
import { createInnerCamDefault } from './utils';
import { MAIN_STREAM, SUB_STREAM } from '../constants/stream.json';

// do something for worker ?
/* eslint-disable no-plusplus */
/* eslint-disable no-async-promise-executor */
/* eslint-disable no-underscore-dangle */

// wrapper of both onvif client stream and idpr stream
let cam;

const handleMessage = ({ cmd, data }) => {
  switch (cmd) {
    case 'init':
      return initCam(data);
    case 'close':
      return disconnect();
    case 'profile':
      return updateProfile(data);
    default:
      break;
  }
};

let sendMessage;

if (isMainThread) {
  process.on('message', handleMessage);
  sendMessage = process.send.bind(process);
} else {
  parentPort.on('message', handleMessage);
  sendMessage = parentPort.postMessage.bind(parentPort);
}

let keepAliveTimer;
const keepAlive = async () => {
  // retry 1 time
  if (!cam.IsConnected) {
    // incase connect loop forever
    await connect();
  }

  keepAliveTimer = setTimeout(keepAlive, 10000);
};

const initCam = ({ connectedDevice, isIPCam }) => {
  cam = createInnerCamDefault(connectedDevice, isIPCam);

  // logger.log(isIPCam, connectedDevice.url);
  keepAlive();
};

const updateProfile = profile => {
  cam.connectedDevice.profile = profile;
};

const pipeMessage = (type, StreamType) => {
  const inputStream = cam.getVideoStream(StreamType);
  if (inputStream[type]) {
    inputStream[type].on('data', buffer => {
      if (sendMessage && cam.IsConnected)
        sendMessage({
          cmd: 'pipe',
          data: { StreamType, type, buffer }
        });
    });
  }
};

const pipeMessageAll = () => {
  pipeMessage('video', MAIN_STREAM);
  pipeMessage('audio', MAIN_STREAM);
  pipeMessage('video', SUB_STREAM);
  pipeMessage('audio', SUB_STREAM);
};

let connecting = false;
const connect = async () => {
  // prevent call multiple times
  if (connecting) return;
  connecting = true;
  // do not let connect throw error, so claim one stream does not affect other
  await cam.connect();
  if (cam.IsConnected) {
    pipeMessageAll();
    logger.log('connected', cam.connectedDevice.url);
  } else {
    logger.log('not connected', cam.connectedDevice.url);
  }
  connecting = false;
};

const removeEvents = StreamType => {
  const { video, audio } = cam.getVideoStream(StreamType);
  if (video) video.removeAllListeners('data');
  if (audio) audio.removeAllListeners('data');
};

const disconnect = async () => {
  // unbind all then disconnect
  removeEvents(MAIN_STREAM);
  removeEvents(SUB_STREAM);
  clearTimeout(keepAliveTimer);
  sendMessage = null;
  try {
    await cam.close();
  } catch (ex) {
    logger.log(ex);
  }

  process.exit();
};
