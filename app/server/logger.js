const chalk = require('chalk');

const LOG_TYPES = {
  NONE: 0,
  ERROR: 1,
  WARN: 2,
  NORMAL: 3,
  DEBUG: 4,
  FFDEBUG: 5
};

let logType = LOG_TYPES.NORMAL;
let logInstance = console;

const formatNode = (color, messageType, args) =>
  logInstance.log(
    logTime(),
    process.pid,
    chalk.bold[color](messageType),
    ...args
  );

const formatWeb = (color, messageType, args) => {
  // already format
  if (
    args.length &&
    args.some(arg => typeof arg === 'string' && arg.startsWith('%'))
  ) {
    return logInstance.log(...args);
  }

  logInstance.log(
    `${logTime()} ${process.pid} %c${messageType}`,
    `color:${color};font-weight:bold;`,
    ...args
  );
};

let logFormat = formatNode;

const setLogInstance = (log, web = false) => {
  if (typeof log !== 'object') return;

  logInstance = log;
  if (!('log' in log)) {
    // try alias
    logInstance.log = log.info;
  }

  logFormat = web ? formatWeb : formatNode;
};

const setLogType = type => {
  if (typeof type !== 'number') return;

  logType = type;
};

const logTime = () => {
  const nowDate = new Date();
  return `${nowDate.toLocaleDateString()} ${nowDate.toLocaleTimeString([], {
    hour12: false
  })}`;
};

const log = (...args) => {
  if (logType < LOG_TYPES.NORMAL) return;
  logFormat('green', '[INFO]', args);
};

const warn = (...args) => {
  if (logType < LOG_TYPES.WARN) return;
  logFormat('yellow', '[WARN]', args);
};

const error = (...args) => {
  if (logType < LOG_TYPES.ERROR) return;
  logFormat('red', '[ERROR]', args);
};

const debug = (...args) => {
  if (logType < LOG_TYPES.DEBUG) return;
  logFormat('blue', '[DEBUG]', args);
};

const ffdebug = (...args) => {
  if (logType < LOG_TYPES.FFDEBUG) return;
  logFormat('blue', '[FFDEBUG]', args);
};

module.exports = {
  LOG_TYPES,
  setLogInstance,
  setLogType,
  log,
  warn,
  info: log,
  error,
  debug,
  ffdebug
};
