/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */
/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
import express from 'express';
import path from 'path';
import fs from 'fs';
import mkdirp from 'mkdirp';
import compression from 'compression';
import db from './database';
import {
  streamApp,
  start,
  addDevice,
  addStream,
  getDevicesInfo,
  restoreSaveFolder
} from './media-server';
import logger from './logger';
import { dataDir, dbName } from './constants/database.json';
import { MAIN_STREAM } from './constants/stream.json';

// load database sqlite
const DatabaseSqlite = require('./sqlite');
const ImageReposity = require('./sqlite/image_repository');

const database = new DatabaseSqlite('./database.sqlite3');
console.log(database);
const imageRepository = new ImageReposity(database);
imageRepository.createTable();

const appPath = process.env.APP_DATA || process.cwd();

// process.env.WEB = process.env.WEB || 'true';

const fallbackRoutes = ['/history', '/setting', '/camera', '/module'];
const notFoundMiddleware = distPath => {
  return (req, res) => {
    if (process.env.WEB === 'true') {
      const found = fallbackRoutes.some(route => req.url.startsWith(route));
      if (found) return res.sendFile(path.join(distPath, 'index.html'));
    }

    res.status(404).send('Not Found');
  };
};

const addAppMethods = (app, watcher) => {
  // hot reload for express js, using absolute require to compare
  // use route as current file, in the future may use custom param and set this as default
  app.useRouter = function useRouter(route, router, ...args) {
    let moduleRouter = null;
    if (watcher) {
      const folder = path.join(__dirname, `.${route}.js`);
      const id = folder.replace(/\//g, path.sep);
      watcher.watch(folder, () => {
        logger.log(`Restarting Router [${route}] from server`);
        // update module, reset everything
        delete require.cache[id];
        moduleRouter = null;
      });
    }
    return app.use(route, async (req, res, next) => {
      if (!moduleRouter) {
        moduleRouter = await router().then(m => m.default(...args));
      }
      moduleRouter(req, res, next);
    });
  };
};

const initDevices = async ffmpegPath => {
  const devicesInfo = await getDevicesInfo();
  if (Array.isArray(devicesInfo)) {
    // do not await Promise.all, 1 device error can lead to discard all
    devicesInfo.forEach(async device => {
      const { url, user, pass, record, mainStream, tcp, isIPCam } = device;
      const connectedDevice = await addDevice({
        url,
        user,
        pass,
        tcp,
        isIPCam
      });
      if (record && mainStream) {
        // add Main Stream to record
        const streamURL = await addStream({
          url,
          connectedDevice,
          device,
          stream: MAIN_STREAM,
          ffmpegPath
        });

        logger.log('Init stream for recording', streamURL);
      }
    });
  }
};

async function runHttpServer({
  logType,
  port,
  host = '0.0.0.0',
  prefix = '/api',
  limit = '50mb',
  appData = appPath,
  ffmpegPath,
  modelPath,
  distPath
}) {
  // do no show any log or show everything ?
  logger.setLogType(logType);

  // open webrtc stream
  const app = express();
  // const wsInstance = expressWs(app);

  // other APIs with hot reload for each api seperatedly
  let watcher;
  if (
    process.env.START_HOT !== 'false' &&
    process.env.NODE_ENV !== 'production'
  ) {
    const Watcher = require('./watcher').default;
    watcher = new Watcher();
  }
  addAppMethods(app, watcher);
  // wrap websocket
  // addWsMethod(app);
  // addWsMethod(express.Router);

  const mediaroot = path.join(appData, 'media');
  // create if not exist
  mkdirp.sync(mediaroot);

  // default modelPath and ffmpegPath if run separatedly
  if (!modelPath || !fs.existsSync(modelPath))
    modelPath = path.join(appPath, 'app', 'models');
  if (!ffmpegPath || !fs.existsSync(ffmpegPath)) {
    // current path with this process, not current file from asar, like when copy to resource folder
    ffmpegPath = path.join(appPath, 'bin', process.platform, `ffmpeg`);
  }

  // fix for window, add exe to end
  if (process.platform === 'win32' && !ffmpegPath.endsWith('.exe')) {
    ffmpegPath += '.exe';
  }
  // logger.log(`ffmpeg path : ${ffmpegPath}`);
  if (!distPath) distPath = path.join(appPath, 'app', 'dist');

  if (process.env.NODE_ENV === 'production') {
    global.camWorkerPath = path.join(distPath, 'cam-worker.prod.js');
  }

  logger.log(
    'AppDir',
    appData,
    'ffmpegPath',
    ffmpegPath,
    'modelPath',
    modelPath
  );

  // assign global cam-worker path

  // init from appData
  const dataDirPath = path.join(appData, dataDir);
  const bundlePath = path.join(dataDirPath, 'modules');
  const dbDir = path.join(dataDirPath, dbName);
  // create if not exist
  mkdirp.sync(bundlePath);
  mkdirp.sync(dbDir);
  db.load(dbDir);

  // overide rootFolder after load db
  restoreSaveFolder(mediaroot);

  // init at background all devices
  initDevices(ffmpegPath);

  const streamAppRoot = path.join(mediaroot, streamApp);

  // init app
  app
    .use(compression())

    .all('*', (req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header(
        'Access-Control-Allow-Headers',
        'Content-Type,Content-Length, Authorization, Accept,X-Requested-With'
      );
      res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Credentials', true);
      res.setHeader('X-Powered-By', 'CameraAI');
      if (req.method === 'OPTIONS') res.sendStatus(200);
      else next();
    });

  // now init node media server, this will start and return httpserver
  const { httpServer } = start({
    app,
    port,
    host
  });

  // with bundle path as default dist for both web and app
  app.use(express.static(bundlePath));

  // this is for serve web testing or ship inside web
  if (process.env.WEB === 'true') {
    // use compression for web
    app.use(express.static(distPath));
  }

  // already has express.urlencoded and cors
  app
    .use(express.json({ extended: false }))
    // only allow image type ?
    .use(express.raw({ limit, type: '*/*' }))
    // routers
    .useRouter(`${prefix}/db`, () => import('./api/db'))
    .useRouter(`${prefix}/utils`, () => import('./api/utils'))
    .useRouter(
      `${prefix}/camera`,
      () => import('./api/camera'),
      streamAppRoot,
      ffmpegPath,
      imageRepository
    )
    .useRouter(`${prefix}/onvif`, () => import('./api/onvif'))
    .useRouter(`${prefix}/license`, () => import('./api/license'))
    .useRouter(`${prefix}/model`, () => import('./api/model'), modelPath)
    // .useRouter(`${prefix}/hotreload`, () => import('./api/hotreload'))
    .useRouter(
      `${prefix}/module`,
      () => import('./api/module'),
      bundlePath,
      appPath
    )
    // finally apply middleware
    .use(notFoundMiddleware(distPath));

  return httpServer;
}

function closeHttpServer(app) {
  logger.log('Server closing');
  // close webrtc stream
  return new Promise(resolve => {
    if (app && app.listening) {
      app.close(resolve);
    } else {
      resolve();
    }
  });
}

process.on('uncaughtException', logger.error);

if (require.main === module) {
  runHttpServer({ log: true, port: process.env.SERVER_PORT || 3000 });
}

export { runHttpServer, closeHttpServer };
