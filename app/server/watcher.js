import fs from 'fs';
import { EventEmitter } from 'events';
import crypto from 'crypto';

const getFileHash = file =>
  new Promise((resolve, reject) => {
    const fd = fs.createReadStream(file);
    const hash = crypto.createHash('md5');
    hash.setEncoding('hex');

    fd.on('end', () => {
      hash.end();
      resolve(hash.read()); // the desired sha1sum
    });

    fd.on('error', reject);

    // read all file and pipe it (write it) to the hash object
    fd.pipe(hash);
  });

export default class Watcher extends EventEmitter {
  constructor({ checkContent = true, pollInterval = 100, ...options } = {}) {
    super(options);
    this.pollInterval = pollInterval;
    if (checkContent) this.hfiles = new Map();
    this.mfiles = new Map();

    this.run();
  }

  async watch(file, callback) {
    if (!this.mfiles.has(file)) {
      this.mfiles.set(file, fs.statSync(file).mtime);
      if (this.hfiles) {
        const hash = await getFileHash(file);
        this.hfiles.set(file, hash);
      }
    }
    this.addListener(file, callback);
  }

  unwatch(file, callback) {
    this.removeListener(file, callback);

    if (!this.listenerCount(file)) {
      this.mfiles.delete(file);
      if (this.hfiles) this.hfiles.delete(file);
    }
  }

  unwatchAll(file) {
    this.removeAllListeners(file);
    this.mfiles.delete(file);
    if (this.hfiles) this.hfiles.delete(file);
  }

  run = async () => {
    await Promise.all(
      this.eventNames().map(async file => {
        const { mtime } = fs.statSync(file);
        if (mtime !== this.mfiles.get(file)) {
          this.mfiles.set(file, mtime);
          if (this.hfiles) {
            const hash = await getFileHash(file);
            if (this.hfiles.get(file) !== hash) {
              this.hfiles.set(file, hash);
              this.emit(file, mtime, hash);
            }
          } else {
            this.emit(file, mtime);
          }
        }
      })
    );

    setTimeout(this.run, this.pollInterval);
  };
}
