import path from 'path';
import { Writable, PassThrough } from 'stream';
import transform from 'sdp-transform';
import Cam from '../onvif/cam';
import { addDevice } from '../media-server';
import FileAdapterStream from '../media-stream/FileAdapterStream';

// .h264 file header
const H264_HEADER = Buffer.from([0x00, 0x00, 0x00, 0x01]);
const getH264Header = mediaSource => {
  // Extract SPS and PPS from the MediaSource part of the SDP
  const fmtp = mediaSource.fmtp[0];

  if (!fmtp) {
    return;
  }
  const fmtpConfig = transform.parseParams(fmtp.config);
  const splitSpropParameterSets = fmtpConfig['sprop-parameter-sets']
    .toString()
    .split(',');
  const spsBase64 = splitSpropParameterSets[0];
  const ppsBase64 = splitSpropParameterSets[1];
  const sps = Buffer.from(spsBase64, 'base64');
  const pps = Buffer.from(ppsBase64, 'base64');

  return Buffer.concat([H264_HEADER, sps, H264_HEADER, pps]);
};

class Test extends Writable {
  write(chunk) {
    if (!this.firstBuffer) {
      this.firstBuffer = chunk;
      console.log('header', this.firstBuffer);
    }
    // left
  }
}

const run = async () => {
  const connectedDevice = await addDevice({
    url: 'rtsp://cchuymay.cameraddns.net:554/Streaming/Channels/1301',
    user: 'demo',
    pass: 'admin123'
  });

  // const connectedDevice = await addDevice({
  //     url: 'rtsp://192.168.1.113:554/Streaming/Channels/101?transportmode=unicast&profile=Profile_1',
  //     user: 'admin',
  //     pass: 'abcd1234'
  // });
  const cam = new Cam({ connectedDevice, isIPCam: false });

  const test = new FileAdapterStream({
    // filename: path.join(__dirname, 'test.h264')
    folder: __dirname,
    ffmpegPath: 'ffmpeg.exe',
    duration: 60 //seconds
  });
  // const test = new Test();
  // const test1 = new Test();
  cam.pipeVideo('Extra', test);
  // setTimeout(() => {
  //   cam.pipeVideo('Extra', test1);
  // }, 5000);
  setTimeout(
    () => {
      cam.unpipeVideo('Extra', test);
      test.destroy();
    },
    110 * 1000
    // MediaStream.fileLength * 1000
  );
};

run();
