/* eslint-disable no-underscore-dangle */
// import { EventEmitter } from 'events';
import H264Transport from './transports/H264Transport';
import AACTransport from './transports/AACTransport';
import RTSPClient from './RTSPClient';

import { MAIN_STREAM, SUB_STREAM } from '../constants/stream.json';

import logger from '../logger';

const closeStream = async streamClient => {
  if (streamClient.stream) {
    await streamClient.stream.close();
    if (streamClient.video) streamClient.video.removeAllListeners();
    if (streamClient.audio) streamClient.audio.removeAllListeners();
  }
};

// RTSP client with ONVIF extensions.
export default class ONVIFClient {
  constructor(connectedDevice) {
    // super();
    this.connectedDevice = connectedDevice;

    this.main = {};
    this.extra = {};
  }

  async connectStream(StreamType, streamURL, protocol) {
    // because of interleave, we simple re-create stream when disconnected instead of remaining socket

    const _streamClient = {};

    _streamClient.stream = new RTSPClient(
      this.connectedDevice.user,
      this.connectedDevice.pass,
      {
        Require: 'onvif-replay'
      }
    );

    // this handle error will be call when there is error
    const handlerError = err => {
      // _streamClient.stream.close();
      logger.log(
        'Onvif connect err',
        this.connectedDevice.url,
        'streamUrl',
        streamURL,
        err
      );
      // check if this stream is removed from database then disconnect
    };

    // _streamClient.stream.once('error', handlerError);

    // re-assign video and audio, if not connected or not writable
    // try catch so that is one connect fail, does not affect other

    try {
      await _streamClient.stream.connect(streamURL);
      // is connected then get video, audio stream
      // console.log('connected', detailsArray);
      const detailsArray = await _streamClient.stream.claimVideoStream(
        protocol
      );

      // re-create streams
      detailsArray.forEach(details => {
        if (details.codec === 'H264') {
          _streamClient.video = new H264Transport(
            _streamClient.stream,
            details
          );
          // _streamClient.videoDetails = details;
        } else if (details.codec === 'AAC') {
          _streamClient.audio = new AACTransport(_streamClient.stream, details);
        }
      });

      // start play
      await _streamClient.stream.play();
    } catch (err) {
      handlerError(err);
    }

    // just return
    return _streamClient;
  }

  get IsConnected() {
    let connected = !!this.extra.stream && this.extra.stream.IsConnected;
    if (this.main.stream) connected = connected && this.main.stream.IsConnected;
    return connected;
  }

  async close() {
    await closeStream(this.extra);
    await closeStream(this.main);
  }

  // re-connect
  async connect(protocol = 'UDP') {
    // close all streams and remove all listeners
    await this.close();

    const { stream, mainStream } = this.connectedDevice.profile;
    // may not have
    if (stream)
      this.extra = await this.connectStream(SUB_STREAM, stream, protocol);
    // prevent double connect
    if (mainStream && mainStream !== stream) {
      this.main = await this.connectStream(MAIN_STREAM, mainStream, protocol);
      if (!stream) {
        // hard code
        this.extra = await this.connectStream(
          SUB_STREAM,
          mainStream.replace('stream=0', 'stream=1'),
          protocol
        );
      }
    } else {
      // assign main to extra pointer
      this.main = this.extra;
    }
  }

  async playFrom(from, to, StreamType = MAIN_STREAM) {
    const _streamClient = StreamType === MAIN_STREAM ? this.main : this.extra;
    const obj = {
      Session: _streamClient.stream._session,
      Immediate: 'yes',
      Range: `clock=${from.toISOString()}-`
    };

    if (to) {
      obj.Range += to.toISOString();
    }

    await _streamClient.stream.request('PLAY', obj);
  }

  async playReverse(from, to, StreamType = MAIN_STREAM) {
    const _streamClient = StreamType === MAIN_STREAM ? this.main : this.extra;
    const obj: any = {
      Session: _streamClient.stream._session,
      'Rate-Control': 'no',
      Scale: '-1.0'
    };

    if (from) {
      obj.Range = `clock=${from.toISOString()}-`;
      if (to) {
        obj.Range += to.toISOString();
      }
    }

    await _streamClient.stream.request('PLAY', obj);
  }

  getVideoStream(StreamType = MAIN_STREAM) {
    return StreamType === MAIN_STREAM ? this.main : this.extra;
  }
}
