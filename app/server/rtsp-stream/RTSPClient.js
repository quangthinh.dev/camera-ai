/* eslint-disable class-methods-use-this */
/* eslint-disable no-bitwise */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-plusplus */
/* eslint-disable radix */
/* eslint-disable no-return-assign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
/* eslint-disable func-names */
/* eslint-disable no-shadow */
import { Socket } from 'net';
import dgram from 'dgram';
import { parse } from 'url';
import events from 'events';
import transform from 'sdp-transform';
import * as util from './util';
import logger from '../logger';

const RTP_AVP = 'RTP/AVP';
const STATUS_OK = 200;
const STATUS_UNAUTH = 401;
const WWW_AUTH = 'WWW-Authenticate';
const WWW_AUTH_REGEX = new RegExp(
  '([a-zA-Z]+)s*=s*"?((?<=").*?(?=")|.*?(?=,?s*[a-zA-Z]+s*=)|.+[^=])',
  'g'
);
// $
const PACKET_START = 0x24;
// R
const RTSP_HEADER_START = 0x52;
// /n
const ENDL = 10;

const DEBUG = false;

const SEARCHING = 0;
const READING_RTSP_HEADER = 1;
const READING_RTSP_PAYLOAD = 2;
const READING_RAW_PACKET_SIZE = 3;
const READING_RAW_PACKET = 4;

export default class RTSPClient extends events.EventEmitter {
  constructor(username, password, headers, commandTimeoutMs = 5000) {
    super();

    this.commandTimeoutMs = commandTimeoutMs;
    this._nextFreeInterleavedChannel = 0;

    this._cSeq = 0;
    this._isConnected = false;
    this._nextFreeUDPPort = 15000;
    this.readState = SEARCHING;
    // Used as a cache for the data stream.
    // What's in here is based on current #readState.
    this.messageBytes = [];
    // Used for parsing RTSP responses,
    // Content-Length header in the RTSP message.
    this.rtspContentLength = 0;
    this.rtspStatusLine = '';
    this.rtspHeaders = {};
    // Used for parsing RTP/RTCP responses.
    this.rtspPacketLength = 0;
    this.rtspPacket = Buffer.from('');
    this.rtspPacketPointer = 0;
    // Used in #_emptyReceiverReport.
    this.clientSSRC = util.generateSSRC();
    this.username = username;
    this.password = password;
    this.headers = { ...headers, 'User-Agent': 'CameraAI/0.x' };

    this._onData = this._onData.bind(this);
    // may drop ACK from client
    this._socket = new Socket({ allowHalfOpen: false });

    // just trigger it is not connect to retry again, or just prevent crashing ?
    this._socket.on('error', () => {
      // logger.log('Onvif Client error', this._url, err);
      this.close();
      this._isConnected = false;
    });

    this._socket.setTimeout(5000, () => {
      logger.log('socket timeout');
      if (this.reconnect) {
        this.reconnect();
      } else {
        this.close();
        this._isConnected = false;
      }
    });

    this._socket.setKeepAlive(true, commandTimeoutMs);
    // this._socket.setNoDelay(true);
    this._socket.on('data', this._onData);

    // this._socket.on('close', () => {
    //   this.close();
    // });

    this.textDecoder = new TextDecoder();
  }

  get IsConnected() {
    return (
      this._socket &&
      !this._socket.destroyed &&
      !!this._socket.remoteAddress &&
      this._isConnected
    );
  }

  // This manages the lifecycle for the RTSP connection
  // over TCP.
  //
  // Sets #_client.
  //
  // Handles receiving data & closing port, called during
  // #connect.
  _netConnect(hostname, port) {
    // prevent call connect multiple times cause event emitter leak

    return new Promise((resolve, reject) => {
      // Set after listeners defined.

      const errorListener = err => {
        this._connectPromise = null;
        this._socket && this._socket.off('connect', connectListener);
        this._isConnected = false;
        reject(err);
      };

      let timeout;

      const connectListener = () => {
        // remove this one may have problem later
        this._socket && this._socket.off('error', errorListener);
        this.once('response', responseListener);
        timeout = setTimeout(
          () => this.off('response', responseListener),
          this.commandTimeoutMs
        );
        this._connectPromise = null;
        this._isConnected = true;
        resolve();
      };

      const responseListener = async (responseName, headers) => {
        clearTimeout(timeout);
        const name = responseName.split(' ')[0];
        if (name.indexOf('RTSP/') === 0) {
          return;
        }
        if (name === 'REDIRECT' || name === 'ANNOUNCE') {
          this.respond('200 OK', { CSeq: headers.CSeq });
        }

        if (name === 'REDIRECT' && headers.Location) {
          await this.close(false);
          // recursive login
          return this.connect(headers.Location);
        }
      };
      this._socket.once('error', errorListener);
      this.reconnect = () =>
        this._socket.connect(port, hostname, connectListener);
      this.reconnect();
    });
  }

  async claimVideoStream(protocol = 'UDP') {
    const details = [];

    // whether allow listing url
    await this.request('OPTIONS');
    // cache this media describe ?

    const describeRes = await this.request('DESCRIBE', {
      Accept: 'application/sdp'
    });

    if (!describeRes || !describeRes.mediaHeaders) {
      throw new Error(
        'No media headers on DESCRIBE; RTSP server is broken (sanity check)'
      );
    }
    // For now, only RTP/AVP is supported.
    const { media } = transform.parse(describeRes.mediaHeaders.join('\r\n'));

    // Loop over the Media Streams in the SDP looking for Video or Audio
    // In theory the SDP can contain multiple Video and Audio Streams. We only want one of each type
    let hasVideo = false;
    let hasAudio = false;
    let hasMetaData = false;
    for (let x = 0; x < media.length; x++) {
      let needSetup = false;
      let codec = '';
      const mediaSource = media[x];
      if (
        mediaSource.type === 'video' &&
        mediaSource.protocol === RTP_AVP &&
        mediaSource.rtp[0].codec === 'H264'
      ) {
        if (DEBUG) console.log('H264 Video Stream Found in SDP', '');
        if (hasVideo === false) {
          needSetup = true;
          hasVideo = true;
          codec = 'H264';
        }
      }
      if (
        mediaSource.type === 'audio' &&
        mediaSource.protocol === RTP_AVP &&
        mediaSource.rtp[0].codec === 'mpeg4-generic' &&
        mediaSource.fmtp[0].config.includes('AAC')
      ) {
        if (DEBUG) console.log('AAC Audio Stream Found in SDP', '');
        if (hasAudio === false) {
          needSetup = true;
          hasAudio = true;
          codec = 'AAC';
        }
      }
      if (
        mediaSource.type === 'appliction' &&
        mediaSource.protocol === RTP_AVP &&
        mediaSource.rtp[0].codec === 'VND.ONVIF.METADATA'
      ) {
        if (DEBUG) console.log('ONVIF Meta Data Found in SDP', '');
        if (hasMetaData === false) {
          needSetup = true;
          hasMetaData = true;
          codec = 'VND.ONVIF.METADATA';
        }
      }
      if (needSetup) {
        let streamurl = '';
        // The 'control' in the SDP can be a relative or absolute uri
        if (mediaSource.control) {
          if (mediaSource.control.toLowerCase().startsWith('rtsp://')) {
            // absolute path
            streamurl = mediaSource.control;
          } else {
            // relative path
            streamurl = `${this._url}/${mediaSource.control}`;
          }
        }
        // Perform a SETUP on the streamurl
        // either 'UDP' RTP/RTCP packets
        // or with 'TCP' RTP/TCP packets which are interleaved into the TCP based RTSP socket
        let setupRes;
        let rtpChannel;
        let rtcpChannel;
        if (protocol === 'UDP') {
          // Create a pair of UDP listeners, even numbered port for RTP
          // and odd numbered port for RTCP
          rtpChannel = this._nextFreeUDPPort;
          rtcpChannel = this._nextFreeUDPPort + 1;
          this._nextFreeUDPPort += 2;
          const rtpPort = rtpChannel;
          const rtpReceiver = dgram.createSocket('udp4');
          rtpReceiver.on('message', buf => {
            const packet = util.parseRTPPacket(buf);
            this.emit('data', rtpPort, packet);
          });
          const rtcpPort = rtcpChannel;
          const rtcpReceiver = dgram.createSocket('udp4');
          rtcpReceiver.on('message', (buf, remote) => {
            const packet = util.parseRTCPPacket(buf);
            this.emit('controlData', rtcpPort, packet);
            const receiverReport = this._emptyReceiverReport();
            this._sendUDPData(remote.address, remote.port, receiverReport);
          });
          // Block until both UDP sockets are open.
          await Promise.all([
            new Promise(resolve => rtpReceiver.bind(rtpPort, resolve)),
            new Promise(resolve => rtcpReceiver.bind(rtcpPort, resolve))
          ]);
          const setupHeader = {
            Transport: `RTP/AVP;unicast;client_port=${rtpPort}-${rtcpPort}`
          };
          if (this._session)
            Object.assign(setupHeader, { Session: this._session });
          setupRes = await this.request('SETUP', setupHeader, streamurl);
        } else if (protocol === 'TCP') {
          // channel 0, RTP
          // channel 1, RTCP
          rtpChannel = this._nextFreeInterleavedChannel;
          rtcpChannel = this._nextFreeInterleavedChannel + 1;
          this._nextFreeInterleavedChannel += 2;
          const setupHeader = {
            Transport: `RTP/AVP/TCP;interleaved=${rtpChannel}-${rtcpChannel}`
          };
          if (this._session)
            Object.assign(setupHeader, { Session: this._session }); // not used on first SETUP

          setupRes = await this.request('SETUP', setupHeader, streamurl);
        } else {
          throw new Error(
            `Connection parameter to RTSPClient#connect is ${protocol}, not udp or tcp!`
          );
        }

        if (!setupRes) {
          throw new Error(
            'No SETUP response; RTSP server is broken (sanity check)'
          );
        }
        const { headers } = setupRes;
        if (!headers.Transport) {
          throw new Error(
            'No Transport header on SETUP; RTSP server is broken (sanity check)'
          );
        }
        const transport = util.parseTransport(headers.Transport);
        if (
          transport.protocol !== 'RTP/AVP/TCP' &&
          transport.protocol !== 'RTP/AVP'
        ) {
          throw new Error(
            'Only RTSP servers supporting RTP/AVP(unicast) or RTP/ACP/TCP are supported at this time.'
          );
        }
        if (headers.Unsupported) {
          this._unsupportedExtensions = headers.Unsupported.split(',');
        }
        if (headers.Session) {
          this._session = headers.Session.split(';')[0];
        }

        details.push({
          codec,
          mediaSource,
          transport: transport.parameters,
          rtpChannel,
          rtcpChannel
        });
      } // end if (needSetup)
    } // end for loop, looping over each media stream
    return details;
  }

  async connect(url, keepAlive = false) {
    this._url = url;
    const { hostname, port } = parse(url);
    if (!hostname) {
      throw new Error(`URL parsing error in connect method: ${url}`);
    }

    if (this._socket.connecting || this.IsConnected) return; // throw 'Already connected';

    // logger.log('socket connnecting', this._url);
    await this._netConnect(hostname, parseInt(port || '554'));

    if (keepAlive) this.setupAliveKeeper();
  }

  setupAliveKeeper(interval = 2) {
    // Start a Timer to send OPTIONS every 20 seconds to keep stream alive
    // using the Session ID
    if (this._keepAliveID) clearInterval(this._keepAliveID);
    this._keepAliveID = setInterval(() => {
      this.request('OPTIONS', { Session: this._session }).catch(err => {
        logger.error(err);

        // there is problem, disconnect
        this._isConnected = false;
        clearInterval(this._keepAliveID);
      });
    }, interval * 1000);
  }

  getAuthorizationString(requestName, authHeader) {
    const type = authHeader.split(' ')[0];
    // Get auth properties from WWW_AUTH header.
    let realm = '';
    let nonce = '';
    let match = WWW_AUTH_REGEX.exec(authHeader);
    while (match != null) {
      const prop = match[1];
      if (prop === 'realm' && match[2]) {
        realm = match[2];
      }
      if (prop === 'nonce' && match[2]) {
        nonce = match[2];
      }
      match = WWW_AUTH_REGEX.exec(authHeader);
    }
    // mutable, corresponds to Authorization header
    let authString = '';
    if (type === 'Digest') {
      // Digest Authentication
      const ha1 = util.getMD5Hash(`${this.username}:${realm}:${this.password}`);
      const ha2 = util.getMD5Hash(`${requestName}:${this._url}`);
      const ha3 = util.getMD5Hash(`${ha1}:${nonce}:${ha2}`);
      authString = `Digest username="${this.username}",realm="${realm}",nonce="${nonce}",uri="${this._url}",response="${ha3}"`;
    } else if (type === 'Basic') {
      // Basic Authentication
      // https://xkcd.com/538/
      const b64 = Buffer.from(`${this.username}:${this.password}`).toString(
        'base64'
      );
      authString = `Basic ${b64}`;
    }
    return authString;
  }

  request(requestName, headersParam = {}, url, IgnoreResponse = false) {
    // stop exec because disconnect
    if (!this._isConnected) {
      return IgnoreResponse
        ? Promise.resolve()
        : Promise.reject(new Error('Disconnected'));
    }
    const id = ++this._cSeq;
    // mutable via string addition
    let req = `${requestName} ${url || this._url} RTSP/1.0\r\nCSeq: ${id}\r\n`;
    const headers = { ...this.headers, ...headersParam };
    req += Object.entries(headers)
      .map(([key, value]) => `${key}: ${value}\r\n`)
      .join('');
    if (DEBUG) console.log(req, 'C->S');
    // Make sure to add an empty line after the request.
    this._socket.write(`${req}\r\n`);

    return new Promise((resolve, reject) => {
      let timeoutReject = setTimeout(() => {
        this.removeListener('response', responseHandler);
        // may ping request
        if (IgnoreResponse) resolve();
        else reject(new Error(`Execution timed out ${req}`));
        timeoutReject = undefined;
      }, this.commandTimeoutMs);

      const responseHandler = (responseName, resHeaders, mediaHeaders) => {
        if (!timeoutReject) return;
        clearTimeout(timeoutReject);

        if (resHeaders.CSeq !== id && resHeaders.Cseq !== id) {
          return resolve();
        }

        const statusCode = parseInt(responseName.split(' ')[1]);
        if (statusCode === STATUS_OK) {
          if (mediaHeaders.length) {
            resolve({
              headers: resHeaders,
              mediaHeaders
            });
          } else {
            resolve({
              headers: resHeaders
            });
          }
        } else {
          const authHeader = resHeaders[WWW_AUTH];
          // We have status code unauthenticated.
          if (statusCode === STATUS_UNAUTH && authHeader) {
            Object.assign(headers, {
              Authorization: this.getAuthorizationString(
                requestName,
                authHeader
              )
            });
            // callback in recursive manner
            clearTimeout(timeoutReject);
            timeoutReject = undefined;

            return this.request(requestName, headers, url)
              .then(resolve)
              .catch(reject);
          }

          reject(new Error(`Bad RTSP status code ${statusCode}!`));
        }
      };
      this.once('response', responseHandler);
    });
  }

  respond(status, headersParam = {}) {
    if (!this._socket.writable) {
      return;
    }
    // mutable via string addition
    let res = `RTSP/1.0 ${status}\r\n`;
    const headers = { ...this.headers, ...headersParam };
    res += Object.entries(headers)
      .map(([key, value]) => `${key}: ${value}\r\n`)
      .join('');
    if (DEBUG) console.log(res, 'C->S');
    this._socket.write(`${res}\r\n`);
  }

  async play() {
    if (!this.IsConnected) {
      throw new Error('Client is not connected.');
    }
    await this.request('PLAY', { Session: this._session });
    return this;
  }

  async pause() {
    if (!this.IsConnected) {
      throw new Error('Client is not connected.');
    }
    await this.request('PAUSE', { Session: this._session });
    return this;
  }

  async close(isImmediate = true) {
    // reset because we still remain socket
    if (!isImmediate) {
      await this.request(
        'TEARDOWN',
        {
          Session: this._session
        },
        this._url,
        true
      );
    }

    this._isConnected = false;
    this._nextFreeUDPPort = 0;
    this._nextFreeInterleavedChannel = 0;

    if (this._keepAliveID) {
      clearInterval(this._keepAliveID);
      this._keepAliveID = 0;
    }

    // do not process data
    if (this._socket) {
      this._socket.removeAllListeners('data');
      this._socket.end();
      this._socket = null;
    }

    // remove events, no more pipe
    this.removeAllListeners();

    this._cSeq = 0;
    return this;
  }

  decodeText(messageBytes) {
    // limit length of String.fromCharCode
    return messageBytes.length > 65535
      ? this.textDecoder.decode(Buffer.from(messageBytes))
      : String.fromCharCode.apply(null, messageBytes);
  }

  _onData(data) {
    let index = 0;

    while (index < data.length) {
      // read RTP or RTCP packet
      if (this.readState === SEARCHING && data[index] === PACKET_START) {
        this.messageBytes = [data[index]];
        index++;
        this.readState = READING_RAW_PACKET_SIZE;
      } else if (this.readState === READING_RAW_PACKET_SIZE) {
        // accumulate bytes for $, channel and length
        this.messageBytes.push(data[index]);
        index++;
        if (this.messageBytes.length === 4) {
          this.rtspPacketLength =
            (this.messageBytes[2] << 8) + this.messageBytes[3];
          if (this.rtspPacketLength > 0) {
            this.rtspPacket = Buffer.alloc(this.rtspPacketLength);
            this.rtspPacketPointer = 0;
            this.readState = READING_RAW_PACKET;
          } else {
            this.readState = SEARCHING;
          }
        }
      } else if (this.readState === READING_RAW_PACKET) {
        this.rtspPacket[this.rtspPacketPointer++] = data[index];
        index++;
        if (this.rtspPacketPointer === this.rtspPacketLength) {
          const packetChannel = this.messageBytes[1];
          if ((packetChannel & 0x01) === 0) {
            // even number
            const packet = util.parseRTPPacket(this.rtspPacket);
            this.emit('data', packetChannel, packet);
          }
          if ((packetChannel & 0x01) === 1) {
            // odd number
            const packet = util.parseRTCPPacket(this.rtspPacket);
            this.emit('controlData', packetChannel, packet);
            const receiverReport = this._emptyReceiverReport();
            this._sendInterleavedData(packetChannel, receiverReport);
          }
          this.readState = SEARCHING;
        }
        // read response data
      } else if (
        this.readState === SEARCHING &&
        data[index] === RTSP_HEADER_START
      ) {
        // found the start of a RTSP rtsp_message
        this.messageBytes = [data[index]];
        index++;
        this.readState = READING_RTSP_HEADER;
      } else if (this.readState === READING_RTSP_HEADER) {
        // Reading a RTSP message.
        // Add character to the messageBytes
        // Ignore /r (13) but keep /n (10)
        if (data[index] !== 13) {
          this.messageBytes.push(data[index]);
        }
        index++;
        // if we have two new lines back to back then we have a complete RTSP command,
        // note we may still need to read the Content Payload (the body) e.g. the SDP
        if (
          this.messageBytes.length >= 2 &&
          this.messageBytes[this.messageBytes.length - 2] === ENDL &&
          this.messageBytes[this.messageBytes.length - 1] === ENDL
        ) {
          // Parse the Header
          const text = this.decodeText(this.messageBytes);
          const lines = text.split('\n');
          this.rtspContentLength = 0;
          this.rtspStatusLine = lines[0];
          this.rtspHeaders = {};
          lines.forEach(line => {
            const indexOf = line.indexOf(':');
            if (indexOf !== line.length - 1) {
              const key = line.substring(0, indexOf).trim();
              const data = line.substring(indexOf + 1).trim();
              this.rtspHeaders[key] =
                key !== 'Session' && data.match(/^[0-9]+$/)
                  ? parseInt(data, 10)
                  : data;
              // workaround for buggy Hipcam RealServer/V1.0 camera which returns Content-length and not Content-Length
              if (key.toLowerCase() === 'content-length') {
                this.rtspContentLength = parseInt(data, 10);
              }
            }
          });
          // if no content length, there there's no media headers
          // emit the message
          if (!this.rtspContentLength) {
            if (DEBUG) console.log(text, 'S->C');
            this.emit('response', this.rtspStatusLine, this.rtspHeaders, []);
            this.readState = SEARCHING;
          } else {
            this.messageBytes = [];
            this.readState = READING_RTSP_PAYLOAD;
          }
        }
      } else if (
        this.readState === READING_RTSP_PAYLOAD &&
        this.messageBytes.length < this.rtspContentLength
      ) {
        // Copy data into the RTSP payload
        this.messageBytes.push(data[index]);
        index++;
        if (this.messageBytes.length === this.rtspContentLength) {
          const text = this.decodeText(this.messageBytes);
          const mediaHeaders = text.split('\n');
          // Emit the RTSP message
          if (DEBUG) console.log(text, 'S->C');
          this.emit(
            'response',
            this.rtspStatusLine,
            this.rtspHeaders,
            mediaHeaders
          );
          this.readState = SEARCHING;
        }
      } else {
        // unexpected data
        // return this.emit('error', Error('Bug in RTSP data framing.'));
        // this.readState = READING_RAW_PACKET;
        break;
      }
    } // end while
  }

  _sendInterleavedData(channel, buffer) {
    if (!this._socket || !this._socket.writable) {
      return;
    }
    const req = `${buffer.length} bytes of interleaved data on channel ${channel}`;
    if (DEBUG) console.log(req, 'C->S');
    const header = Buffer.alloc(4);
    header[0] = 0x24; // ascii $
    header[1] = channel;
    header[2] = (buffer.length >> 8) & 0xff;
    header[3] = (buffer.length >> 0) & 0xff;
    const data = Buffer.concat([header, buffer]);
    this._socket.write(data);
  }

  _sendUDPData(host, port, buffer) {
    const udp = dgram.createSocket('udp4');
    udp.send(buffer, 0, buffer.length, port, host, (err, bytes) => {
      // TODO: Don't ignore errors.
      if (err !== null) {
        logger.error(err.message, bytes);
      }
      udp.close();
    });
  }

  _emptyReceiverReport() {
    const report = Buffer.alloc(8);
    const version = 2;
    const paddingBit = 0;
    const reportCount = 0; // an empty report
    const packetType = 201; // Receiver Report
    const length = report.length / 4 - 1; // num 32 bit words minus 1
    report[0] = (version << 6) + (paddingBit << 5) + reportCount;
    report[1] = packetType;
    report[2] = (length >> 8) & 0xff;
    report[3] = (length >> 0) & 0xff;
    report[4] = (this.clientSSRC >> 24) & 0xff;
    report[5] = (this.clientSSRC >> 16) & 0xff;
    report[6] = (this.clientSSRC >> 8) & 0xff;
    report[7] = (this.clientSSRC >> 0) & 0xff;
    return report;
  }
}
