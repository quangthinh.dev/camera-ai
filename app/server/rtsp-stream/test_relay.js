const ChildProcess = require('child_process');
const { Writable } = require('stream');
const WebSocket = require('ws');
const logger = require('../logger');
const { addDevice } = require('../media-server');

class Test extends Writable {
  constructor({ port }) {
    super();
    this.port = port;
    this.start = Date.now();
  }

  init() {
    this.wss = new WebSocket.Server({ port: this.port });
    const elapsed = Date.now() - this.start;
    console.log(`Server ready on port ${this.port}`, 'take ', elapsed, 'ms');

    this.wss.on('connection', ws => {
      ws.send(this.firstBuffer);
      ws.on('error', error => {
        console.log('WebSocket error', error);
      });
    });
  }

  write(chunk) {
    if (!this.firstBuffer) {
      this.firstBuffer = chunk;
      return this.init();
    }
    // console.log(chunk);
    // if (chunk.length < 100) return;
    this.wss.clients.forEach(ws => {
      ws.send(chunk);
    });
  }
}

logger.setLogType(logger.LOG_TYPES.FFDEBUG);
const run = async () => {
  const connectedDevice = await addDevice({
    url: 'rtsp://nguyenchanh.cameraddns.net:1554/Streaming/Channels/101/',
    user: 'demo',
    pass: 'admin123'
  });
  // console.log(connectedDevice.profile);
  const { cam } = connectedDevice;
  const test = new Test({ port: 3000 });

  wrapPipe(cam, test);
};

const wrapPipe = (cam, test) => {
  const spawnOptions = [
    '-threads',
    '1',
    '-hide_banner',
    '-loglevel',
    'panic',
    '-fflags',
    'nobuffer',
    '-fflags',
    '+genpts',
    '-i',
    'pipe:',
    '-c',
    'copy',
    '-an',
    '-preset',
    'ultrafast',
    '-flvflags',
    'no_duration_filesize',
    '-f',
    'flv',
    'pipe:1'
  ];
  const ffmpeg = ChildProcess.spawn('ffmpeg', spawnOptions);
  cam.pipeVideo('Extra', ffmpeg.stdin);
  ffmpeg.stdout.pipe(test);
};

run();
