/* eslint-disable no-plusplus */
/* eslint-disable no-bitwise */
/* eslint-disable no-constant-condition */
/* eslint-disable camelcase */
import { PassThrough } from 'stream';
import transform from 'sdp-transform';
import * as util from '../util';
// import logger from '../../logger';

export default class AACTransport extends PassThrough {
  constructor(client, { mediaSource, rtpChannel }) {
    super();
    this.ObjectType = 0;
    this.FrequencyIndex = 0;
    this.ChannelConfiguration = 0;
    this.client = client;

    client.on('data', (channel, packet) => {
      if (channel === rtpChannel) {
        this.processRTPPacket(packet);
      }
    });
    // Process the SDP to get the parameters for the AAC audio
    // "profile-level-id=1;mode=AAC-hbr;sizelength=13;indexlength=3;indexdeltalength=3;config=1490"
    const fmtp = mediaSource.fmtp[0];
    const fmtpConfig = transform.parseParams(fmtp.config);
    const bs = new util.BitStream();
    bs.AddHexString(fmtpConfig.config.toString());
    /** *
        5 bits: object type
            if (object type == 31)
            6 bits + 32: object type
        4 bits: frequency index
            if (frequency index == 15)
            24 bits: frequency
        4 bits: channel configuration
        var bits: AOT Specific Config
        ** */
    // Read 5 bits
    const ObjectType = bs.Read(5);
    // Read 4 bits
    const FrequencyIndex = bs.Read(4);
    // Read 4 bits
    const ChannelConfiguration = bs.Read(4);

    const bsHeader = new util.BitStream(); // TODO - we could cache the header bitstream
    bsHeader.AddValue(0xfff, 12); // (a) Start of data
    bsHeader.AddValue(0, 1); // (b) Version ID, 0 = MPEG4
    bsHeader.AddValue(0, 2); // (c) Layer always 2 bits set to 0
    const protection_absent = 1;
    bsHeader.AddValue(protection_absent, 1); // (d) 1 = No CRC
    bsHeader.AddValue(ObjectType - 1, 2); // (e) MPEG Object Type / Profile, minus 1
    bsHeader.AddValue(FrequencyIndex, 4); // (f)
    bsHeader.AddValue(0, 1); // (g) private bit. Always zero
    bsHeader.AddValue(ChannelConfiguration, 3); // (h)
    bsHeader.AddValue(0, 1); // (i) originality
    bsHeader.AddValue(0, 1); // (j) home
    bsHeader.AddValue(0, 1); // (k) copyrighted id
    bsHeader.AddValue(0, 1); // (l) copyright id start

    // cache
    this.bsHeader = bsHeader;
  }

  writeACC(header, data) {
    if (this.writable) {
      this.write(Buffer.concat([header, data]));
    }
  }

  processRTPPacket(packet) {
    // RTP Payload for MPEG4-GENERIC consis of multiple blocks of data
    // Each block has 3 parts
    // Part 1 - Acesss Unit Header Length + Header
    // Part 2 - Access Unit Auxiliary Data Length + Data (not used in AAC High Bitrate)
    // Part 3 - Access Unit Audio Data
    const rtp_payload = packet.payload;
    let ptr = 0;
    const audio_data = [];
    while (true) {
      if (ptr + 4 > rtp_payload.length) break; // 2 bytes for AU Header Length, 2 bytes of AU Header payload
      // Get Size of the AU Header
      const au_headers_length_bits =
        (rtp_payload[ptr] << 8) + (rtp_payload[ptr + 1] << 0); // 16 bits
      const au_headers_length = Math.ceil(au_headers_length_bits / 8.0);
      ptr += 2;
      // Examine the AU Header. Get the size of the AAC data
      const aac_frame_size =
        ((rtp_payload[ptr] << 8) + (rtp_payload[ptr + 1] << 0)) >> 3; // 13 bits
      // const aac_index_delta = rtp_payload[ptr + 1] & 0x03; // 3 bits
      ptr += au_headers_length;
      // extract the AAC block
      if (ptr + aac_frame_size > rtp_payload.length) break; // not enough data to copy
      audio_data.push(rtp_payload.slice(ptr, ptr + aac_frame_size));
      ptr += aac_frame_size;
    }
    // Write Audio Data Transport Stream (adts) header
    // followed by the AAC data
    for (let x = 0; x < audio_data.length; x++) {
      const data = audio_data[x];
      const bs = this.bsHeader.Clone(); // TODO - we could cache the header bitstream
      bs.AddValue(data.length + 7, 13); // (m) AAC data + size of the ASDT header
      bs.AddValue(2047, 11); // (n) buffer fullness ???
      const num_acc_frames = 1;
      bs.AddValue(num_acc_frames - 1, 1); // (o) num of AAC Frames, minus 1
      // If Protection was On [value=0], there would be a 16 bit CRC here
      // if (protection_absent === 0) bs.AddValue(0xabcd /* Calc CRC() */, 16); // (p)
      const header = bs.ToArray();
      // write to the aac file
      this.writeACC(header, data);
      // this.write(header);
      // this.write(data);
    }
  }
}
