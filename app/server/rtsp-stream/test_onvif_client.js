// import fs from 'fs';
import ONVIFClient from './ONVIFClient';
import { addDevice } from '../media-server';
import { SUB_STREAM } from '../constants/stream.json';

const run = async () => {
  const connectedDevice = await addDevice({
    url: 'rtsp://cchuymay.cameraddns.net:554/Streaming/Channels/1301/',
    user: 'demo',
    pass: 'admin123'
  });

  const onvifClient = new ONVIFClient(connectedDevice);
  await onvifClient.connect();

  const { video } = onvifClient.getVideoStream({ StreamType: SUB_STREAM });
  console.log(video);
  // const videoWriter = fs.createWriteStream('test.h264');
  // video.pipe(videoWriter);
  // setTimeout(() => {
  //   console.log('done');
  //   videoWriter.end();
  //   process.exit();
  // }, 5000);
};

run();
