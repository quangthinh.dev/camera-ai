import express from 'express';
import db from '../database';
// import fs from 'fs';
// import logger from '../logger';

export default function(modelPath) {
  // should use leveldb for example to store this item

  const router = express.Router();

  if (process.env.WEB === 'true') {
    // serve static weights maybe for load from url
    router.use('/weights', express.static(modelPath));
  }

  router.get('/descriptor/:label', async (req, res) => {
    const { label } = req.params;
    const descriptors = await db.getItem(`descriptors.${label}`, true);
    res.send(descriptors);
  });

  router.delete('/descriptor/:label', (req, res) => {
    const { label } = req.params;
    db.removeItem(`descriptors.${label}`);
    res.json({ label });
  });

  router.get('/descriptors', async (req, res) => {
    const labels = await db.getItem('descriptors');
    res.json(labels);
  });

  router.post('/descriptor/:label', (req, res, next) => {
    try {
      const { label } = req.params;

      db.setItem(`descriptors.${label}`, req.body, true);
      res.json({ success: true, bytesLength: req.body.length });
    } catch (ex) {
      next(ex);
    }
  });

  router.post('/descriptors', async (req, res, next) => {
    try {
      db.setItem('descriptors', req.body);
      res.json({ success: true });
    } catch (ex) {
      next(ex);
    }
  });

  return router;
}
