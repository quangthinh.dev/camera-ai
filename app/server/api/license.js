/* eslint-disable camelcase */
import express from 'express';
// import path from 'path';

import { LicenseKey, utils } from '../license';
import db from '../database';
import { SUCCESS, FAILURE, INVALID_INPUT } from '../status';
import logger from '../logger';

const rsa_private_key = `-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,5B1EF7CD00F44A63

zGBZtJInb3NiJ2+2lAdLwcp0lcWe57GjeMC9YkIFNTL9HbS/3YtsKbhT63acfkDQ
jvThLIo2p7Gs0xABSlsRZ4S2VOnxQ2FuK1B2RXgQurKYLJdH/jDLnaBYQlvddPLw
jEAsLnsgDBI/W5HF6Dmh5CgCQjt1vuW2J6yuxmZDkxsQ1q0Tdu2I9Cnq8htNTxsl
d2DBm/4XPYvjXyvG8GqOOoNSQdJrtlTVQZfV9PmN0joVDeK010op+aHJcteZ7LuK
IxeyKyRdcDXeCH3YQBZSOHPOIQG3jaj1ieUZ5D02ak4wlHcR3SQHBN3BfC0BoDAh
jYX2TEb400unLNMZIDryJb+mh3QezPQZ38lIxrMWFD+lykkZX7fx8tW16S7dVO88
snejaJzIQji+wFz1OCP7LAHTdhISN/AxYJLno4zBl2YUJuj2ubyJO5O4cekyeU9y
eHxkwq/LVKP5CHON2WoqzRQ8GDdod59epZjw77sok1M2WnVU36CgMP/VLgAjXBid
ZiGA8FFXHfx3sVzObf9MGjpTi3h02JLXwvpVnTAYyoPv54xgE/4C+Y/myYVWM1zm
oa2/HR19xeUqPpY5JG8LP1RIE2WNmi0I1NR7/XOGhWbVHXWtDjkry9LK5Zu5JdcA
rnt07AmOtFQn6FP/5eTGNBj3CpHGEXTMdALuXwe77/ZtgmvOosS4X6zBp08tkp0D
Ap1c2WLy86ysVo1rwXtHlrKRdAqDOYSih6ldMqlUSEfjnsJtjNtPWzHuNsIga+fu
XKYWYWppoI/5mO2AebBO9V9NFyyIBJ2dFx0Kbk1BSRN/Erkw4/Ralw==
-----END RSA PRIVATE KEY-----`;

export default function() {
  const model = new LicenseKey({
    identity: 'CameraAI', // client software identity
    expireAfter: 365 * 24 * 60 * 60 * 1000, // 1 year
    rsa_private_key,
    rsa_passphrase: '1234'
  });

  const router = express.Router();
  router.get('/generate', (req, res, next) => {
    const { key, status } = model.issue({
      identity: 'CameraAI'
    });

    if (status !== SUCCESS) {
      return next();
    }

    // get machine id
    const { machineId } = req.query;

    const data = model.validate(key);
    if (!data) return res.json({ status: INVALID_INPUT });

    const license = model.generateLicense(key, machineId);
    return res.json({ status: SUCCESS, license });
  });

  router.post('/authorize', async (req, res) => {
    const { licenseKey, machineId } = req.body;
    const buf = Buffer.from(licenseKey, 'hex');
    await db.setItem(`license.${machineId}`, buf, true);

    res.json({ status: SUCCESS });
  });

  router.get('/authorize/:machineId', async (req, res) => {
    const { machineId } = req.params;
    const licenseKeyBuf = await db.getItem(`license.${machineId}`, true);
    if (!licenseKeyBuf)
      return res.json({ status: FAILURE, message: 'no license' });
    // convert key to hex
    const key = licenseKeyBuf.subarray(0, 256).toString('hex');
    const buf = licenseKeyBuf.subarray(256);

    try {
      const license = JSON.parse(
        utils.crypt(model.PrivateKey, buf, false).toString()
      );

      if (
        license.key === key &&
        license.machine === machineId &&
        license.identity === model.config.identity
      ) {
        if (
          license.meta.persist ||
          (license.meta.startDate < Date.now() &&
            license.meta.endDate > Date.now())
        ) {
          res.json({ status: SUCCESS });
        } else {
          res.json({
            status: FAILURE,
            message: 'invalid effect date of license'
          });
        }
      } else {
        res.json({ status: FAILURE, message: 'invalid license' });
      }
    } catch (ex) {
      logger.log(ex.message);
      return res.json({
        status: FAILURE,
        message: 'invalid license'
      });
    }
  });

  return router;
}
