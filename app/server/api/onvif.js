import express from 'express';
import onvif from '../onvif';
import { getDevice, addDevice, removeDevice } from '../media-server';

import { createPromiseTimeout } from '../utils';
// import logger from '../logger';

export default function() {
  const router = express.Router();

  router.post('/device_discover', async (req, res) => {
    const { target, port } = req.body;
    try {
      const ret = await onvif.startScan({
        target,
        port
      });

      res.send(ret);
    } catch (error) {
      res.send({ id: 'connect', error: error.message });
    }
  });

  router.get('/snapshot', async (req, res) => {
    const { url, timeout = 3000 } = req.query;

    const connectedDevice = getDevice(url);
    if (connectedDevice) {
      try {
        const snapshotRes = await Promise.race([
          connectedDevice.device.fetchSnapshot(),
          createPromiseTimeout(timeout)
        ]);
        if (snapshotRes) {
          res.contentType(snapshotRes.headers['content-type']);
          res.send(snapshotRes.body);
        } else {
          res.send();
        }
      } catch (error) {
        res.send(error);
      }
    } else {
      res.send({ error: 'Device is not connected' });
    }
  });

  router.post('/device_connect', async (req, res) => {
    const { user, pass, url, tcp, timeout = 5000, isIPCam } = req.body;

    const connectedDevice = await addDevice({
      url,
      user,
      pass,
      tcp,
      timeout,
      isIPCam
    });

    res.send(connectedDevice.profile);
  });

  router.post('/device_profile', (req, res) => {
    const { url } = req.body;
    const connectedDevice = getDevice(url);
    let ret;
    if (connectedDevice) {
      ret = connectedDevice.profile;
    } else {
      ret = {};
    }
    res.send(ret);
  });

  router.post('/device_disconnect', (req, res) => {
    const { url } = req.body;
    const ret = removeDevice(url);
    res.send({ success: ret });
  });

  router.post('/device_execute', async (req, res) => {
    const { action, url, distance = 0.5, timeout = 3000 } = req.body;
    const connectedDevice = getDevice(url);

    if (connectedDevice) {
      try {
        await connectedDevice.move(action, distance, timeout);
        res.send({ message: 'moved' });
      } catch (err) {
        res.send({ error: err.message });
      }
    } else {
      res.send({ error: 'device not found' });
    }
  });

  return router;
}
