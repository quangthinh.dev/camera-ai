/* eslint-disable prefer-const */
import express from 'express';
// import { Validator } from 'express-json-validator-middleware';
// import swagger from 'swagger-spec-express';

import path from 'path';
import fs from 'fs';
import { MAIN_STREAM } from '../constants/stream.json';
import logger from '../logger';
import db from '../database';
import { getStreamName } from '../utils/common';
import {
  getDevicesInfo,
  addStream,
  updateSaveFolder,
  getSaveFolder,
  updateSaveMotion,
  updateVideoLength,
  updateStream,
  getStream,
  // removeStream,
  getStreamPathURL,
  getDevice
  // getItem
  // getTransSession,
  // setStream
} from '../media-server';

const checkDiskSpace = require('check-disk-space');

const checkFreeDisk = async folder => {
  let diskSpace = await checkDiskSpace(folder);
  return diskSpace;
};

const videoExt = '.mp4';

export default function (streamAppRoot, ffmpegPath, imageRepository) {
  const router = express.Router();

  router.get('/getDevices', async (req, res) => {
    const item = await getDevicesInfo();
    res.send({ item });
  });

  router.post('/addDevice', async (req, res) => {
    const device = req.body;
    const ret = await db.setItem(`devices.${device.url}`, device);
    res.send({ ret });
  });

  router.post('/deleteDevice', async (req, res) => {
    const device = req.body;
    const ret = await db.removeItem(`devices.${device.url}`);
    res.send({ ret });
  });

  router.get('/media/:streamPath/:filename', (req, res) => {
    const { filename, streamPath } = req.params;
    const folderPath = getSaveFolder();
    res.sendFile(path.join(folderPath, streamPath, filename));
  });

  router.post('/history', async (req, res) => {
    let {
      items,
      ext = videoExt,
      startTime = '',
      endTime = '',
      faces = ''
    } = req.body;
    const folderPath = getSaveFolder();
    if (ext === videoExt) {
      items = items
        .map(item => {
          const streamName = getStreamName(item.url, MAIN_STREAM);
          const streamPath = path.join(folderPath, streamName);
          return {
            name: item.name,
            streamName,
            streamPath
          };
        })
        .filter(item => fs.existsSync(item.streamPath));

      // read all folder from streamAppRoot that match streamName
      const files = Array.prototype.concat.apply(
        [],
        items.map(item =>
          fs
            .readdirSync(item.streamPath)
            .filter(file => file.endsWith(ext))
            .map(file => ({
              name: item.name,
              fileName: file.replace(ext, ''),
              streamURL: `${getStreamPathURL(item.streamName)}/${file}`
            }))
        )
      );
      res.send(files);
    } else {
      const streams =
        items && items.length !== 0
          ? items.map(item => {
            const streamName = getStreamName(item.url, MAIN_STREAM);
            return streamName;
          })
          : [];
      const images = await imageRepository.getByDate(
        streams,
        startTime,
        endTime
      );
      let imagesFilter = [];
      const reqFaces = faces === '' ? '' : JSON.parse(faces);
      if (images && images.length) {
        images.forEach(item => {
          const fileName = item.filename.replace(ext, '');
          const faceKnowns = JSON.parse(item.faces);
          if (reqFaces && reqFaces.length > 0) {
            if (faceKnowns && faceKnowns.length === 0) {
              return;
            }
            reqFaces.forEach(element => {
              if (faceKnowns.includes(element)) {
                return imagesFilter.push({
                  name: item.url_stream,
                  fileName,
                  streamURL: `${getStreamPathURL(
                    item.stream_name
                  )}/${fileName}.jpg`
                });
              }
            });
          } else {
            return imagesFilter.push({
              name: item.url_stream,
              fileName,
              streamURL: `${getStreamPathURL(item.stream_name)}/${fileName}.jpg`
            });
          }
        });
      }
      const files = Array.prototype.concat.apply([], imagesFilter);
      // images && images.length !== 0
      //   ? images.for(item => {
      //       const fileName = item.filename.replace(ext, '');
      //       return {
      //         name: item.url_stream,
      //         fileName,
      //         streamURL: `${getStreamPathURL(item.stream_name)}/${fileName}.jpg`
      //       };
      //     })
      //   : [];

      res.send(files);
    }
  });
  router.post('/get_media_path', async (req, res) => {
    // let { appDataPath, videoLength, fps, vfSave } = req.body;
    // logger.info('streamAppRoot ' + streamAppRoot)
    // streamAppRoot = appDataPath;
    const folderPath = getSaveFolder();
    res.send({ mediaPath: folderPath });
  });

  router.post('/update_video_length', async (req, res) => {
    let { videoLength } = req.body;
    // logger.info(`videoLength ${videoLength}`);
    const ret = updateVideoLength(videoLength);
    res.send({ ret });
  });

  router.post('/update_only_motion', async (req, res) => {
    let { vfSave } = req.body;
    const ret = updateSaveMotion(vfSave);
    res.send({ ret });
  });

  router.post('/record', async (req, res) => {
    let {
      url,
      streamType,
      save,
      saveAuto,
      saveDate,
      scheduler,
      duration,
      streamName
    } = req.body;

    logger.info(`get url ${url}`);
    const connectedDevice = getDevice(url);
    if (!connectedDevice) {
      logger.info('device null');
      return;
    }

    // let device = device || (getItem(`devices.${url}`));

    const savestreamName = getStreamName(url, streamType);

    logger.info(`record stream ${savestreamName}`);

    let videoStream = getStream(savestreamName);

    logger.info(`videoStream ${videoStream}`);

    // streamName = streamName || getStreamName(url);
    // const videoStream = getStream(streamName);

    if (videoStream) {
      // let appRoot = streamAppRoot;
      // if (videoStream.saveFolder) {
      //   appRoot = videoStream.saveFolder;
      // }
      // const filePath = path.join(appRoot, streamName);
      // if (!fs.existsSync(filePath)) {
      //   fs.mkdirSync(filePath, { recursive: true });
      // }

      videoStream.record = save;
      videoStream.saveAuto = saveAuto;
      videoStream.saveDate = saveDate;
      videoStream.saveDuration = duration * 60 * 60;
      videoStream.saveRepeat = scheduler;

      videoStream.checkSaveRule();
    }
    res.send({ streamName });
  });

  // swagger.swaggerize(router);
  router.post('/slot', async (req, res) => {
    const { url, stream } = req.body;
    const streamName = getStreamName(url, stream);
    const videoStream = getStream(streamName);
    let streamURL = '';
    if (videoStream) {
      streamURL = videoStream.streamUrl;
    }
    res.send({ streamURL });
  });

  router.post('/update_stream', async (req, res, next) => {
    const { fps } = req.body;
    let streamURL = '';
    if (fps) {
      try {
        streamURL = await updateStream(fps);
      } catch (ex) {
        next(ex);
      }
    }
    res.send({ streamURL });
  });

  router.get('/get_save_folder', async (req, res) => {
    // const { abc } = req.body;
    const folderPath = getSaveFolder();

    res.send({ path: folderPath });
  });

  router.post('/add', async (req, res, next) => {
    const { url, stream } = req.body;
    let streamURL = '';
    if (url) {
      try {
        streamURL = await addStream({
          url,
          stream,
          ffmpegPath
        });
      } catch (ex) {
        next(ex);
      }
    }
    res.send({ streamURL });
  });

  router.post('/change_save_dir', async (req, res) => {
    // const folderPath = getSaveFolder();

    // const free = await checkFreeDisk(folderPath);
    const { newPath } = req.body;
    if (newPath) {
      updateSaveFolder(newPath);
    }
    res.send({ success: true });
  });

  router.post('/save_image_path', async (req, res) => {
    const { imgPath, streamName, urlStream, faces } = req.body;
    // const arrFilenames = imgPath.split('/');
    const filename = imgPath;
    // const faceKnowns = JSON.parse(faces);
    await imageRepository.create(filename, streamName, urlStream, faces);
    res.send({ success: true });
  });

  // router.post('/history_save_image', async (req, res) => {
  //   const { startTime, endTime } = req.body;
  //   const ret = await imageRepository.getByDate(startTime, endTime);
  // });

  return router;
}
