import express from 'express';
import db from '../database';
import Mailer from '../utils/mailer';

export default function() {
  const router = express.Router();

  // router.get('/mail', async (req, res) => {
  //   const { mailAddress, mailPass } = await db.getItem('settings');
  //   const { Warning } = await db.getItem('form');

  //   const mailer = new Mailer({ user: mailAddress, pass: mailPass });
  //   const subject = 'Cảnh báo';
  //   const message = 'Có 12 người';
  //   mailer.send(subject, message, 'tubackkhoa@gmail.com');
  //   res.send({ warning: Warning.values.warning, mailAddress, mailPass });
  // });

  router.post('/sendMail', async (req, res) => {
    const { mailAddress, mailPass } = await db.getItem('settings');
    const { subject, message, bbc, imagePath } = req.body;

    const mailer = new Mailer({ user: mailAddress, pass: mailPass });

    const info = mailer.send(subject, message, bbc, imagePath);
    res.send(info);
  });

  return router;
}
