import express from 'express';
import path from 'path';
import fs from 'fs';
import zlib from 'zlib';
import logger from '../logger';

const bufSep = Buffer.from('##CameraAI##');

export default function(bundlePath, appPath) {
  const router = express.Router();
  const modulePath = path.join(appPath, 'modules');

  router.use('/list/:target', (req, res) => {
    let modules;
    const { target } = req.params;
    if (process.env.NODE_ENV !== 'production') {
      // get from modules path relative to this

      modules = fs
        .readdirSync(modulePath, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent =>
          fs
            .readFileSync(path.join(modulePath, dirent.name, 'package.json'))
            .toString()
        );
    } else {
      // get from bundlePath and return package.json
      const postfix = `${target}.prod.json`;
      modules = fs
        .readdirSync(bundlePath)
        .filter(name => name.endsWith(postfix))
        .map(name => fs.readFileSync(path.join(bundlePath, name)).toString());
    }

    // we may return more information
    res.setHeader('Content-Type', 'application/json');
    res.send(`[${modules.join(',')}]`);
  });

  if (process.env.NODE_ENV !== 'production') {
    router.get('/download/:target/:moduleName/', (req, res) => {
      const { target, moduleName } = req.params;
      const prefix = `${moduleName}.${target}.prod`;
      const filter = [`${prefix}.css`, `${prefix}.js`, `${prefix}.json`]; // 3 files: css, js, json
      const modulesContent = [Buffer.from(prefix)];

      fs.readdirSync(bundlePath)
        .filter(name => filter.includes(name))
        .sort() // .css, .js, .json
        .map(name => fs.readFileSync(path.join(bundlePath, name)))
        .forEach(buf => {
          modulesContent.push(bufSep);
          modulesContent.push(buf);
        });

      const finalBuf = zlib.gzipSync(Buffer.concat(modulesContent));
      res.end(finalBuf);
    });
  }

  router.get('/delete/:target/:moduleName/', (req, res) => {
    const { target, moduleName } = req.params;
    const prefix = `${moduleName}.${target}.prod`;

    fs.readdirSync(bundlePath)
      .filter(name => name.startsWith(prefix))
      .forEach(name => fs.unlinkSync(path.join(bundlePath, name)));

    res.json({ prefix });
  });

  router.post('/upload', (req, res) => {
    const out = zlib.unzipSync(req.body);
    const [prefix, css, js, json] = out.toString().split(bufSep);
    logger.log('[module prefix]', prefix);

    // write at async is better
    fs.writeFileSync(path.join(bundlePath, `${prefix}.css`), css);
    fs.writeFileSync(path.join(bundlePath, `${prefix}.js`), js);
    fs.writeFileSync(path.join(bundlePath, `${prefix}.json`), json);

    res.json({ prefix });
  });

  return router;
}
