import express from 'express';
// import logger from '../logger';
import db from '../database';

export default function() {
  const router = express.Router();

  router.get('/getItem/:key', async (req, res) => {
    const { key } = req.params;
    const item = await db.getItem(key);
    res.send({ item });
  });

  router.post('/saveItem', async (req, res) => {
    const { key, item } = req.body;
    const ret = await db.setItem(key, item);
    res.send({ ret });
  });

  return router;
}
