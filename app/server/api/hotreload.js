import express from 'express';
import logger from '../logger';

export default function() {
  const router = express.Router();
  router.get('/', (req, res) => {
    logger.log('hello world');
    res.send('hello world hot module');
  });

  router.post('/buffer', async (req, res) => {
    const { buffer } = new Uint8Array(req.body);
    const descriptor = new Float32Array(buffer);

    res.send(descriptor.toString());
  });

  return router;
}
