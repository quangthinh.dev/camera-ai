/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-plusplus */
/* eslint-disable max-classes-per-file */
// const ipReg = /(?=.*[^\.]$)(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}/;

import dayjs from 'dayjs';
import md5 from 'md5';

const protocolReg = /[\w:]+(?=\/\/)/;

export const getPTZMove = (action, moveDistance = 0.01, timeout = 3) => {
  const move = {
    speed: {
      x: 0.0, // Speed of pan (in the range of -1.0 to 1.0)
      y: 0.0, // Speed of tilt (in the range of -1.0 to 1.0)
      z: 0.0 // Speed of zoom (in the range of -1.0 to 1.0)
    },
    timeout // seconds
  };
  switch (action) {
    case 'right':
      move.speed.x = moveDistance;
      break;
    case 'left':
      move.speed.x = -moveDistance;
      break;
    case 'up':
      move.speed.y = moveDistance;
      break;
    case 'down':
      move.speed.y = -moveDistance;
      break;
    case 'ptz-zoom-in':
      move.speed.z = moveDistance;
      break;
    case 'ptz-zoom-out':
      move.speed.z = -moveDistance;
      break;
    default:
      break;
  }

  return move;
};

const defaultProtocol = 'http:';
export class StringURL extends URL {
  constructor(url) {
    const match = url.match(protocolReg);
    // change protocol to fix bug
    if (match) {
      const protocol = match[0];
      if (protocol !== defaultProtocol) {
        const modifiedURL = url.replace(protocolReg, defaultProtocol);
        super(modifiedURL);
        this.realProtocol = protocol;
        return;
      }
    }

    super(url);
  }

  // overide methods
  get protocol() {
    return this.realProtocol;
  }

  get href() {
    let href = super.href;
    if (this.realProtocol) {
      href = href.replace(protocolReg, this.realProtocol);
    }
    return href;
  }

  get origin() {
    let origin = super.origin;
    if (this.realProtocol) {
      origin = origin.replace(protocolReg, this.realProtocol);
    }
    return origin;
  }

  toString() {
    let url = super.toString();
    if (this.realProtocol) {
      url = url.replace(protocolReg, this.realProtocol);
    }
    return url;
  }
}

export const getUrlWithLogin = (url, user, pass) => {
  const urlObj = new StringURL(url);
  if (user) urlObj.username = user;
  if (pass) urlObj.password = pass;
  return urlObj.toString();
};

export const createPromiseTimeout = timeout =>
  new Promise(resolve => setTimeout(resolve, timeout));

export const getSize = resolution => resolution.width * resolution.height;

export const fixURL = url => new StringURL(url).toString();

const regApp = /\/([^/]+)\/([^/]+)/;
export const getAppAndName = streamPath => {
  if (!streamPath) return [];
  // console.log(`getAppAndName: ${streamPath}`);
  const regRes = streamPath.match(regApp);
  if (regRes === null) return [];
  return regRes.slice(1);
};

export const getSnapshot = (canvas, quality = 1, contentType = 'image/jpeg') =>
  new Promise(resolve => {
    canvas.toBlob(resolve, contentType, quality);
  });

export const copyVideo2Canvas = (video, canvas) => {
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
};

export class RingBuffer {
  constructor(length, onClean) {
    this.buffer = new Array(length);
    this.pointer = 0;
    this.onClean = onClean;
  }

  get(i) {
    return this.buffer[i];
  }

  push(item) {
    if (this.onClean && this.buffer[this.pointer]) {
      this.onClean(this.buffer[this.pointer]);
    }
    this.buffer[this.pointer] = item;
    this.pointer++;
    if (this.pointer === this.buffer.length) {
      this.pointer = 0;
    }
  }

  clean() {
    if (this.onClean) {
      for (const i in this.buffer) {
        if (this.buffer[i]) {
          // do job for item, and delete it so we do not re-clean
          this.onClean(this.buffer[i]);
          delete this.buffer[i];
        }
      }
    }
  }
}

export const flatten = (
  object,
  prev,
  output = {},
  delimiter = '.',
  currentDepth = 1
) => {
  Object.entries(object).forEach(([key, value]) => {
    const newKey = prev ? `${prev}${delimiter}${key}` : key;
    if (typeof value === 'object') {
      return flatten(value, newKey, output, delimiter, currentDepth + 1);
    }
    output[newKey] = value;
  });
  return output;
};

const startDayjs = dayjs('0 00:00:00');
export const getShortTime = time => {
  let shortTime = startDayjs.add(time, 'second').format('HH:mm:ss');
  if (shortTime.substr(0, 2) === '00') {
    shortTime = shortTime.substr(3);
  }

  return shortTime;
};

// uniq for stream
export const getStreamName = (url, stream) =>
  url && typeof url === 'string' ? md5(`${url}.${stream}`) : null;
