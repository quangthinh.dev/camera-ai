/* eslint-disable no-undef */
/* eslint-disable camelcase */
/* eslint-disable no-unused-expressions */
/* eslint-disable import/prefer-default-export */

// import { SERVER_BASE } from '../api';

// // reload module ?
// const appendLink = (name, reload = false) => {
//   const href = `${SERVER_BASE}/${name}.${process.env.TARGET}.prod.css`;
//   let link = document.querySelector(`head>link[href="${href}"]`);
//   if (link) {
//     if (!reload) return link;
//     link.parentElement.removeChild(link);
//   }
//   link = document.createElement('link');
//   link.type = 'text/css';
//   link.rel = 'stylesheet';
//   link.href = href;
//   // HACK: Writing the link to document
//   document.getElementsByTagName('head')[0].appendChild(link);
// };

// load module and bundle all module with default entry index.js only
export const loadModule = name => {
  // change publicPath on the fly for production module load
  // if (process.env.NODE_ENV === 'production') {
  //   // appendLink(name);
  //   const current_public_path = __webpack_public_path__;
  //   __webpack_public_path__ = `${SERVER_BASE}/`;
  //   // now public path is applied
  //   const promise = import(
  //     /* webpackChunkName: "[request]" */
  //     /* webpackInclude: /modules\/\w+\/index.jsx?$/ */
  //     `../../modules/${name}`
  //   );
  //   // restore
  //   __webpack_public_path__ = current_public_path;
  //   return promise;
  // }

  return import(
    /* webpackChunkName: "[request]" */
    /* webpackInclude: /modules\/\w+\/index.jsx?$/ */
    `../../modules/${name}`
  );
};
