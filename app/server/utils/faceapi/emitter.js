let emitter;

export default async function initEmitter() {
  if (emitter || process.env.DISABLED_AI === 'true') return;
  // base on env
  const mod =
    process.env.TARGET === 'web'
      ? await import('./faceapi-web')
      : await import('./faceapi-electron');

  emitter = mod.emitter;
  return emitter;
}

export const assignDetectionTask = (task, ...args) => {
  if (!emitter) return;
  emitter.emit(`assign-task.${task}`, ...args);
};

export const addDetectionListener = (task, handler) => {
  if (!emitter) return;
  emitter.on(`done-task.${task}`, handler);
};

export const removeDetectionListener = (task, handler) => {
  if (!emitter) return;
  if (handler) emitter.removeListener(`done-task.${task}`, handler);
  else emitter.removeAllListeners(`done-task.${task}`);
};
