/* eslint-disable import/prefer-default-export */
/* eslint-disable global-require */
import work from 'webworkify-webpack';
import EventEmitter from '../../components/FLV/events';
import { API_BASE } from '../../api/common';

// single thread, wrap this
export const emitter = new EventEmitter();
const modelPath = `${API_BASE}/model/weights`;

let assignTaskHandler;

// has worker
if (typeof Worker !== 'undefined') {
  try {
    const FaceApiWorker = require.resolve('./faceapi-worker.js');
    // console.log(FaceApiWorker);
    const worker = work(FaceApiWorker);
    worker.postMessage({
      cmd: 'initModel',
      args: { modelPath, patch: false, API_BASE }
    });

    assignTaskHandler = task =>
      emitter.addListener(`assign-task.${task}`, (canvas, blobURL, slot) => {
        const context = canvas.getContext('2d');
        const imgData = context.getImageData(0, 0, canvas.width, canvas.height);
        worker.postMessage(
          {
            cmd: task,
            slot,
            args: [canvas.width, canvas.height, imgData.data.buffer, blobURL]
          },
          [imgData.data.buffer]
        );
      });

    worker.addEventListener('message', ({ data: { cmd, slot, args } }) => {
      emitter.emit(`done-task.${cmd}.${slot}`, args);
    });
  } catch (error) {
    console.log(error);
  }
} else {
  // webworkify using webpack module to extract to blob resource for worker
  // so no need to use import
  const faceApiModule = require('./core');
  faceApiModule.initModel({ modelPath, API_BASE });

  assignTaskHandler = task =>
    emitter.addListener(
      `assign-task.${task}`,
      async (canvas, blobURL, slot) => {
        if (!faceApiModule.isInitialized()) return;
        // const start = Date.now();
        const ret = await faceApiModule[task](canvas, blobURL);
        // ret = await getDescriptorData(ret);
        // console.log(Date.now() - start, 'ms', ret);
        emitter.emit(`done-task.${task}.${slot}`, ret);
      }
    );
}

assignTaskHandler('detectImage');
assignTaskHandler('predictObjects');
