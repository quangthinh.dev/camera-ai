/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
/* eslint-disable class-methods-use-this */
import nodemailer from 'nodemailer';
// import logger from '../server/logger';
import path from 'path';
import {
  getSaveFolder
  // getItem
  // getTransSession,
  // setStream
} from '../media-server';

// const fs = require('fs');

export default class Mailer {
  constructor({ service = 'gmail', user, pass } = {}) {
    this.transporter = nodemailer.createTransport({
      service,
      auth: {
        user,
        pass
      }
    });

    this.mailOpt = { from: user, to: null };
  }

  getHtml(subject, body, align) {
    return `<!DOCTYPE html>
    <html
      lang="en"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
    >
      <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="x-apple-disable-message-reformatting" />
        <title>${subject}</title>
      </head>
      <body>
        <div
          style="max-width:600px; width:100%;text-align: ${align};"
        >
          <h3>${subject}</h3>
          <hr/>
          ${body}
      </div>
      </body>
    </html>
    `; // html
  }

  getAttachments = imagePath => {
    // const reader = new FileReader();
    // // eslint-disable-next-line no-unused-vars
    // let base64data = '';
    // reader.readAsDataURL(blobURL);
    // reader.onloadend = () => {
    //   base64data = reader.result;
    // };
    // const folderPath = getSaveFolder();
    // const subImagePath = imagePath.split('/');
    // const l = subImagePath.length;
    // const imgPath = path.join(
    //   folderPath,
    //   `${subImagePath[l - 2]}/${subImagePath[l - 1]}`
    // );
    const folderPath = getSaveFolder();
    const imgPath = path.join(folderPath, imagePath);
    const arrFileNames = imagePath.split('/');
    const fileName = arrFileNames[arrFileNames.length - 1];
    const attachments = [
      {
        filename: fileName,
        // eslint-disable-next-line new-cap
        path: imgPath
      }
    ];
    return attachments;
  };

  sendPromise(mailOpt) {
    return new Promise((resolve, reject) => {
      this.transporter.sendMail(mailOpt, (error, info) => {
        console.log(error, info);
        if (error) {
          reject(error);
        } else {
          resolve(info);
        }
      });
    });
  }

  async send(subject, message, bcc, imagePath, retry = 3, align = 'left') {
    const html = this.getHtml(subject, message, align);
    const attachments = this.getAttachments(imagePath);
    console.log(attachments);
    const mailOpt = {
      ...this.mailOpt,
      bcc,
      subject,
      html,
      attachments
    };

    for (let i = 0; i < retry; i++) {
      try {
        const info = await this.sendPromise(mailOpt);
        return info;
      } catch (error) {
        return error;
      }
    }
  }
}
