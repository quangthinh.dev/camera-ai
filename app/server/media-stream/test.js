import path from 'path';
import { Writable, PassThrough } from 'stream';
import Cam from '../onvif/cam';
import { addDevice } from '../media-server';
// import FileAdapterStream from '../media-stream/FileAdapterStream';

const run = async () => {
  const connectedDevice = await addDevice({
    // 'http://192.168.1.195:8899/onvif/device_service',
    // 'admin',
    // ''

    url: 'rtsp://192.168.1.200:8554/live.sdp',
    tcp: true
    // user: 'admin',
    // pass: 'abcd1234'
  });

  console.log(connectedDevice.profile);
  const { cam } = connectedDevice;

  // const test = new FileAdapterStream({
  //   //filename: path.join(__dirname, 'test.h264')
  //   folder: __dirname,
  //   ffmpegPath: "ffmpeg.exe",
  //   duration: 1 //minutes
  // });

  const test = new Writable();
  test.write = b => console.log(b.length);
  cam.pipeVideo('Main', test);
};

run();
