/* eslint-disable no-return-assign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import { spawn, execSync } from 'child_process';
// import { PassThrough } from 'stream';
// import { EventEmitter } from 'events';
import dayjs from 'dayjs';
import path from 'path';
import fs from 'fs';
import { CronJob } from 'cron';
import logger from '../logger';
import { MAIN_STREAM } from '../constants/stream.json';
import { format } from '../constants/date.json';

const FileAdapterStream = require('./FileAdapterStream').default;

// function createFileStream(filename, flags = 'a') {
//   return fs.createWriteStream(filename, { flags });
// }

export type MediaStreamOptions = {
  name: string,
  connectedDevice: any,
  camStream?: string,
  // rtmpUrl: string,
  streamUrl: string,
  // username: string,
  // password: string,
  timeout: number,

  saveFormat: string,
  saveDuration: number,
  saveAuto: boolean,
  saveDate: string,
  record: boolen
};

export default class MediaStream {
  static fps = 25;

  static rootFolder =
    process.env.MEDIA_ROOT || path.join(process.cwd(), 'media');

  static vfSave = false;

  static fileLength = 60;

  static ffmpegSpeed = 'ultrafast';

  static async create(options) {
    const instance = new MediaStream(options);
    await instance.init();
    return instance;
  }

  constructor(options) {
    // super();
    this.pipes = new Set();

    this.setOptions(options);

    // this.init();

    // return this;
  }

  setOptions(options: MediaStreamOptions) {
    this.ffmpegOptions = options.ffmpegOptions || this.ffmpegOptions;
    this.name = options.name || this.name;
    // if (options.cam) {
    this.connectedDevice = options.connectedDevice;
    this.camStream = options.camStream;
    // } else {
    //   this.rtspUrl = options.rtspUrl;
    //   this.username = options.username;
    //   this.password = options.password;
    // }
    // this.wss = options.wss;

    // this.rtmpUrl = options.rtmpUrl;
    this.streamUrl = options.streamUrl;

    // timeout after 10 seconds
    // this.timeout = options.timeout || 6000;

    // this.saveFolder = options.saveFolder;
    this.saveFormat = options.saveFormat || format;
    this.saveDuration = 1800; // in second
    this.saveAuto = options.saveAuto || false;
    this.saveDate = null;
    this.record = options.record || false;
    // this.record = false;
    this.streamPath = null;
    this.saveExtension = 'h264';
    this.saveRepeat = false;
    this.additionalFlags = [];
    if (this.ffmpegOptions) {
      for (const key in this.ffmpegOptions) {
        this.additionalFlags.push(key);
        if (String(this.ffmpegOptions[key]) !== '') {
          this.additionalFlags.push(String(this.ffmpegOptions[key]));
        }
      }
    }

    // use arrow is the same
    this.startFlvStream = this.startFlvStream.bind(this);

    // start stream
    this.inputStreamStarted = true;
    this.ffmpegPath = options.ffmpegPath;
  }

  async init() {
    // start this to init camera first
    await this.startFlvStream();

    // need savefolder
    // if cronInterval => check time, if start => run, find h264 file nearest,
    // append if duration, else resolveUnsave, other resolveUnsave, else by everyday if repeat
    // else if normal record => append h264 file, else => resolveUnsave

    // use this to continue doing something unfinished
    // if (this.saveFolder) this.checkSaveRule();
    if (this.camStream === MAIN_STREAM) {
      this.checkSaveRule();
    }
  }

  checkSaveRule() {
    // cronInterval
    if (!this.record) {
      // unsave
      if (this.saveAuto) {
        this.stopJob();
      } else {
        this.unsave();
        // this.unsaveWithParam();
      }
      return;
    }
    // const saveFolder = path.join(MediaStream.rootFolder, this.name);
    // const outFiles = this.findUnfinishedSavingFiles(saveFolder);
    // //let lastItem, processingItems;
    // //if not saveAuto, rename to mp4
    // // if (!this.saveAuto) {
    // outFiles.map(file => {
    //   if (fs.existsSync(file)) {
    //     this.convertFile(file);
    //     console.log('unlink ' + file);
    //     fs.unlinkSync(file);
    //     logger.log('Conver to mp4 ', file);
    //   }
    // });
    // }
    // else {
    //   // get nearest file by day
    //   processingItems = outFiles
    //     .map(file => {
    //       const time = dayjs(path.basename(file, '.mp4'), this.saveFormat);
    //       return { file, time };
    //     })
    //     .sort((a, b) => a.time - b.time);
    // }

    if (this.saveAuto) {
      if (this.saveDate) {
        // lastItem = processingItems.pop();
        // if (lastItem) {
        //   const now = dayjs();
        //   const remainMilliseconds =
        //     this.saveDuration * 1000 - now.diff(lastItem.time);
        //   if (remainMilliseconds > 0 /* && within duration */) {
        //     this.doSaveByRule(lastItem.file, remainMilliseconds);
        //   }
        // } else {
        this.doSaveByRule();
        // processingItems.push(lastItem);
        // }
      } else {
        // processingItems.push(lastItem);
        console.log('scheduler need saveDate');
      }
    } else {
      if (this.record && this.camStream === MAIN_STREAM) {
        this.save();
      }
      // continue appeding the last file
      // lastItem = processingItems.pop();
      // if (lastItem) {
      //   this.save(lastItem.file);
      // } else {
      // this.save();
      // if (this.record && this.camStream === MAIN_STREAM) {
      //   this.save();
      // }
      // }
      // resolveUnsave
      // processingItems.map(item => {
      //   if (item) {
      //     this.unsave(item.file);
      //   }
      // });
    }
  }

  getFilenameByNow() {
    const saveFolder = path.join(MediaStream.rootFolder, this.name);
    if (!fs.existsSync(saveFolder)) {
      fs.mkdirSync(saveFolder, { recursive: true });
    }

    return path.join(
      saveFolder,
      `${dayjs().format(this.saveFormat)}.${this.saveExtension}`
    );
  }

  // filename is start file to save
  doSaveByRule(lastFile, remainMilliseconds) {
    // continue saving the current job
    if (lastFile) {
      this.save(lastFile);
      // unsave in next duration second, and stop connection
      setTimeout(() => this.unsave(), remainMilliseconds);
    }

    this.stopJob();

    const start = dayjs(this.saveDate, this.saveFormat);
    let cronTime;
    if (this.saveRepeat) {
      cronTime = `${start.second()} ${start.minute()} ${start.hour()} * * *`;
    } else {
      cronTime = `${start.second()} ${start.minute()} ${start.hour()} ${start.date()} ${start.month()} *`;
    }

    logger.info(`crontime ${cronTime}`);

    const onTick = () => {
      // must be greater than equal day
      const currentDate = dayjs();
      if (start <= currentDate) {
        const filename = this.getFilenameByNow();
        logger.info(`start record ${filename}`);
        logger.info(`duration ${this.saveDuration}`);
        this.save(filename);
        // unsave in next duration second, and stop connection
        // setTimeout(() => this.unsave(), this.saveDuration * 1000);
        setTimeout(() => this.unsave(), this.saveDuration * 1000);
      }
    };

    this.job = new CronJob({
      cronTime,
      onTick,
      start: true
    });
  }

  doVfSave() {
    // logger.info('vfSave ffmpeg -i .\40.mp4 -preset superfast -vf mpdecimate,setpts=N/FRAME_RATE/TB out.mp4');

    this.stopVfJob();

    const start = dayjs();
    const cronTime = `00 05 00 * * *`;

    const onVfTick = () => {
      logger.log('vf save run ', this.name);
      // 1. scan file mp4 not remove
      const saveFolder = path.join(MediaStream.rootFolder, this.name);
      const outFiles = this.findUnVfSaveFiles(saveFolder);
      outFiles.map(file => {
        const inputFile = path.join(saveFolder, file);
        if (fs.existsSync(inputFile)) {
          logger.info(`mp4 ${inputFile}`);
          try {
            // ..\..\bin\win32\ffmpeg.exe -f h264 -i 2020-02-26-17-03.h264 -filter:v fps=fps=40 40.mp4
            const outputFile = path.join(saveFolder, `vf_${file}`);
            const command = `${this.ffmpegPath} -i ${inputFile} -preset superfast -vf mpdecimate,setpts=N/FRAME_RATE/TB ${outputFile}`;
            logger.log(`command ${command}`);
            const message = execSync(command).toString();
            fs.unlinkSync(inputFile);
            logger.log(`unlink ${inputFile}`);
          } catch (err) {
            logger.error(err.toString());
          }
        }
      });
    };

    this.vfJob = new CronJob({
      cronTime,
      onVfTick,
      start: true
    });
  }

  setStreamPath(streamPath) {
    this.streamPath = streamPath;
  }

  convertFile(outFile, log = false) {
    try {
      // ..\..\bin\win32\ffmpeg.exe -f h264 -i 2020-02-26-17-03.h264 -filter:v fps=fps=40 40.mp4
      // const command = `${this.ffmpegPath} -y -hide_banner -f h264 -i ${outFile} -c copy -r ${MediaStream.fps} ${outFile.replace(
      const command = `${
        this.ffmpegPath
      } -y -hide_banner -f h264 -i ${outFile} -c copy ${outFile.replace(
        new RegExp(`${this.saveExtension}$`),
        'mp4'
      )}`;
      // const command = `${
      //   this.ffmpegPath
      //   } -y -hide_banner -f h264 -i ${outFile} -filter:v fps=fps=30 -preset ${
      //   MediaStream.ffmpegSpeed
      //   } ${outFile.replace(new RegExp(`${this.saveExtension}$`), 'mp4')}`;
      logger.log(`command ${command}`);
      const message = execSync(command).toString();
      // if (log)
      logger.log('convertFile', message);
    } catch (err) {
      logger.error(err.stderr.toString());
    }
  }

  async saveSplit(filename) {
    if (this.saving) return true;
    this.saving = true;

    // auto create filename
    if (!filename) {
      filename = this.getFilenameByNow();
      this.savingFile = filename;
    }

    try {
      // logger.info(`Codec is`, details.codec);
      // Step 3: Open the output file, by default should append
      if (!this.saveStream) {
        this.saveStream = new FileAdapterStream({ filename });
        const { cam } = this.connectedDevice;

        cam.pipeVideo(this.camStream, this.saveStream);
        logger.log('Recording: ', filename);
      }
    } catch (e) {
      logger.error(e);
      return false;
    }

    // return true;

    if (this.record && !this.saveAuto) {
      setTimeout(
        () => this.unsaveWithParam(filename),
        MediaStream.fileLength * 1000
      );
    }
    return true;
  }

  async save(filename) {
    if (this.saving) return true;
    this.saving = true;

    // auto create filename
    // if (!filename) {
    //   filename = this.getFilenameByNow();
    //   this.savingFile = filename;
    // }

    const duration = 1;

    try {
      // logger.info(`Codec is`, details.codec);
      // Step 3: Open the output file, by default should append
      if (!this.saveStream) {
        this.saveStream = new FileAdapterStream({
          // filename: path.join(__dirname, 'test.h264')
          folder: path.join(MediaStream.rootFolder, this.name),
          ffmpegPath: this.ffmpegPath,
          duration: MediaStream.fileLength // seconds
        });
        const { cam } = this.connectedDevice;
        if (!cam) throw new Error('cam stream not inited');
        cam.pipeVideo(this.camStream, this.saveStream);
        // logger.log('Recording: ');
        logger.log('Recording time (s): ', MediaStream.fileLength);
      }
    } catch (e) {
      logger.error(e);
      return false;
    }

    // if (this.record && !this.saveAuto) {
    //   setTimeout(
    //     () => this.unsave(), 60000
    //     // MediaStream.fileLength * 1000
    //   );
    // }

    return true;
  }

  async unSaveWithParam(outFile) {
    // continue unsave after abnormal exit, or even with cronjob

    logger.info('unsave with param');

    if (this.record && !this.saveAuto) {
      await this.save();
    }

    if (!this.savingFile) {
      this.saving = false;
      return true;
    }

    const { cam } = this.connectedDevice;
    // close client before unsave, because we know this file is not currently processing
    if (this.saveStream) {
      if (cam) cam.unpipeVideo(this.camStream, this.saveStream);

      await this.saveStream.destroy();
      delete this.saveStream;
    }

    if (fs.existsSync(outFile)) {
      // on close will remove all listener, close stream
      // and later add again with new stream on client
      this.convertFile(outFile);
      logger.info(`unlink ${outFile}`);
      fs.unlinkSync(outFile);
      // if (!normalUnlink) {
      //   // do by stream, so it is disconnected by now
      //   logger.log('Disconnected', this.rtspUrl);
      // }
    }

    // this.saving = false;
    return true;
  }

  async unsave(filename) {
    logger.info(`unsave ${this.record}`);

    // if (this.record && !this.saveAuto) {
    //   await this.save();
    // }

    // if (!filename) {
    //   filename = this.savingFile;
    // }

    // if (!this.savingFile) {
    //   this.saving = false;
    //   return true;
    // }

    const { cam } = this.connectedDevice;

    // close client before unsave, because we know this file is not currently processing
    if (this.saveStream) {
      if (cam) {
        cam.unpipeVideo(this.camStream, this.saveStream);
        logger.info('unpipe');
      }
      logger.info('destroy stream');
      this.saveStream.destroy();
      delete this.saveStream;
    }

    // if (fs.existsSync(filename)) {
    //   // on close will remove all listener, close stream
    //   // and later add again with new stream on client
    //   this.convertFile(filename);
    //   logger.info(`unlink ${filename}`);
    //   fs.unlinkSync(filename);
    //   // do by stream, so it is disconnected by now
    //   logger.log('Disconnected', filename);
    // }

    this.saving = false;

    // if (this.record && !this.saving && !this.saveAuto) {
    //   this.save();
    // }
    return true;
  }

  findUnfinishedSavingFiles(saveFolder) {
    if (fs.existsSync(saveFolder) && fs.lstatSync(saveFolder).isDirectory()) {
      return fs
        .readdirSync(saveFolder)
        .filter(file => file.endsWith(this.saveExtension))
        .map(file => path.join(saveFolder, file));
    }
    return [];
  }

  // eslint-disable-next-line class-methods-use-this
  findUnVfSaveFiles(saveFolder) {
    if (fs.existsSync(saveFolder) && fs.lstatSync(saveFolder).isDirectory()) {
      return fs
        .readdirSync(saveFolder)
        .filter(file => file.endsWith('mp4'))
        .filter(file => !file.startsWith('vf'))
        .map(file => file.toString());
    }
    return [];
  }

  resolveUnsave(saveFolder) {
    const outFiles = this.findUnfinishedSavingFiles(saveFolder);
    // logger.info(outFiles);

    return Promise.all(outFiles.map(outFile => this.unsave(outFile)));
  }

  // run only once and forever
  async startFlvStream() {
    if (!this.inputStreamStarted) return;
    // already started and not killed
    const { cam } = this.connectedDevice;
    if (!cam) return;

    // rebind for sure
    if (this.stream && this.stream.stdin.readyState !== 'closed') {
      cam.pipeVideo(this.camStream, this.stream.stdin);
      return;
    }

    // append only one

    // exits when parent exits
    const spawnOptions = [
      '-threads',
      1,
      '-hide_banner',
      '-loglevel',
      'panic',
      // maximum 512 probesize for cache
      // '-probesize',
      // probesize,
      '-fflags',
      'nobuffer',
      '-fflags',
      '+genpts',

      // hwaccess
      '-hwaccel',
      'auto',

      ...this.additionalFlags,
      '-i',
      'pipe:',

      // '-c:v',
      // 'libx264',
      // '-c:v',
      // 'libvpx-vp9',

      '-vcodec',
      'h264',
      '-acodec',
      'mp2',

      // '-c',
      // 'copy',
      '-an',
      '-preset',
      'faster', // 'ultrafast',
      '-tune',
      'zerolatency',

      // prefer opencl for faster endoding
      '-x264-params',
      'opencl=true',

      // '-movflags +faststart',

      '-flvflags',
      'no_duration_filesize',
      '-f',
      'flv',
      // for easy find process
      '-rtmp_conn',
      `S:${this.name}`,
      'pipe:1'
    ];

    logger.log(
      '[Media Stream ffmpeg inited]: ',
      this.name,
      this.connectedDevice.url
    );

    if (this.stream) {
      // re-destroy closed stream
      await this.destroyFlvStream();
    }

    this.stream = spawn(this.ffmpegPath, spawnOptions, {
      detached: false
      // in out err
      // stdio: ['pipe', 'pipe', 'ignore']
    });

    const errHandler = err => {
      logger.log('FFmpeg decode error', this.connectedDevice.url, err);
    };

    this.stream.stdin.on('error', errHandler);
    this.stream.stdout.on('error', errHandler);

    // pipe data
    this.firstBuffer = null;
    this.stream.stdout.on('data', this.handleData);

    cam.pipeVideo(this.camStream, this.stream.stdin);
  }

  handleData = buffer => {
    if (!this.firstBuffer) {
      this.firstBuffer = buffer;
      this.pipes.forEach(stream => {
        if (stream.destroyed) {
          this.pipes.delete(stream);
        } else {
          stream.write(this.firstBuffer);
        }
      });
    } else {
      this.pipes.forEach(stream => {
        if (stream.destroyed) {
          this.pipes.delete(stream);
        } else {
          // write header for stream the first time
          if (!stream.inited) {
            stream.inited = true;
            stream.write(this.firstBuffer);
          }
          stream.write(buffer);
        }
      });
    }
  };

  pipe(stream) {
    if (stream) this.pipes.add(stream);
  }

  unpipe(stream) {
    if (stream) this.pipes.delete(stream);
  }

  stopJob() {
    if (this.job) {
      logger.info('stop cronjob');
      this.job.stop();
      delete this.job;
    }
  }

  stopVfJob() {
    if (this.vfJob) {
      logger.info('stop vfJob cronjob');
      this.vfJob.stop();
      delete this.vfJob;
    }
  }

  async destroyFlvStream() {
    // destroy must clear reset to prevent re-create again
    // clearTimeout(this.resetStreamTimer);
    if (this.stream) {
      // unpipe so not write to close stream, then later re-create one
      const { cam } = this.connectedDevice;
      if (cam) cam.unpipeVideo(this.camStream, this.stream.stdin);
      logger.log(`[Video Stream Destroy] name: ${this.name}`);
      this.stream.stdin.removeAllListeners();
      this.stream.stdout.removeAllListeners();
      this.stream.stdin.destroy();
      this.stream.stdout.unpipe();
      // this.stream.removeAllListeners();

      this.stream.kill();
      this.stream = null;
    }
  }

  async stop() {
    if (this.stream) {
      // prevent trigger close on kill
      await this.destroyFlvStream();

      // un saving
      if (!this.record && this.saving) {
        this.unsave();
      }
    }
    this.inputStreamStarted = false;

    this.stopJob();

    return this;
  }
}

export const setFps = newFps => {
  if (newFps) {
    MediaStream.fps = newFps;
  }
};
