import fs from 'fs';
import path from 'path';
import util from 'util';
import dayjs from 'dayjs';
import logger from '../logger'

import { execSync } from 'child_process';

const { Writable, finished } = require('stream');
// const microtime = require('microtime'); //microtime@3.0.0
const finishedStream = util.promisify(finished);

export default class FileAdapterStream extends Writable {
  constructor({ folder, ffmpegPath, duration }) {
    super();
    logger.info(`folder ${folder}`);
    if (folder) {
      this.folder = folder;
      this.ffmpegPath = ffmpegPath;
      this.duration = duration; // add more 15s
      this.saving = true;
      this.saveStreamInd = 1;
      this.setFileTime();
      this.createStream();
    }
  }

  setFileTime() {
    this.filetime = new Date().getTime();
    this.nextFileTime = this.filetime + this.duration * 1000;
  }

  createStream() {
    this.filename = this.getFilenameByNow();
    logger.info(`file name ${this.filename}${new Date()}`);
    this.stream = fs.createWriteStream(this.filename, { flags: 'a' });
  }

  createNewStream() {
    this.newFilename = this.getFilenameByNow();
    logger.info(`new file name ${this.newFilename}${new Date()}`);
    this.newStream = fs.createWriteStream(this.newFilename, { flags: 'a' });
  }

  createWriteStream() {
    const start = new Date();
    if (this.saveStreamInd == 1) {
      this.createNewStream();
    } else {
      this.createStream();
    }
    const end = new Date();
    logger.info(`createWriteStream: ${end - start}`);
  }

  getFilenameByNow() {
    if (!fs.existsSync(this.folder)) {
      fs.mkdirSync(this.folder, { recursive: true });
    }

    return path.join(
      this.folder,
      `${dayjs().format('YYYY-MM-DD-HH-mm')}.${'h264'}`
    );
  }

  write(buffer) {
    const now = new Date().getTime();
    // if ((now - this.bufferTime) > 1000) {
    //   logger.info('greater than 1s');
    // }
    // this.bufferTime = new Date().getTime();

    if (this.saving) {
      let switchFlag = false;
      if ((now - this.filetime) / 1000 > this.duration) {
        // ghi file moi
        const start = new Date();
        logger.info(`switch file ${new Date()}`);
        if (this.saveStreamInd == 1) {
          // rename file
          setTimeout(() => {
            this.stopStream();
          }, 1000);
        } else if (this.saveStreamInd == 2) {
          // rename file
          setTimeout(() => {
            this.stopNewStream();
          }, 1000);
        }
        this.switchStream();
        this.writeBoth = false;
        switchFlag = true;
        this.setFileTime();
        const end = new Date();
        logger.info(`time process new  file ${end - start}`);
        // this.writeBuffer(buffer);
      }
      this.checkWriteBothStream();
      this.writeBuffer(buffer, switchFlag, this.writeBoth);
    } else {
      logger.info('not save this buffer');
    }
  }

  checkWriteBothStream() {
    if (this.writeBoth) {
      return;
    }
    const now = new Date().getTime();
    if (this.nextFileTime && this.nextFileTime - now < 15 * 1000) {
      this.writeBoth = true;
      this.createWriteStream();
    }
  }

  writeBuffer(buffer, switchFlag, writeBoth) {
    if (writeBoth) {
      this.stream.write(buffer);
      this.newStream.write(buffer);
    } else if (this.saveStreamInd == 1) {
      if (this.stream && this.stream.writable) {
        if (switchFlag) {
          logger.info(` write to file ${this.filename}${new Date()}`);
        }
        this.stream.write(buffer);
      }
    } else if (this.newStream && this.newStream.writable) {
      if (switchFlag) {
        logger.info(` write to file ${this.newFilename}${new Date()}`);
      }
      this.newStream.write(buffer);
    }
  }

  switchStream() {
    this.saveStreamInd = this.saveStreamInd === 1 ? 2 : 1;
  }

  stopStream() {
    if (!this.stream) return;
    this.stream.end();
    this.stream.on('finish', () => {
      this.stream = null;
      this.convertFile(this.filename);
    });
    // await finishedStream(this.stream);
    // this.saving = false;
  }

  stopNewStream() {
    if (!this.newStream) return;
    this.newStream.end();
    this.newStream.on('finish', () => {
      this.newStream = null;
      this.convertFile(this.newFilename);
    });
    // await finishedStream(this.newStream);
    // this.saving = false;
  }

  destroy() {
    clearTimeout(this.createtimeout);
    this.stopStream();
    this.stopNewStream();
    logger.info('Disconnected', this.filename);
  }

  convertFile(filename) {
    try {
      const command = `${this.ffmpegPath
        } -y -hide_banner -f h264 -i ${filename} -c copy ${filename.replace(
          new RegExp(`h264$`),
          'mp4'
        )}`;
      logger.info(`command ${command}`);
      const message = execSync(command).toString();
      logger.info(`unlink ${filename}`);
      fs.unlinkSync(filename);
    } catch (err) {
      logger.info(err);
    }
  }
}
