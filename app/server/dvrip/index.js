/* eslint-disable no-underscore-dangle */
import DVRIPStreamClient from './lib/dvripstreamclient';
import { StringURL } from '../utils';
import { MAIN_STREAM, SUB_STREAM } from '../constants/stream.json';

import logger from '../logger';

export default class IPCamClient {
  constructor(connectedDevice) {
    // super();
    this.connectedDevice = connectedDevice;
    this.main = {};
    this.extra = {};

    this.camIp = new StringURL(connectedDevice.url).hostname;
  }

  async connectStream(StreamType, protocol) {
    const _streamClient = {};

    _streamClient.stream = new DVRIPStreamClient({
      StreamType,
      camIp: this.camIp,
      protocol
    });

    try {
      // retry connect, maybe cam is off
      await _streamClient.stream.connect();
      await _streamClient.stream.login({
        Username: this.connectedDevice.user,
        Password: this.connectedDevice.pass
      });

      const streamResult = await _streamClient.stream.getVideoStream();
      Object.assign(_streamClient, streamResult);
    } catch (err) {
      // _streamClient.stream.close();
      // check if not in db then remove
      logger.log('IP cam connect err', this.connectedDevice.url, err);
    }
    // just return
    return _streamClient;
  }

  // dvrip always have 2 streams
  get IsConnected() {
    return this.isConnected(MAIN_STREAM) && this.isConnected(SUB_STREAM);
  }

  isConnected(StreamType) {
    const { stream } = this.getVideoStream(StreamType);
    return !!stream && stream.IsConnected;
  }

  async close() {
    if (this.extra.stream) await this.extra.stream.close();
    if (this.main.stream) await this.main.stream.close();
  }

  // connect only stream that is disconnect
  async connect(protocol = 'UDP') {
    // close all streams and remove all listeners
    await this.close();
    this.extra = await this.connectStream(SUB_STREAM, protocol);
    this.main = await this.connectStream(MAIN_STREAM, protocol);
  }

  getVideoStream(StreamType = MAIN_STREAM) {
    return StreamType === MAIN_STREAM ? this.main : this.extra;
  }
}
