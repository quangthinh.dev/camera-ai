module.exports = Object.freeze({
  IFrame: 0xfc,
  PFrame: 0xfd,
  PlusEnc: 0xf9, // Something related to H.264/5+. Not exactly sure.
  Audio: 0xfa
});
