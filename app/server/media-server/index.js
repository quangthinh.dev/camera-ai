/* eslint-disable no-return-await */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */
import http from 'http';
import WebSocket from 'ws';
import { getStreamName } from '../utils/common';
import MediaStream from '../media-stream';
import logger from '../logger';
import { MAIN_STREAM, SUB_STREAM } from '../constants/stream.json';
import { fixURL } from '../utils';
import ConnectedDevice from '../onvif/devices';
import db from '../database';


// let nms;
export const streamApp = 'live';
const streamAppBase = `/${streamApp}/`;
// these dicts map cam stream to dvr ip stream then to ffmpeg relay, and normal rtsp stream directly to ffmpeg relay
// const { validate } = new Validator({ allErrors: true });
const streamDict = new Map();
const connectedDevices = new Map();

export const getDevicesInfo = async () => {
  let item;
  const deviceKeys = await db.getItem('devices');
  if (Array.isArray(deviceKeys)) {
    // get all devices from leveldb
    const value = await Promise.all(
      deviceKeys.map(deviceKey => db.getItem(`devices.${deviceKey}`))
    );
    item = value.filter(device => device && device.url);
  }
  return item;
};

export const getStream = streamName => streamDict.get(streamName);
export const setStream = (streamName, videoStream) =>
  streamDict.set(streamName, videoStream);
export const getFlvStream = (url, stream) => {
  const streamName = getStreamName(url, stream);
  return `${streamAppBase}${streamName}.flv`;
};

export const getDevice = url => connectedDevices.get(fixURL(url));

export const removeStreamFromDevice = async connectedDevice => {
  const { stream, mainStream } = connectedDevice.profile;
  if (mainStream)
    await removeStream(getStreamName(connectedDevice.url, MAIN_STREAM));
  if (stream)
    await removeStream(getStreamName(connectedDevice.url, SUB_STREAM));
};

export const removeDevice = async (url, removeStream = true) => {
  const key = fixURL(url);
  const connectedDevice = connectedDevices.get(key);
  if (connectedDevice) {
    if (removeStream) await removeStreamFromDevice(connectedDevice);
    // removeCam(key);
    await connectedDevice.close();
    connectedDevices.delete(key);
  }
};

const shouldStopOnRetry = async connectedDevice => {
  const { url } = connectedDevice;
  const deviceKeys = await db.getItem('devices');
  const shouldStop = Array.isArray(deviceKeys) && !deviceKeys.includes(url);
  // logger.info(deviceKeys, shouldStop);
  // if (shouldStop) {
  //   logger.log('Remove device', url);
  //   removeDevice(url);
  // }
  return shouldStop;
};

export const addDevice = async ({
  url,
  user,
  pass,
  tcp = false,
  isIPCam = false,
  timeout = 15000
}) => {
  // return res.send({ message: 'Device is connected' });
  // try connect
  const key = fixURL(url);

  logger.info('add device');

  let connectedDevice = connectedDevices.get(key);
  const protocol = tcp ? 'TCP' : 'UDP';
  // protocol changed (tcp/udp or ipcam) will re-create device
  if (
    connectedDevice &&
    (protocol !== connectedDevice.protocol ||
      isIPCam !== connectedDevice.isIPCam)
  ) {
    logger.info('removing device', tcp, isIPCam, connectedDevice.url);
    removeDevice(connectedDevice.url);
    connectedDevice = null;
  }

  if (!connectedDevice) {
    connectedDevice = new ConnectedDevice({
      url: key,
      getFlvStream,
      protocol,
      isIPCam,
      shouldStopOnRetry
    });
    logger.info(`set url ${key}`);
    connectedDevices.set(key, connectedDevice);
  }

  // try init
  await connectedDevice.init({ user, pass, timeout });

  return connectedDevice;
};

export const addStream = async ({
  url,
  stream,
  connectedDevice,
  device,
  wss,
  ffmpegPath
}) => {
  // if (!nms) return;

  // must check connectdDevice
  connectedDevice = connectedDevice || getDevice(url);
  if (!connectedDevice) return;

  url = connectedDevice.url;

  device = device || (await db.getItem(`devices.${url}`));
  // logger.info(device, url, stream);

  const streamName = getStreamName(url, stream);

  let videoStream = streamDict.get(streamName);

  if (videoStream) {
    // try start after update
    await videoStream.startFlvStream();

    // do nothing if already set, otherwise can restore from database
    return videoStream.streamUrl;
  }

  const streamPath = `${streamAppBase}${streamName}`;

  const options = { connectedDevice, camStream: stream, wss };

  // update usernane/password for sure, 2 stream can not share same information from camera
  // only mainStream use camera options
  if (device) {
    Object.assign(options, device);
  }

  // overide with current params
  Object.assign(options, {
    name: streamName,
    streamUrl: `${streamPath}.flv`,
    ffmpegPath // : nms.ffmpeg
  });

  videoStream = await MediaStream.create(options);
  //  new MediaStream(options);

  streamDict.set(streamName, videoStream);
  return videoStream.streamUrl;
};

export const updateStream = async fps => {
  MediaStream.fps = fps;
  return 'ok';
};

export const updateVideoLength = async length => {
  MediaStream.fileLength = length * 60;
  logger.info(`MediaStream.fileLength ${MediaStream.fileLength}`);
  return 'ok';
};

export const updateSaveMotion = async only => {
  MediaStream.vfSave = only;
  [...streamDict].forEach(([streamName, videoStream]) => {
    logger.info(`process vf stream ${streamName}`);
    if (only) {
      videoStream.doVfSave();
    } else {
      videoStream.stopVfJob();
    }
  });
  return 'ok';
};

export const updateSaveFolder = async newFolder => {
  MediaStream.rootFolder = newFolder.toString();
  logger.info(`new folder ${MediaStream.rootFolder}`);
  await db.setItem('saveFolder', MediaStream.rootFolder, true);
  return 'ok';
};

export const restoreSaveFolder = async defaultFolder => {
  MediaStream.rootFolder =
    (await db.getItem('saveFolder', true)) || defaultFolder;
};

export const getSaveFolder = () => {
  return MediaStream.rootFolder;
};

export const getStreamPathURL = streamName => {
  return `media/${streamName}`;
};

export const removeStream = async streamName => {
  const videoStream = streamDict.get(streamName);
  if (videoStream) {
    await videoStream.stop();
    streamDict.delete(streamName);
  }
};

// help pipe websocket
WebSocket.prototype.write = function write(buffer) {
  if (this.readyState !== WebSocket.OPEN) return;
  return this.send(buffer);
};
WebSocket.prototype.end = WebSocket.prototype.close;

export const start = ({ app, host, port }) => {
  const httpServer = http.createServer(app);

  httpServer.listen(port, host, () => {
    logger.log(`Node Media Http Server started on port: ${port}`);
  });

  const wsServer = new WebSocket.Server({ server: httpServer });

  wsServer.on('connection', (ws, req) => {
    if (req.url.startsWith(streamAppBase)) {
      // remove suffix .flv with length = 4
      const streamName = req.url.slice(streamAppBase.length, -4);
      const videoStream = getStream(streamName);

      if (videoStream) {
        videoStream.pipe(ws);
        // when ws destroy, it will trigger close and unpipe
        ws.on('error', code => {
          logger.log('Socket error', code);
        });
        ws.on('close', () => {
          videoStream.unpipe(ws);
        });
      }
    }
  });

  wsServer.on('listening', () => {
    logger.log(`Node Media WebSocket Server started on port: ${port}`);
  });

  wsServer.on('error', e => {
    logger.error(`Node Media WebSocket Server ${e}`);
  });

  return { httpServer, wsServer };
};
