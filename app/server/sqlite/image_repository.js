class ImageReposity {
  constructor(db) {
    this.db = db;
  }

  createTable = () => {
    const sql = `
      CREATE TABLE IF NOT EXISTS images (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        filename TEXT,
        stream_name TEXT,
        url_stream TEXT,
        faces TEXT,
        created_at TEXT DEFAULT (datetime(CURRENT_TIMESTAMP, 'localtime')))`;
    return this.db.runquery(sql);
  };

  create = (imagePath, streamName, urlStream, faces) => {
    return this.db.runquery(
      `INSERT INTO images (filename, stream_name, url_stream, faces) VALUES (?, ?, ?, ?)`,
      [imagePath, streamName, urlStream, faces]
    );
  };

  getByDate = (streams, startTime, endTime) => {
    let url = `SELECT filename,stream_name,url_stream, created_at,faces FROM images`;
    if (startTime === '' && endTime === '' && streams.length === 0) {
      return [];
    }
    if (startTime !== '' && endTime !== '') {
      url += ' WHERE (created_at BETWEEN ? AND ?)';
    }
    if (streams.length !== 0) {
      if (startTime !== '' && endTime !== '') {
        url += ' AND stream_name IN (';
      } else {
        url += ' WHERE stream_name IN (';
      }

      streams.forEach((element, index) => {
        url += JSON.stringify(element);
        if (index !== streams.length - 1) {
          url += ',';
        }
        url += ')';
      });
    }
    // console.log('url', url);
    // url += ' ORDER BY created_at ASC';
    if (startTime !== '' && endTime !== '') {
      return this.db.all(url, [startTime, endTime]);
    }

    return this.db.all(url);
  };
}
module.exports = ImageReposity;
