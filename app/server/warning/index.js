let warningType = 'Email Only';

let warningPeriod = 15; // 15 phut gui canh bao 1 lan

let maxPeople = 20;

let running = false;

let scheduler = null;

export const runWarning = () => {
  if (!running) {
    running = true;
    console.log('Warning init');
    sendWarning();
  } else {
    console.log('Already run');
  }
};

export const sendWarning = () => {
  console.log(`waring type ${warningType}`);
  console.log(`waring warningPeriod ${warningPeriod}`);
  console.log(`waring maxPeople ${maxPeople}`);
  // doc database thong tin
  // gui canh bao
  scheduler = setTimeout(() => sendWarning(), warningPeriod * 60 * 1000);
};

export const setWarningType = newWarningType => {
  console.log(`change warning type ${newWarningType}`);
  warningType = newWarningType;
  rerunScheduler();
};

export const setWarningPeriod = newWarningPeriod => {
  console.log(`change warning period ${newWarningPeriod}`);
  warningPeriod = newWarningPeriod;
  rerunScheduler();
};

export const rerunScheduler = () => {
  if (scheduler != null) {
    clearTimeout(scheduler);
    scheduler = setTimeout(() => sendWarning(), warningPeriod * 60 * 1000);
  }
};

export const setMaxPeople = newMaxPeople => {
  console.log(`change max people ${newMaxPeople}`);
  maxPeople = newMaxPeople;
  rerunScheduler();
};
