/* eslint-disable react/jsx-props-no-spreading */
import { connect } from 'react-redux';
import React, { Component } from 'react';
import Settings from '../components/Settings';

import * as SettingsActions from '../actions/settings';

function mapStateToProps(state) {
  return {
    settings: state.settings,
    devices: state.devices
  };
}

type Props = {};

@connect(mapStateToProps, SettingsActions)
export default class SettingsPage extends Component<Props> {
  render() {
    return <Settings {...this.props} />;
  }
}
