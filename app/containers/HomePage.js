/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import formValueSelector from 'redux-form/es/formValueSelector';
import Home from '../components/Home';
import * as DevicesActions from '../actions/devices';

import { initialState as defaultSettings } from '../reducers/settings';

type Props = {};

const peopleSelector = formValueSelector('People'); // <-- same as form name
const warningSelector = formValueSelector('Warning'); // <-- same as form name

function mapStateToProps(state) {
  return {
    devices: state.devices,
    videoRatio: state.settings.videoRatio || defaultSettings.videoRatio,
    items: state.settings.items || defaultSettings.items,
    aiServer: state.settings.aiServer,
    confidence: state.settings.confidence || 30,
    people: peopleSelector(state, 'people'),
    warning: warningSelector(state, 'warning')
  };
}
@connect(mapStateToProps, DevicesActions)
export default class HomePage extends Component<Props> {
  props: Props;

  render() {
    return <Home {...this.props} />;
  }
}
