/* eslint-disable react/jsx-props-no-spreading */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import formValueSelector from 'redux-form/es/formValueSelector';

import Warning from '../components/Warning';

const selector = formValueSelector('People'); // <-- same as form name

type Props = {};

function mapStateToProps(state) {
  return {
    devices: state.devices,
    locale: state.settings.locale,
    people: selector(state, 'people')
  };
}

@connect(mapStateToProps)
export default class WarningPage extends Component<Props> {
  props: Props;

  render() {
    return <Warning {...this.props} />;
  }
}
