/* eslint-disable react/jsx-pascal-case */
/* eslint-disable import/prefer-default-export */

import React, { Suspense, lazy, Fragment } from 'react';
import { render } from 'react-dom';
import { makeStyles } from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import CachedIcon from '@material-ui/icons/Cached';
import { AppContainer as ReactHotAppContainer } from 'react-hot-loader';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';

// import initEmitter from './utils/faceapi/emitter';
// light modules
import { get, post } from './api';
import './app.global.css';

const AppContainer = process.env.PLAIN_HMR ? Fragment : ReactHotAppContainer;

const useStyles = makeStyles(theme => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1
  }
}));

type ReloadProps = {
  message: string
};
const Reload = ({ message }: ReloadProps) => {
  const classes = useStyles();
  return (
    <>
      <Typography color="error" variant="h4" align="center">
        {message}
      </Typography>
      <Backdrop className={classes.backdrop} open>
        <Button
          variant="outlined"
          color="secondary"
          size="large"
          onClick={() => window.location.reload()}
          startIcon={<CachedIcon />}
        >
          Reload
        </Button>
      </Backdrop>
    </>
  );
};

// should be seperated ?
const blacklist = { settings: ['license'] };

const inbound = async key => {
  // with devices we use special restore,
  // but for update should use action, it is better because we already know changed path
  const { item } = await get(
    key === 'devices' ? `/camera/getDevices` : `/db/getItem/${key}`
  );

  return item;
};

const outbound = (key, state) => {
  const item =
    key === 'devices' ? state.devices.map(device => device.url) : state[key];

  // delete blacklist items before save to database
  if (blacklist[key]) {
    // delete redundant key
    blacklist[key].forEach(blacklistKey => delete item[blacklistKey]);
  }

  return post('/db/saveItem', { key, item });
};

// Lazy loading app, with minimal import to show loading page first
const Root = lazy(async () => {
  let LazyComponent;
  try {
    const { configureStore } = await import('./store');
    const promises = [
      configureStore(inbound, outbound),
      // initEmitter(),
      import('./Root')
    ];
    if (
      process.env.TARGET !== 'web' &&
      process.env.DISABLED_LICENSE !== 'true'
    ) {
      promises.push(import('./components/LicenseChecker'));
    }

    // getting all
    const [store, RootComponent, ExtraComponent] = await Promise.all(promises);
    // return React.Node extra
    const extra = ExtraComponent ? <ExtraComponent.default /> : null;
    // window.emitter = emitter;
    // bind store to module then re-export
    LazyComponent = () => <RootComponent.default store={store} extra={extra} />;
  } catch (ex) {
    LazyComponent = Reload.bind(this, ex);
  }

  return { default: LazyComponent };
});

const AppContainerWrapper = () => {
  return (
    <AppContainer>
      <Suspense fallback={<LinearProgress size={24} thickness={4} />}>
        <Root />
      </Suspense>
    </AppContainer>
  );
};

render(<AppContainerWrapper />, document.getElementById('root'));
