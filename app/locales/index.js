// others but default language - en
import en from './langs/en.json';
import vi from './langs/vi.json';
import { flatten } from '../utils';
import tableEN from './table/en.json';
import tableVI from './table/vi.json';

export const messages = {
  en: flatten(en),
  vi: flatten(vi)
};

export const tableLocalization = {
  en: tableEN,
  vi: tableVI
};

export const locales = {
  en: 'English',
  vi: 'Tiếng Việt'
};
