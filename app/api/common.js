/* eslint-disable no-restricted-globals */

// get param direct from process.env, because of bundle param
export const SERVER_PORT = process.env.SERVER_PORT || 80;

// can use same hostname as web version
export const SERVER_HOST =
  process.env.SERVER_HOST || location.hostname || 'localhost';

export const { origin: SERVER_BASE } = new URL(
  `http://${SERVER_HOST}:${SERVER_PORT}`
);

export const API_BASE = `${SERVER_BASE}/api`;

export const WS_BASE = `ws://${SERVER_HOST}:${SERVER_PORT}`;

export const rejectErrors = res => {
  const { status } = res;
  if (status >= 200 && status < 300) {
    return res;
  }
  // we can get message from Promise but no need, just use statusText instead of
  // server return errors, also status code
  return Promise.reject(new Error(res.statusText));
};
