/* eslint-disable no-param-reassign */
/* eslint-disable react/jsx-props-no-spreading */
import React, {
  lazy,
  createContext,
  useContext,
  useState,
  useEffect
} from 'react';
import { useHistory } from 'react-router-dom';
import HomeIcon from '@material-ui/icons/Home';
import CameraIcon from '@material-ui/icons/Camera';
import PeopleIcon from '@material-ui/icons/People';
import HistoryIcon from '@material-ui/icons/History';
import SettingsIcon from '@material-ui/icons/Settings';
import ExtensionIcon from '@material-ui/icons/Extension';
import WarningIcon from '@material-ui/icons/Warning';

import uniqBy from 'lodash/uniqBy';
import { loadModule } from './utils/module';
import { abortRequests } from './api';

export type RouteProps = {
  key: string,
  path: string,
  component: React.Node,
  icon: React.Element<JSX.Element>
};

// customize routes, in the future can add more route like extension
const routes: RouteProps[] = [
  {
    key: 'location.home',
    path: '/',
    component: lazy(() => import('./containers/HomePage')),
    icon: <HomeIcon />
  }
];

if (process.env.TARGET !== 'web') {
  if (process.env.DISABLED_AI !== 'true') {
    routes.push({
      key: 'location.people',
      path: '/people',
      component: lazy(() => import('./containers/PeoplePage')),
      icon: <PeopleIcon />
    });
  }

  routes.push({
    key: 'location.warning',
    path: '/warning',
    component: lazy(() => import('./containers/WarningPage')),
    icon: <WarningIcon />
  });
  routes.push({
    key: 'location.setting',
    path: '/setting',
    component: lazy(() => import('./containers/SettingPage')),
    icon: <SettingsIcon />
  });
}

routes.push(
  ...[
    {
      key: 'location.camera',
      path: '/camera',
      component: lazy(() => import('./containers/CameraPage')),
      icon: <CameraIcon />
    },

    {
      key: 'location.history',
      path: '/history',
      component: lazy(() => import('./containers/HistoryPage')),
      icon: <HistoryIcon />
    },
    {
      key: 'location.extension',
      path: '/module',
      component: lazy(() => import('./containers/ModulePage')),
      icon: <ExtensionIcon />
    }
  ]
);

// monkey patching to prevent pushing same url into history stack
const fixHistory = history => {
  if (history.fixed) return;

  history.fixed = true;
  const prevHistoryPush = history.push;
  let lastPathname = history.location.pathname;

  history.push = (pathname, state = {}) => {
    if (pathname !== lastPathname) {
      lastPathname = pathname;
      prevHistoryPush(pathname, state);
      window.scrollTo(0, 0);
      abortRequests();
    }
  };
};

const RoutesContext = createContext();

export const useRoutes = () => {
  return useContext(RoutesContext);
};

type RoutesProviderProps = {
  children: ReactNode,
  value: RouteProps[]
};
const RoutesProvider = ({ children, value }: RoutesProviderProps) => {
  return (
    <RoutesContext.Provider value={value}>{children}</RoutesContext.Provider>
  );
};

type RoutesProps = {
  modules: string[]
} & RoutesProviderProps;

export default ({ modules, ...props }: RoutesProps) => {
  fixHistory(useHistory());
  // update modules
  const [value, setValue] = useState(routes);

  useEffect(() => {
    const updateRoutes = async () => {
      try {
        const moduleList = await Promise.all(modules.map(loadModule));
        const newRoutes = moduleList.map(m => m.route);
        const updatedRoutes = uniqBy([...routes, ...newRoutes], 'key');
        setValue(updatedRoutes);
      } catch (ex) {
        console.log(ex);
      }
    };
    updateRoutes();
  }, [modules]);

  return <RoutesProvider {...props} value={value} />;
};
