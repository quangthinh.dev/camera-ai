/* eslint-disable react/jsx-props-no-spreading */
import React, { Suspense } from 'react';
import { Provider, connect } from 'react-redux';
import { Switch, Route } from 'react-router';
import { BrowserRouter, HashRouter } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import DayJSUtils from '@date-io/dayjs';
import dayjs from 'dayjs';
import 'dayjs/locale/vi';
import { hot } from 'react-hot-loader/root';
import { LocalizationProvider } from '@material-ui/pickers';
import LinearProgress from '@material-ui/core/LinearProgress';
import MainDrawer from './components/MainDrawer';
import Routes, { useRoutes } from './Routes';
import { initialState as SettingState } from './reducers/settings';
import type { Store } from './reducers/types';
import * as SettingsActions from './actions/settings';
import { messages } from './locales';

const ConnectedIntlProvider = connect(state => {
  let {
    settings: { locale }
  } = state;
  if (typeof locale !== 'string') locale = 'vi';
  return { locale, key: locale, messages: messages[locale] };
})(IntlProvider);

type WrapThemeProviderProps = {
  theme: string,
  locale: string,
  children: ReactNode
};
const WrapThemeProvider = ({
  theme,
  locale,
  children
}: WrapThemeProviderProps) => {
  return (
    <LocalizationProvider
      dateLibInstance={dayjs}
      dateAdapter={DayJSUtils}
      locale={locale}
    >
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </LocalizationProvider>
  );
};

const ConnectedThemeProvider = connect(state => {
  const type = state.settings.theme || SettingState.theme;
  const theme = createMuiTheme({
    palette: {
      type
    }
  });
  // update theme
  return { theme, locale: state.settings.locale };
})(WrapThemeProvider);

const Router = process.env.TARGET === 'web' ? BrowserRouter : HashRouter;

const ConnectedMain = connect(
  state => ({
    open: state.settings.drawerOpen
  }),
  SettingsActions
)(({ open, setItem }) => {
  const routes = useRoutes();
  const handleDrawerOpen = value => {
    setItem('drawerOpen', value);
  };
  return (
    <MainDrawer routes={routes} open={open} onOpen={handleDrawerOpen}>
      <Suspense fallback={<LinearProgress size={24} thickness={4} />}>
        <Switch>
          {routes.map(route => (
            <Route exact {...route} />
          ))}
        </Switch>
      </Suspense>
    </MainDrawer>
  );
});

// system or load modules?
const defaultModules = [];
const ConnectedRoutes = connect(state => ({
  modules: state.settings.modules || defaultModules
}))(Routes);

type RootProps = {
  store: Store,
  extra: React.Node
};

const Root = ({ store, extra }: RootProps) => (
  <Provider store={store}>
    <ConnectedIntlProvider>
      <ConnectedThemeProvider>
        {extra}
        <Router>
          <ConnectedRoutes>
            <ConnectedMain />
          </ConnectedRoutes>
        </Router>
      </ConnectedThemeProvider>
    </ConnectedIntlProvider>
  </Provider>
);

export default hot(Root);
