/* eslint-disable compat/compat */
/* eslint-disable no-return-assign */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-unused-vars */
/* eslint-disable no-alert */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/media-has-caption */
import React, { useState, useRef, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import VideoLabelIcon from '@material-ui/icons/VideoLabel';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Box from '@material-ui/core/Box';
import Input from '@material-ui/core/Input';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import dayjs from 'dayjs';
import {
  DateRangePicker,
  DateRangeDelimiter,
  DateRange
} from '@material-ui/pickers';
import { useIntl } from 'react-intl';
import { CanvasWithPeople, PeopleCount } from './common';
import styles from './History.css';
import { getSnapshot, copyVideo2Canvas, getShortTime } from '../utils';
import { post, get, fetchWithOptions, API_BASE } from '../api';
// import {
//   assignDetectionTask,
//   addDetectionListener,
//   removeDetectionListener
// } from '../utils/faceapi/emitter';

const Playlist = ({ playlist, handleItemClick, isVideo, currentURL }) => {
  const { formatMessage } = useIntl();

  const getAvatar = item => {
    if (isVideo)
      return (
        <Avatar>
          <VideoLabelIcon />
        </Avatar>
      );

    return <Avatar variant="square" src={item.videoURL} />;
  };
  window.dayjs = dayjs;
  const getSecondary = text => {
    if (isVideo) return text;

    return dayjs(+text).format(formatMessage({ id: 'format.fullDate' }));
  };
  return (
    <Box className={styles.playlist}>
      <List>
        {playlist.map((item, index) => (
          <ListItem
            selected={item.videoURL === currentURL}
            button
            key={index}
            onClick={() => handleItemClick && handleItemClick(item)}
          >
            <ListItemAvatar>{getAvatar(item)}</ListItemAvatar>
            <ListItemText
              primary={item.name}
              secondary={getSecondary(item.fileName)}
            />
          </ListItem>
        ))}
      </List>
    </Box>
  );
};

const PeopleDrawResult = ({ src, confidence }) => {
  const [people, setResult] = useState();
  const peopleCountRef = useRef();
  const drawPeople = async () => {
    const result = await get(src.replace(/\.([^.]+)$/, '.json'));
    console.log('result: ', src.replace(/\.([^.]+)$/, '.json'));
    const { facesResult, peopleCount } = result || {};
    setResult(facesResult);
    if (peopleCount && peopleCountRef.current) {
      const { classes, scores } = peopleCount;
      const count = classes.filter((c, i) => c === 0 && scores[i] >= 0.7)
        .length;
      peopleCountRef.current.setState({ count });
    }
  };
  useEffect(() => {
    drawPeople();
  }, [src]);

  // when src changed, people changed, and cause re-draw
  return (
    <Box position="relative">
      <PeopleCount
        ref={peopleCountRef}
        style={{ zoom: 3 }}
        right={10}
        top={10}
      />
      <CanvasWithPeople
        className={styles.preview}
        minConfidence={confidence}
        src={src}
        people={people}
      />
    </Box>
  );
};

const startDayjs = dayjs('0 00:00:00');
class PeopleSearchResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = { result: {} };
  }

  addItem(item) {
    const { result } = this.state;
    this.setState({
      result: {
        ...result,
        ...item
      }
    });
  }

  clear() {
    this.setState({ result: {} });
  }

  render() {
    const { onClick } = this.props;
    const { result } = this.state;

    return (
      <Box my={2}>
        {Object.entries(result).map(([time, labels]) => (
          <Chip
            key={time}
            label={`${getShortTime(time)} (${labels.join(', ')})`}
            variant="outlined"
            onClick={() => onClick(time)}
          />
        ))}
      </Box>
    );
  }
}

const filterPlaylist = (playlist, start, end) => {
  if (!start || !end) return playlist;
  const startTime = start.unix() * 1000;
  const endTime = end.unix() * 1000;
  return playlist
    .map(item => ({ ...item, currentTime: +item.fileName }))
    .filter(
      item => item.currentTime >= startTime && item.currentTime <= endTime
    )
    .sort((a, b) => b.currentTime - a.currentTime);
};

type HistoryProps = {
  devices: any[]
};
// prevent changing defaultValue when reload
const defaultValue = [];
export default function History({
  devices,
  people,
  aiServer,
  confidence
}: HistoryProps) {
  const [playlist, setPlaylist] = useState([]);
  const [videoURL, setVideoURL] = useState('');
  const [isVideo, setIsVideo] = useState(true);
  const [peopleList, setPeopleList] = useState(defaultValue);
  const [selectedDate, handleDateChange] = React.useState([null, null]);
  const [selectedCameras, handleChangeCamera] = useState([]);

  const blobURLMap = new Map();

  const searchResultElement = useRef();
  const rateElement = useRef();
  const videoElement = useRef();
  const { formatMessage } = useIntl();
  const handleChange = async (e, value) => {
    const items = value.map(item => ({
      name: item.name,
      url: item.url
    }));
    handleChangeCamera(items);
    const ext = isVideo ? '.mp4' : '.jpg';
    let bodyObject = {};
    if (selectedDate[0] && selectedDate[1] && !isVideo) {
      const startTime = selectedDate[0].format('YYYY-MM-DD HH:MM:ss');
      const endTime = selectedDate[1].format('YYYY-MM-DD HH:MM:ss');
      let faces = '';
      if (peopleList && peopleList.length > 0) {
        faces = JSON.stringify(peopleList);
      }

      bodyObject = { items, ext, startTime, endTime, faces };
    } else {
      bodyObject = { items, ext };
    }
    const ret = await post('/camera/history', bodyObject);
    const data = ret.map(item => ({
      ...item,
      videoURL: `${API_BASE}/camera/${item.streamURL}`
    }));
    setPlaylist(data);
    setVideoURL('');
  };

  const clearMap = () => {
    [...blobURLMap.keys()].forEach(URL.revokeObjectURL);
    blobURLMap.clear();
  };

  const onDetected = data => {
    if (!Array.isArray(data)) return;
    const result = {};
    data.forEach(({ blobURL, label }) => {
      const currentTime = blobURLMap.get(blobURL);
      if (!result[currentTime]) result[currentTime] = [];
      if (peopleList.includes(label)) result[currentTime].push(label);
    });

    searchResultElement.current.addItem(result);
  };

  const handleChangeDateRange = async value => {
    handleDateChange(value);
    if (value[0] && value[1]) {
      const startTime = value[0].format('YYYY-MM-DD HH:MM:ss');
      const endTime = value[1].format('YYYY-MM-DD HH:MM:ss');
      const ext = isVideo ? '.mp4' : '.jpg';
      let faces = '';
      if (peopleList && peopleList.length > 0) {
        faces = JSON.stringify(peopleList);
      }

      const ret = await post('/camera/history', {
        items: selectedCameras,
        ext,
        startTime,
        endTime,
        faces
      });
      const data = ret.map(item => ({
        ...item,
        videoURL: `${API_BASE}/camera/${item.streamURL}`
      }));
      setPlaylist(data);
      setVideoURL('');
    }
  };
  const handleChangePeople = async value => {
    setPeopleList(value);
    if (!isVideo) {
      let faces = '';
      let startTime = '';
      let endTime = '';
      const ext = isVideo ? '.mp4' : '.jpg';

      if (selectedDate[0] && selectedDate[1]) {
        startTime = selectedDate[0].format('YYYY-MM-DD HH:MM:ss');
        endTime = selectedDate[1].format('YYYY-MM-DD HH:MM:ss');
      }
      if (value && value.length > 0) {
        faces = JSON.stringify(value);
      }

      const ret = await post('/camera/history', {
        items: selectedCameras,
        ext,
        startTime,
        endTime,
        faces
      });
      const data = ret.map(item => ({
        ...item,
        videoURL: `${API_BASE}/camera/${item.streamURL}`
      }));
      setPlaylist(data);
      setVideoURL('');
    }
  };

  // every time reload
  // useEffect(() => {
  //   const onDetected = data => {
  //     if (!Array.isArray(data)) return;
  //     const result = {};
  //     data.forEach(({ blobURL, label }) => {
  //       const currentTime = blobURLMap.get(blobURL);
  //       if (!result[currentTime]) result[currentTime] = [];
  //       if (peopleList.includes(label)) result[currentTime].push(label);
  //     });

  //     searchResultElement.current.addItem(result);
  //   };

  //   // addDetectionListener('detectImage.history', onDetected);
  //   // return () => {
  //   //   removeDetectionListener('detectImage.history', onDetected);
  //   //   clearMap();
  //   // };
  // });

  const handleItemClick = item => {
    // console.log(item.streamURL);
    setVideoURL(`${API_BASE}/camera/${item.streamURL}`);
  };

  const selectFileToOpen = event => {
    if (event.target.files[0]) {
      const filePath = event.target.files[0].path;
      if (filePath) {
        if (searchResultElement.current) searchResultElement.current.clear();
        setVideoURL(event.target.files[0].path);
      }
    }
  };

  const canvas = document.createElement('canvas');
  let searching = false;
  const goToTime = time => {
    if (searching) return;
    const video = videoElement.current;
    if (!video) return;
    video.currentTime = time;
  };

  const searchPeople = () => {
    const video = videoElement.current;
    if (!video) return;

    if (searching) {
      alert('Searching');
      return;
    }

    searchResultElement.current.clear();
    clearMap();
    searching = true;

    video.onended = () => {
      clearInterval(runner);
      searching = false;
      video.playbackRate = 1;
    };

    video.playbackRate = +rateElement.current.value || 10;
    video.currentTime = 0;
    video.play();

    // running
    const runner = setInterval(async () => {
      copyVideo2Canvas(video, canvas);
      const snapshot = await getSnapshot(canvas);
      const blobURL = URL.createObjectURL(snapshot);
      blobURLMap.set(blobURL, video.currentTime);

      //
      const formData = new FormData();
      formData.append('file', snapshot);
      const controller = new AbortController();
      const { signal } = controller;
      setTimeout(() => controller.abort(), 10000);
      const ret = await fetchWithOptions(`${aiServer}/api/search_face`, {
        method: 'POST',
        body: formData,
        signal
      });

      if (ret && ret.length) {
        const data = ret.map(item => ({ blobURL, label: item.name }));
        onDetected(data);
      }

      // assignDetectionTask('detectImage', canvas, blobURL, 'history');
    }, 500);
  };

  const peopleOptions = Array.isArray(people)
    ? people.map(person => person.label)
    : [];

  return (
    <Box className={styles.root}>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Autocomplete
            multiple
            defaultValue={defaultValue}
            options={devices.map((d, idx) => ({
              idx,
              ...d
            }))}
            onChange={handleChange}
            getOptionLabel={device => device.name || `Noname#${device.idx}`}
            renderInput={params => (
              <TextField
                {...params}
                variant="standard"
                placeholder={formatMessage({ id: 'app.chooseCam' })}
                fullWidth
              />
            )}
          />
          {isVideo ? (
            <Input fullWidth type="file" onChange={selectFileToOpen} />
          ) : (
            <DateRangePicker
              calendars={2}
              mask="__-__-____ __:__:__"
              inputFormat={formatMessage({ id: 'format.fullDate' })}
              value={selectedDate}
              startText={formatMessage({ id: 'app.startTime' })}
              endText={formatMessage({ id: 'app.endTime' })}
              onChange={handleChangeDateRange}
              renderInput={(startProps, endProps) => (
                <>
                  <TextField {...startProps} variant="standard" size="small" />
                  <DateRangeDelimiter> - </DateRangeDelimiter>
                  <TextField {...endProps} variant="standard" size="small" />
                </>
              )}
            />
          )}
        </Grid>

        <Grid item xs={6}>
          <Autocomplete
            onChange={(e, val) => {
              handleChangePeople(val);
            }}
            multiple
            defaultValue={defaultValue}
            options={peopleOptions}
            renderInput={params => {
              return (
                <TextField
                  {...params}
                  variant="standard"
                  placeholder={formatMessage({ id: 'app.choosePeople' })}
                  fullWidth
                />
              );
            }}
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={isVideo}
                onChange={event => setIsVideo(event.target.checked)}
                disableRipple
              />
            }
            label={formatMessage({ id: 'app.video' })}
          />
        </Grid>
      </Grid>
      <Grid container spacing={1} style={{ height: '100%' }}>
        <Grid item xs={4} style={{ height: '100%' }}>
          <Playlist
            currentURL={videoURL}
            isVideo={isVideo}
            playlist={filterPlaylist(playlist, ...selectedDate)}
            handleItemClick={handleItemClick}
          />
        </Grid>
        <Grid item xs={8} style={{ position: 'relative' }}>
          <div className={styles.videoContainer}>
            {videoURL &&
              (isVideo ? (
                <Box my={2} style={{ position: 'relative', height: '100%', width: '100%' }}>
                  <video
                    ref={videoElement}
                    src={videoURL}
                    width="100%"
                    controls
                    autoPlay
                    style={{ height: 'calc(100% - 180px)'}}
                  >
                    <source src={videoURL} type="video/mp4" />
                  </video>
                  <TextField
                    label="Playback Rate"
                    defaultValue="10"
                    inputRef={rateElement}
                    type="number"
                    InputProps={{
                      endAdornment: (
                        <Button
                          fullWidth
                          variant="text"
                          size="small"
                          onClick={searchPeople}
                          endIcon={<SearchIcon />}
                        >
                          {formatMessage({ id: 'app.search' })}
                        </Button>
                      )
                    }}
                  />
                  <PeopleSearchResult
                    style={{ position: 'relative', height: '100%', width: '100%' }}>
                    onClick={goToTime}
                    ref={searchResultElement} /></PeopleSearchResult>
                </Box>
              ) : (
                <PeopleDrawResult src={videoURL} confidence={confidence} />
              ))}
            </div>
        </Grid>
      </Grid>
    </Box>
  );
}
