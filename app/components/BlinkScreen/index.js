import React, { Component } from 'react';
import Chip from '@material-ui/core/Chip';
import Box from '@material-ui/core/Box';
import Error from '@material-ui/icons/Error';

export default class BlinkScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blink: false
    };
  }

  hideBlink = () => {
    this.setState({ blink: false }, () => {
      if (this.interval) {
        clearInterval(this.interval);
      }
    });
  };

  showBlink = () => {
    this.setState({ blink: true });
  };

  starterBlink = () => {
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.interval = setInterval(() => {
      const { blink } = this.state;
      this.setState({ blink: !blink });
    }, 1000);
  };

  render() {
    const { variant, ...props } = this.props;
    const { blink } = this.state;
    if (!blink) {
      return null;
    }
    return (
      <Box position="absolute" {...props}>
        <Chip
          icon={<Error />}
          label=""
          size="small"
          variant="default"
          color="secondary"
        />
      </Box>
    );
  }
}
