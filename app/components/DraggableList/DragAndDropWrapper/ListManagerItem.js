import React, { ReactElement } from 'react';
import { Draggable, DraggableProvided } from 'react-beautiful-dnd';

export type ListManagerItemProps = {
  item: any,
  index: number,
  draggableId: string,
  renderProps: any,
  render(item: any): ReactElement<{}>
};

const ListManagerItem: React.StatelessComponent<ListManagerItemProps> = ({
  item,
  index,
  render,
  renderProps,
  draggableId
}: ListManagerItemProps) => (
  <Draggable
    draggableId={draggableId}
    disableInteractiveElementBlocking
    index={index}
  >
    {(provided: DraggableProvided) =>
      render(
        {
          item,
          ...provided.draggableProps,
          ...provided.dragHandleProps,
          ...renderProps
        },
        provided.innerRef
      )
    }
  </Draggable>
);

export default ListManagerItem;
