/* eslint-disable react/destructuring-assignment */
import React from 'react';
import {
  splitItems,
  computeOriginalIndex,
  computeOriginalIndexAfterDrop
} from './compute';
import type { DragAndDropResult, Chunk, Omit } from '../types';

export type Props = {
  chunks: Chunk[],
  onDragEnd(result: DragAndDropResult): void
};

const withMaxItems = (Component: React.Component<Props>) => {
  return class ComponentWithMaxItems extends React.Component<
    Omit<P, Props> & WithMaxItemsProps,
    WithMaxItemsState
  > {
    constructor(props: Omit<P, Props> & WithMaxItemsProps) {
      super(props);
      const maxItems: number =
        props.maxItems && props.maxItems > 0
          ? props.maxItems
          : props.items.length;
      this.config = {
        maxItems,
        items: [...props.items]
      };

      this.state = {
        chunks: splitItems(maxItems, this.config.items)
      };
    }

    findChunkIndex = (id: string): number => {
      return this.state.chunks.findIndex((chunk: Chunk) => chunk.id === id);
    };

    onDragEnd = ({ source, destination }: DragAndDropResult): void => {
      if (destination) {
        const { getKey, onDragEnd } = this.props;
        const { index: indexInSourceChunk, id: sourceChunkId } = source;
        const {
          index: indexInDestinationChunk,
          id: destinationChunkId
        } = destination;
        const sourceChunkIndex: number = this.findChunkIndex(sourceChunkId);
        const destinationChunkIndex: number = this.findChunkIndex(
          destinationChunkId
        );
        const { maxItems, items } = this.config;
        const sourceIndex: number = computeOriginalIndex(
          maxItems,
          sourceChunkIndex,
          indexInSourceChunk
        );
        const destinationIndex: number = computeOriginalIndexAfterDrop(
          maxItems,
          sourceChunkIndex,
          destinationChunkIndex,
          indexInDestinationChunk
        );

        // remove item from source, add item to dest

        const [removed] = items.splice(sourceIndex, 1);
        items.splice(destinationIndex, 0, removed);

        const chunks = splitItems(maxItems, items);

        // re-render
        this.setState({ chunks });

        onDragEnd({
          source: { index: sourceIndex, id: getKey(items[sourceIndex]) },
          destination: {
            index: destinationIndex,
            id: getKey(items[destinationIndex])
          }
        });
      }
    };

    render() {
      const { items, maxItems, onDragEnd, ...rest } = this.props;
      return (
        <Component
          chunks={this.state.chunks}
          onDragEnd={this.onDragEnd}
          {...rest}
        />
      );
    }
  };
};

export default withMaxItems;
