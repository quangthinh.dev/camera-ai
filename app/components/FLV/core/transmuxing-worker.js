/*
 * Copyright (C) 2016 Bilibili. All Rights Reserved.
 *
 * @author zheng qian <xqq@xqq.im>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// import Log from '../utils/logger';
import LoggingControl from '../utils/logging-control';
import TransmuxingController from './transmuxing-controller';
import TransmuxingEvents from './transmuxing-events';
import Polyfill from '../utils/polyfill.js';

/* post message to worker:
   data: {
       cmd: string
       param: any
   }

   receive message from worker:
   data: {
       msg: string,
       data: any
   }
 */

// const TAG = 'TransmuxingWorker';

export default function TransmuxingWorker(self) {
  let controller = null;
  const logcatListener = onLogcatCallback.bind(this);

  Polyfill.install();

  self.addEventListener('message', function(e) {
    switch (e.data.cmd) {
      case 'init':
        controller = new TransmuxingController(
          e.data.param[0],
          e.data.param[1]
        );
        controller.on(TransmuxingEvents.IO_ERROR, onIOError.bind(this));
        controller.on(TransmuxingEvents.DEMUX_ERROR, onDemuxError.bind(this));
        controller.on(TransmuxingEvents.INIT_SEGMENT, onInitSegment.bind(this));
        controller.on(
          TransmuxingEvents.MEDIA_SEGMENT,
          onMediaSegment.bind(this)
        );
        controller.on(
          TransmuxingEvents.LOADING_COMPLETE,
          onLoadingComplete.bind(this)
        );
        controller.on(
          TransmuxingEvents.RECOVERED_EARLY_EOF,
          onRecoveredEarlyEof.bind(this)
        );
        controller.on(TransmuxingEvents.MEDIA_INFO, onMediaInfo.bind(this));
        controller.on(
          TransmuxingEvents.METADATA_ARRIVED,
          onMetaDataArrived.bind(this)
        );
        controller.on(
          TransmuxingEvents.SCRIPTDATA_ARRIVED,
          onScriptDataArrived.bind(this)
        );
        controller.on(
          TransmuxingEvents.STATISTICS_INFO,
          onStatisticsInfo.bind(this)
        );
        controller.on(
          TransmuxingEvents.RECOMMEND_SEEKPOINT,
          onRecommendSeekpoint.bind(this)
        );
        break;
      case 'destroy':
        if (controller) {
          controller.destroy();
          controller = null;
        }
        self.postMessage({ msg: 'destroyed' });
        break;
      case 'start':
        controller.start();
        break;
      case 'stop':
        controller.stop();
        break;
      case 'seek':
        controller.seek(e.data.param);
        break;
      case 'pause':
        controller.pause();
        break;
      case 'resume':
        controller.resume();
        break;
      case 'logging_config': {
        const config = e.data.param;
        LoggingControl.applyConfig(config);

        if (config.enableCallback === true) {
          LoggingControl.addLogListener(logcatListener);
        } else {
          LoggingControl.removeLogListener(logcatListener);
        }
        break;
      }
      default:
        break;
    }
  });

  function onInitSegment(type, initSegment) {
    const obj = {
      msg: TransmuxingEvents.INIT_SEGMENT,
      data: {
        type,
        data: initSegment
      }
    };
    self.postMessage(obj, [initSegment.data]); // data: ArrayBuffer
  }

  function onMediaSegment(type, mediaSegment) {
    const obj = {
      msg: TransmuxingEvents.MEDIA_SEGMENT,
      data: {
        type,
        data: mediaSegment
      }
    };
    self.postMessage(obj, [mediaSegment.data]); // data: ArrayBuffer
  }

  function onLoadingComplete() {
    const obj = {
      msg: TransmuxingEvents.LOADING_COMPLETE
    };
    self.postMessage(obj);
  }

  function onRecoveredEarlyEof() {
    const obj = {
      msg: TransmuxingEvents.RECOVERED_EARLY_EOF
    };
    self.postMessage(obj);
  }

  function onMediaInfo(mediaInfo) {
    const obj = {
      msg: TransmuxingEvents.MEDIA_INFO,
      data: mediaInfo
    };
    self.postMessage(obj);
  }

  function onMetaDataArrived(metadata) {
    const obj = {
      msg: TransmuxingEvents.METADATA_ARRIVED,
      data: metadata
    };
    self.postMessage(obj);
  }

  function onScriptDataArrived(data) {
    const obj = {
      msg: TransmuxingEvents.SCRIPTDATA_ARRIVED,
      data
    };
    self.postMessage(obj);
  }

  function onStatisticsInfo(statInfo) {
    const obj = {
      msg: TransmuxingEvents.STATISTICS_INFO,
      data: statInfo
    };
    self.postMessage(obj);
  }

  function onIOError(type, info) {
    self.postMessage({
      msg: TransmuxingEvents.IO_ERROR,
      data: {
        type,
        info
      }
    });
  }

  function onDemuxError(type, info) {
    self.postMessage({
      msg: TransmuxingEvents.DEMUX_ERROR,
      data: {
        type,
        info
      }
    });
  }

  function onRecommendSeekpoint(milliseconds) {
    self.postMessage({
      msg: TransmuxingEvents.RECOMMEND_SEEKPOINT,
      data: milliseconds
    });
  }

  function onLogcatCallback(type, str) {
    self.postMessage({
      msg: 'logcat_callback',
      data: {
        type,
        logcat: str
      }
    });
  }
}
