/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable promise/always-return */
/* eslint-disable promise/catch-or-return */
/* eslint-disable compat/compat */
/* eslint-disable no-else-return */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable max-classes-per-file */

import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
// import Dialog from '@material-ui/core/Dialog';
// import IconButton from '@material-ui/core/IconButton';
// import CloseIcon from '@material-ui/icons/Close';
// import Slide from '@material-ui/core/Slide';

import uniqBy from 'lodash/uniqBy';
import type { DragAndDropResult } from './DraggableList/types';
import DraggableCameraList from './DraggableCameraList';
import styles from './Home.css';
import PreviewDialog from './PreviewDialog';
// import { CanvasWithPeople } from './common';

// class PreviewDialog extends Component {
//   constructor(props) {
//     super(props);
//     this.state = { item: null };
//   }

//   handleClose = () => {
//     this.setState({ item: null });
//   };

//   show(item) {
//     this.setState({ item });
//   }

//   render() {
//     const { item } = this.state;
//     return (
//       <Dialog
//         scroll="paper"
//         fullScreen
//         open={!!item}
//         onClose={this.handleClose}
//         TransitionComponent={Transition}
//       >
//         <IconButton
//           edge="start"
//           classes={{ root: styles.closeButton }}
//           onClick={this.handleClose}
//           aria-label="close"
//         >
//           <CloseIcon />
//         </IconButton>
//         {item && (
//           <CanvasWithPeople
//             src={item.blobURL}
//             people={[item]}
//             className={styles.dialogImage}
//           />
//         )}
//       </Dialog>
//     );
//   }
// }

const AvatarPopover = ({
  label,
  avatar,
  confidence,
  onClick,
  rect,
  blobURL,
  id
}) => {
  const name =
    label && label.match(/unknown/i) ? id : `${label} (${confidence}%)`;
  const { left, top, width, height, imageWidth, imageHeight } = rect;
  const scaleX = 80 / width;
  const scaleY = 80 / height;
  const bgStyle = {
    backgroundRepeat: 'no-repeat',
    backgroundPosition: `${-Math.round(scaleX * left)}px ${-Math.round(
      scaleY * top
    )}px`,
    backgroundSize: `${Math.round(scaleX * imageWidth)}px ${Math.round(
      scaleY * imageHeight
    )}px`,
    backgroundImage: `url(${blobURL})`
  };

  return (
    <ListItem
      dense
      disableGutters
      button
      onClick={onClick}
      classes={{ root: styles.listItem }}
    >
      <div style={bgStyle} className={styles.bigAvatar} />

      {avatar && (
        <Avatar variant="square" className={styles.bigAvatar} src={avatar} />
      )}
      {/* {id && <ListItemText primary={id} className={styles.listItemText} />} */}

      {label && <ListItemText primary={name} className={styles.listItemText} />}
    </ListItem>
  );
};

class ListPeople extends Component {
  static defaultProps = {
    maxItems: 6
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [
        // {
        //   label: 'unknown',
        //   blobURL:
        //     'blob:http://bachors.com/ec42a658-5ffb-41cc-90e3-ac3a37cb6830',
        //   id: 'unknown-1',
        //   rect: {
        //     left: 0,
        //     top: 0,
        //     width: 100,
        //     height: 100
        //   },
        //   time: 1
        // }
      ]
    };
  }

  add(data) {
    // after this page is clean, blob is revoke automatically, but we also need to revoke as soon as possible
    data = uniqBy([...data, ...this.state.data], 'time');
    if (data.length > this.props.maxItems) {
      // clean hidden blobURL
      const removeItems = data.slice(this.props.maxItems);
      data = data.slice(0, this.props.maxItems);
      removeItems.forEach(item => {
        // if remain do not contain
        const found = data.findIndex(c => c.blobURL === item.blobURL) !== -1;
        if (!found) {
          URL.revokeObjectURL(item.blobURL);
          // console.log('Clean ', item.blobURL);
        }
      });
    }

    this.setState({
      data
    });
  }

  render() {
    const { data } = this.state;
    const { onItemClick } = this.props;
    return (
      <List disablePadding dense className={styles.listPeople}>
        {data.map(item => (
          <AvatarPopover
            onClick={() => onItemClick(item)}
            key={item.time}
            {...item}
          />
        ))}
      </List>
    );
  }
}

export default class Home extends React.Component {
  constructor(props) {
    super(props);

    // this.preview = React.createRef();
    this.listUnknown = React.createRef();
    this.listKnown = React.createRef();

    // render running devices, re map real-index for sorting
    const { devices, warning } = this.props;
    this.renderIndexes = [];
    this.renderDevices = devices.filter(
      (device, index) => device.running && this.renderIndexes.push(index)
    );

    // filter warning from camera, check all camera belong to people count or people recognition
    if (Array.isArray(warning))
      warning
        .filter(rule => rule)
        .forEach(rule => {
          const { camera, ...warningConfig } = rule;
          this.renderDevices.forEach(device => {
            if (camera && camera.length > 0 && camera.includes(device.url)) {
              Object.assign(device, { warningConfig });
            }
          });
        });
  }

  getAvatar(label) {
    const { people } = this.props;

    if (!people) return;
    const person = people.find(item => item && item.label === label);
    if (person && Array.isArray(person.files))
      // return first files
      return person.files[0].path;
  }

  getIdUnknowPerson = () => {
    const d = new Date();
    const dformat = `${[d.getMonth() + 1, d.getDate(), d.getFullYear()].join(
      '/'
    )} ${[d.getHours(), d.getMinutes(), d.getSeconds()].join(':')}`;
    return dformat;
  };

  onRecognition = data => {
    const { confidence } = this.props;
    if (!data || !this.listUnknown.current || !this.listKnown.current) {
      return;
    }
    const known = [];
    const unknown = [];

    data
      .filter(item => item.label)
      .forEach((item, index) => {
        // confidence 0.5
        // console.log(item.confidence, item);
        if (item.confidence < confidence) {
          item.label = 'unknown';
          item.id = `unknown - ${this.listUnknown.current.state.data.length +
            index} ${this.getIdUnknowPerson()}`;
        }
        if (item.label.match(/unknown/i)) {
          unknown.push(item);
        } else {
          if (process.env.TARGET !== 'web') {
            item.avatar = this.getAvatar(item.label);
          }
          known.push(item);
        }
      });

    // insert db gui canh bao
    // console.log('known: ', known);

    this.listUnknown.current.add(unknown);
    this.listKnown.current.add(known);
  };

  handleClickOpen = item => {
    if (this.preview) {
      this.preview.show(item);
    }
  };

  handleDragEnd = ({ source, destination }: DragAndDropResult) => {
    // map render index for correct re-ordering
    if (source && destination && source.index !== destination.index)
      this.props.deviceReorder(
        this.renderIndexes[source.index],
        this.renderIndexes[destination.index]
      );
  };

  renderRaw(camXS = 12) {
    const { items, videoRatio, aiServer } = this.props;

    return (
      <Grid container item xs={camXS} className={styles.cameraList}>
        <DraggableCameraList
          devices={this.renderDevices}
          items={items}
          aiServer={aiServer}
          onReg={this.onRecognition}
          videoRatio={videoRatio}
          onDragEnd={this.handleDragEnd}
        />
      </Grid>
    );
  }

  render() {
    if (process.env.DISABLED_AI === 'true') {
      return this.renderRaw();
    }

    const camXS = process.env.TARGET === 'web' ? 10 : 9;
    // console.log(currentItem);
    return (
      <Grid container justify="space-between">
        <Grid item>
          <ListPeople
            onItemClick={this.handleClickOpen}
            ref={this.listUnknown}
          />
        </Grid>
        {this.renderRaw(camXS)}
        <Grid item>
          <ListPeople onItemClick={this.handleClickOpen} ref={this.listKnown} />
        </Grid>
        <PreviewDialog
          onItemRef={c => {
            this.preview = c;
          }}
        />
      </Grid>
    );
  }
}
