/* eslint-disable import/prefer-default-export */
/* eslint-disable no-return-assign */
/* eslint-disable global-require */

import { keys } from '../reducers';

// for better performance, with devices we will store as seperated
// then later can implement inbound, oubound
export const configureStore = async (inbound, outbound) => {
  let prevState = {};
  // restore prevState
  const values = await Promise.all(keys.map(inbound));
  values.forEach((value, ind) => {
    // do no update null value, use default
    if (value) prevState[keys[ind]] = value;
  });

  // dynamic import
  const configureStoreModule = await (process.env.NODE_ENV === 'production'
    ? import('./configureStore.prod')
    : import('./configureStore.dev'));

  const store = configureStoreModule.default(prevState);

  store.subscribe(() => {
    const currentState = store.getState();
    keys.forEach(key => {
      if (currentState[key] !== prevState[key]) {
        // console.log('update for', key, currentState[key]);
        outbound(key, currentState);
      }
    });

    prevState = currentState;
  });

  return store;
};

// export { history, configureStore };
