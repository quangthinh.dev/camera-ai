/* eslint-disable no-case-declarations */

import {
  CHANGE_LOCALE,
  CHANGE_THEME,

  // LICENSE_UPDATE,
  SET_ITEM
} from '../actions/settings';
import type { Action } from './types';

export const initialState = {
  locale: navigator.language.split('-')[0],
  theme: 'dark',
  items: 2,
  fps: 24,
  vfSave: false,
  dataDir: '',
  videoLength: '15',
  license: '',
  pageSize: 10,
  videoRatio: 0.5625
};

export default function(state = initialState, action: Action) {
  switch (action.type) {
    case SET_ITEM:
      return {
        ...state,
        [action.payload.key]: action.payload.item
      };
    case CHANGE_LOCALE:
      return {
        ...state,
        locale: action.payload
      };
    case CHANGE_THEME:
      return {
        ...state,
        theme: action.payload
      };

    // case LICENSE_UPDATE:
    //   return { ...state, license: action.payload };

    default:
      return state;
  }
}
