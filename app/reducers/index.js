import { combineReducers } from 'redux';
import form from 'redux-form/es/reducer';
import settings from './settings';
import devices from './devices';

const reducerMap = {
  devices,
  settings,
  form
};

// do not store router, just load home page first
export const keys = Object.keys(reducerMap);
export default combineReducers(reducerMap);
