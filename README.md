# development

```bash
yarn start-server-dev
# yarn start-server

yarn dev
# TARGET=web yarn dev

# run production
START_HOT=false NODE_SERVER=true yarn open-main

# on windows
yarn cross-env START_HOT=false NODE_SERVER=true DISABLED_LICENSE=true SERVER_PORT=80 WEB=true yarn open-main

# run a specific test
yarn test test/cron/rule.spec.js -t cron
```

If run start-server-dev then open devtool for server at: chrome://inspect/#devices -> 'Open dedicated DevTools for Node'

# package

```bash
DISABLED_AI=true SERVER_PORT=80 TARGET=web yarn build-renderer
DISABLED_AI=false SERVER_PORT=80 TARGET=web yarn build-renderer
# bundle module
MODULE=hello,goodbye yarn build-renderer
# analytic bundle
OPEN_ANALYZER=true yarn build-renderer
SERVER_PORT=80 WEB=true yarn package
#or with debug
DEBUG_PROD=true SERVER_PORT=80 WEB=true yarn package

# bundle web server
TARGET=node WEB=true yarn build-main
# node -e "require('./app/dist/server.prod').runHttpServer({ log: true, port: 80 });"

# generate license
node app/server/license/test.js uuid
# or DISABLED_LICENSE=true yarn open-main
```

# run server alone

```bash
yarn start-server
# using script command :D
SERVER_PORT=80 WEB=true pm2 start npm -- run start-server
# or
SERVER_PORT=80 WEB=true pm2 start node -- -r @babel/register app/server/start.js
# then rename later with pm2
pm2 restart {id} --name web
```

# rebuild module for different version of node in electron

```bash
# fix canvas on mac
PKG_CONFIG_PATH=/usr/local/Cellar/libffi/3.2.1/lib/pkgconfig yarn rebuild-module canvas
# other build:
yarn rebuild-module @tensorflow/tfjs-node
```

# rebuild for electron

```bash
yarn electron-rebuild-module @tensorflow/tfjs-node
# install new module
yarn add module --ignore-scripts
```

# package

```bash
# NODE_SERVER=true DEBUG_PROD=true
yarn package --mac
```

# full hot-reload

```bash
# otherwise watch single route by default for less cpu usage
yarn watch-server
```

## Build native module:

`cd app && yarn add module`

## Fix electron version

`electron-fix start` or

`echo "module.exports = '$PWD/node_modules/electron/dist/Electron.app/Contents/MacOS/Electron'" > node_modules/electron/index.js`

## assign a task

```js
emitter.on('done-task.detectImage', data => console.log(data));
emitter.emit('assign-task.detectImage', null, '/Users/thanhtu/Downloads/1.jpg');
```
