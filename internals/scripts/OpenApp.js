import { spawn } from 'child_process';

export default function OpenApp(runCMD, exitOnClose = false) {
  const child = spawn('npm', ['run', runCMD], {
    detached: exitOnClose,
    shell: true,
    env: process.env,
    // stdin, stdout, stderr
    stdio: [null, 'inherit', 'pipe']
  });

  child
    .on('close', code => {
      if (exitOnClose) process.exit(code);
    })
    .on('error', spawnError => {
      console.error(spawnError);
    });

  // pipe to your stream with error
  child.stderr.on('data', data => {
    const chunk = data.toString();
    if (
      /\d+-\d+-\d+ \d+:\d+:\d+\.\d+ Electron(?: Helper)?\[\d+:\d+] /.test(chunk)
    ) {
      return false;
    }

    // Example: [90789:0810/225804.894349:ERROR:CONSOLE(105)] "Uncaught (in promise) Error: Could not instantiate: ProductRegistryImpl.Registry", source: chrome-devtools://devtools/bundled/inspector.js (105)
    if (/\[\d+:\d+\/|\d+\.\d+:ERROR:CONSOLE\(\d+\)\]/.test(chunk)) {
      return false;
    }

    // Example: ALSA lib confmisc.c:767:(parse_card) cannot find card '0'
    if (/ALSA lib [a-z]+\.c:\d+:\([a-z_]+\)/.test(chunk)) {
      return false;
    }
    console.error(chunk);
  });
}

if (require.main === module) {
  // run directly
  OpenApp(process.env.INSPECT ? 'start-main-dev' : 'start-main');
}
