/* eslint-disable guard-for-in */
/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-syntax */
import fs from 'fs';
import path from 'path';

const PLUGIN_NAME = 'FilterChunkWebpackPlugin';

export default class FilterChunkWebpackPlugin {
  constructor({ modulePath, modules }) {
    if (!modules.length) {
      this.disabledModule = true;
      this.prefixModules = fs
        .readdirSync(modulePath, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => `${dirent.name}.`);
    } else {
      this.prefixModules = modules.map(name => `${name}.`);
      this.modulePath = modulePath;
    }
  }

  apply(compiler) {
    if (this.disabledModule) {
      compiler.hooks.emit.tap(PLUGIN_NAME, compilation => {
        for (const name in compilation.assets) {
          // remove all modules
          if (this.prefixModules.some(prefix => name.startsWith(prefix))) {
            delete compilation.assets[name];
          }
        }
      });
    } else {
      compiler.hooks.emit.tap(PLUGIN_NAME, compilation => {
        for (const name in compilation.assets) {
          // not in allowed modules
          if (this.prefixModules.every(prefix => !name.startsWith(prefix))) {
            delete compilation.assets[name];
          }
        }
      });

      compiler.hooks.afterEmit.tap(PLUGIN_NAME, compilation => {
        for (const name in compilation.assets) {
          if (name.endsWith('.js')) {
            const baseName = compilation.assets[name].existsAt.slice(0, -3);
            fs.copyFileSync(
              path.join(this.modulePath, name.split('.')[0], 'package.json'),
              `${baseName}.json`
            );
            // const cssFile = `${baseName}.css`;
            // if (!fs.existsSync(cssFile)) fs.writeFileSync(cssFile, '');
          }
        }
      });
    }
  }
}
